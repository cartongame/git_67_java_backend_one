package com.imti.bean.dto;

import com.imti.bean.pojo.Nurse;
import lombok.Data;

import java.io.Serializable;

@Data
public class NurseDto extends Nurse implements Serializable {
    private String desknurseName;
}
