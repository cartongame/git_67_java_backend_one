
package com.imti.bean.pojo;

import java.io.Serializable;


/**
 * @文件名: UserInfo.java
 * @类功能说明: 用户实体层
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年7月30日上午11:13:14
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年7月30日上午11:13:14</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 863377942242255764L;
	private Integer userId; // 用户id
	private Integer groupId; // 用户组id
	private String userName; // 用户名
	private String nickName;// 昵称

	private String userCode; // 用户登录code
	private String userPwd; // 用户密码
	private String userType; // 用户类型：1：超级管理员 2：管理员 3：普通用户
	private String userState;// 用户状态：0：未审核 1：已审核
	private String isDelete; // 删除状态：0：正常 1：已删除
	private String createTime; // 创建时间
	private String updateTime; // 修改时间
	private String start;
	private String end;
	private Integer createBy;
	private Integer updateBy;
	

	public Integer getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserState() {
		return userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

}
