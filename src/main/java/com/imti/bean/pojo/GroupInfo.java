package com.imti.bean.pojo;

import java.io.Serializable;

/**
 * @文件名: GroupInfo.java
 * @类功能说明: 用户组实体类
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月8日下午2:56:06
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月8日下午2:56:06</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
public class GroupInfo implements Serializable {

	
	private static final long serialVersionUID = -8588338343516528927L;
    /**
     * 用户组id
     */
	private Integer groupId;
	/**
     * 用户组名称
     */
	private String groupName;
	/**
     * 用户组code
     */
	private String groupCode;
	/**
     * 用户组描述
     */
	private String groupDesc;
	/**
     * 用户组状态
     */
	private String groupState;
	
	private String se;
	public String getSe() {
		return se;
	}

	public void setSe(String se) {
		this.se = se;
	}
	
	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	public String getGroupState() {
		return groupState;
	}

	public void setGroupState(String groupState) {
		this.groupState = groupState;
	}
}
