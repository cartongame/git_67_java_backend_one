package com.imti.bean.pojo;

/**
 * @文件名: AuthVos.java
 * @类功能说明: Vos类
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月8日上午11:30:25
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月8日上午11:30:25</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
public class AuthVos {
	private Integer userauthId;

	private Integer authId;

	private Integer userId;

	public Integer getUserauthId() {
		return userauthId;
	}

	public void setUserauthId(Integer userauthId) {
		this.userauthId = userauthId;
	}

	public Integer getAuthId() {
		return authId;
	}

	public void setAuthId(Integer authId) {
		this.authId = authId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
