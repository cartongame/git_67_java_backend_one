package com.imti.bean.pojo;

/**
 * @文件名: AuthVo.java
 * @类功能说明: Vo类
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月6日上午10:10:36
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月6日上午10:10:36</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
public class AuthVo {
	private Integer roleauthId;

	public Integer getRoleauthId() {
		return roleauthId;
	}

	public void setRoleauthId(Integer roleauthId) {
		this.roleauthId = roleauthId;
	}

	private Integer authId;

	private Integer roleId;

	public Integer getAuthId() {
		return authId;
	}

	public void setAuthId(Integer authId) {
		this.authId = authId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}
