package com.imti.bean.pojo;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**@文件名: Doctor.java
 * @类功能说明: 医生javabean
 *@作者: yaozhen
 * @Email: yaozhen@qq.com
 * @日期: 2020年11月10日上午9:12:42
 * @修改说明:<br> 
 * <pre>
 * 	 <li>作者: yaozhen</li> 
 * 	 <li>日期: 2020年11月10日上午9:12:42</li> 
 *	 <li>内容: </li>
 * </pre>
 */
public class DeskInfo implements Serializable{
	
	private static final long serialVersionUID = 6131511503453453158L;
	//科室id
	private Integer deskId;
	//姓名
	private String name;
	public Integer getDeskId() {
		return deskId;
	}
	public void setDeskId(Integer deskId) {
		this.deskId = deskId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
