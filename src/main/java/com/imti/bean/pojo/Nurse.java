package com.imti.bean.pojo;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
public class Nurse implements Serializable {
    private Integer nurseId;
    private Integer deskId;
    private Integer accId;
    private String isHost;
    private String nurseName;
    private char sex;

    private Date birthday;
    private String intro;
    private String state;
    private String phone;
    private String email;
    private String nurseConcat;
    private String tel;

}
