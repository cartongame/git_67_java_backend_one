package com.imti.bean.pojo;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**@文件名: Doctor.java
 * @类功能说明: 医生javabean
 *@作者: yaozhen
 * @Email: yaozhen@qq.com
 * @日期: 2020年11月10日上午9:12:42
 * @修改说明:<br> 
 * <pre>
 * 	 <li>作者: yaozhen</li> 
 * 	 <li>日期: 2020年11月10日上午9:12:42</li> 
 *	 <li>内容: </li>
 * </pre>
 */
public class Doctor implements Serializable{
	private static final long serialVersionUID = 6131511503453453158L;
	//医生id
	private Integer docId;
	//科室id
	private Integer deskId;
	//账号id
	private Integer accId;
	//职位：1 主任医师  2 副主任医师  3 主治医师 4 实习医生
	private String job;
	//姓名
	private String name;
	//性别：0 女 1 男
	private String sex;
	//出生日期
	private String birthday;
	//介绍
	private String intro;
	//在职状态：-1 离职  0 退休   1  在职
	private String state;
	//电话
	private String phone;
	//邮箱
	private String email;
	//紧急联系人
	private String concat;
	//电话
	private String tel;
	//医生照片
	private String img;
	//医生地址
	private String address;
	
	private String deskName;
	
	public String getDeskName() {
		return deskName;
	}
	public void setDeskName(String deskName) {
		this.deskName = deskName;
	}
	public Integer getDocId() {
		return docId;
	}
	public void setDocId(Integer docId) {
		this.docId = docId;
	}
	public Integer getDeskId() {
		return deskId;
	}
	public void setDeskId(Integer deskId) {
		this.deskId = deskId;
	}
	public Integer getAccId() {
		return accId;
	}
	public void setAccId(Integer accId) {
		this.accId = accId;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getConcat() {
		return concat;
	}
	public void setConcat(String concat) {
		this.concat = concat;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
