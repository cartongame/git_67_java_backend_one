
package com.imti.bean.pojo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @文件名: AuthInfo.java
 * @类功能说明: 权限实体类
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月16日下午2:56:36
 * @修改说明:<br> 
 * <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月16日下午2:56:36</li> 
 *	 <li>内容: </li>
 * </pre>
 */
public class AuthInfo implements Serializable {
	private static final long serialVersionUID = 4074340983043786386L;
	/*
	 * 权限id
	 */
	Integer authId;
	/*
	 * 权限父id
	 */
	Integer parentId;
	/*
	 * 权限名称
	 */
	String authName;
	/*
	 * 权限编码
	 */
	String authCode;
	/*
	 * 菜单url地址
	 */
	String authUrl;
	/*
	 * 权限级别
	 */
	String authType;

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public String getAuthUrl() {
		return authUrl;
	}

	public void setAuthUrl(String authUrl) {
		this.authUrl = authUrl;
	}

	/*
	 * 子权限集合
	 */
	List<AuthInfo> childList = new ArrayList<AuthInfo>();

	public Integer getAuthId() {
		return authId;
	}

	public void setAuthId(Integer authId) {
		this.authId = authId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getAuthName() {
		return authName;
	}

	public void setAuthName(String authName) {
		this.authName = authName;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public List<AuthInfo> getChildList() {
		return childList;
	}

	public void setChildList(List<AuthInfo> childList) {
		this.childList = childList;
	}
}
