/**
 * @filename StaticVo.java
 * @author lg
 * @date 2020年11月13日 上午9:38:01
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.bean.pojo;

import java.io.Serializable;
/**
 * 统计查询vo类
 *@comment
 *@author lg
 *@date 2020年11月13日 上午9:38:24
 *@modifyUser lg
 *@modifyDate 2020年11月13日 上午9:38:24
 *@modifyDesc  TODO
 *@version TODO
 */
public class StaticVo implements Serializable {
 private static final long serialVersionUID = 4801799953121426990L;
  private String name;
  private Float total;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getTotal() {
		return total;
	}
	public void setTotal(Float total) {
		this.total = total;
	}	
  
}
