package com.imti.mapper;

import com.imti.bean.dto.NurseDto;
import com.imti.bean.pojo.Nurse;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.List;
import java.util.Map;

public interface NurseMapper {

//    @Select("<script>SELECT COUNT(*) FROM nurse\n" +
//            "WHERE  1=1 " +
//            "<if test='null != nurseName and nurseName != '' '> nurse_id LIKE concat('%', #{nurseName}, '%') </if>" +
//            "<if test='null != state and state != '' '> AND state = #{state} </if>" +
//            "</script>")
    public abstract int getNurseCount(Nurse nurse);

//    @SelectProvider(value = NurseMapperSqlPrivider.class, method = "findNurseInfoSql")
//    @Results(id = "nurseInfoMap",value = {
//            @Result(id = true, column = "nurse_id" ,property = "nurseId", javaType = Integer.class, jdbcType = JdbcType.INTEGER),
//            @Result(column = "desk_id" ,property = "deskId", javaType = Integer.class, jdbcType = JdbcType.INTEGER),
//            @Result(column = "acc_id" ,property = "accId", javaType = Integer.class, jdbcType = JdbcType.INTEGER),
//            @Result(column = "is_host" ,property = "isHost", javaType = String.class, jdbcType = JdbcType.VARCHAR),
//            @Result(column = "nurse_name" ,property = "nurseName", javaType = String.class, jdbcType = JdbcType.VARCHAR),
//            @Result(column = "nurse_concat" ,property = "nurseConcat", javaType = String.class, jdbcType = JdbcType.VARCHAR)
//    })
    public abstract List<NurseDto> findNurseInfo(Map<String, Object> map);

    public abstract Boolean saveNurse(Nurse nurse);
}
