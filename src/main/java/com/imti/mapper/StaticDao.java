/**
 * @filename StaticDao.java
 * @author lg
 * @date 2020年11月13日 上午9:42:26
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.imti.bean.pojo.StaticVo;

public interface StaticDao {
/**
 * 统计查询科室的贡献值
 * @comment 
 * @param date ：202010
 * @return
 * @version 1.0
 */
@Select("SELECT dw.`name`,SUM(c.`total`)AS total FROM cost_list c,`doctor` d,`desk_work` dw"
+" WHERE c.`doc_id`=d.`doc_id` AND d.`desk_id`=dw.`desk_id`"
+" AND DATE_FORMAT(c.`create_time`,'%Y%m') =#{date} GROUP BY dw.`name`")	
public List<StaticVo> findDeskMoney(String date);
/**
 * 查询上个月份
 * @comment 
 * @return
 * @version 1.0
 */
@Select("SELECT PERIOD_ADD(DATE_FORMAT(NOW(),'%Y%m'),-1)  FROM DUAL")
public String findPreMoney();
}
