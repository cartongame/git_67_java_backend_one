package com.imti.mapper;

import com.imti.bean.pojo.Nurse;
import com.imti.util.PageBean;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class NurseMapperSqlPrivider {

    public String findNurseInfoSql(Map<String, Object> map){

        return new SQL(){{
            SELECT("n.*,dw.`desk_name` AS desknurseName");
            FROM("nurse n, desk_work dw");
            StringBuilder sb = new StringBuilder();
            Nurse nurse = (Nurse) map.get("nurse");
            if (null != nurse && null != nurse.getNurseName() && StringUtils.isNotBlank(nurse.getNurseName())){
                sb.append(" and n.nurse_name like concat('%', #{nurseName} ,'%' )");
            }
            if (null != nurse && null != nurse.getState() && StringUtils.isNotBlank(nurse.getState())){
                sb.append(" and n.state = #{state}");
            }
            WHERE(" 1=1 and n.`desk_id` = dw.`desk_id` " + sb.toString());
            PageBean page = (PageBean) map.get("page");
            if (null != page && null != page.getPageIndex()){
                LIMIT(page.getPageIndex());
            }
            if (null != page && null != page.getPage()){
                LIMIT(page.getPage());
            }
        }}.toString();
    }
}
