/**
 * @filename RoleDao.java
 * @author lg
 * @date 2020年8月4日 下午2:08:15
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.mapper;

import java.util.List;
import java.util.Map;

import com.imti.bean.pojo.RoleInfo;
import com.imti.bean.pojo.UserInfo;


/**
 * @comment 角色映射接口
 * @author sdb
 * @date 2020年8月4日 下午2:08:22
 * @modifyUser sdb
 * @modifyDate 2020年8月4日 下午2:08:22
 */
public interface RoleInfoDao {
	/**
	 * @comment 查询所有角色
	 * @return
	 * @version 1.0
	 */
	public List<RoleInfo> findAllRoleInfo();

	/**
	 * @comment 根据用户id查询已经绑定的角色信息
	 * @param user
	 * @return
	 * @version 1.0
	 */
	public String getRoleInfoByUserId(UserInfo userInfo);

	/**
	 * @方法名: deleteUserInfoRoleInfo
	 * @方法说明: 删除用户角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月4日下午10:36:07
	 * @param userId
	 * @return
	 * @return: int
	 */
	public int deleteUserInfoRoleInfo(Integer userId);

	/**
	 * @方法名: addUserInfoRoleInfo
	 * @方法说明: 给用户新增角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月4日下午10:36:32
	 * @param map
	 * @return
	 * @return: int
	 */
	public int addUserInfoRoleInfo(Map<String, Object> map);

	/**
	 * @方法名: deleteGroupRoleInfo
	 * @方法说明: 删除用户角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月4日下午10:36:07
	 * @param groupId
	 * @return
	 * @return: int
	 */
	public int deleteGroupRoleInfo(Integer groupId);

	/**
	 * @方法名: addGroupRoleInfo
	 * @方法说明: 给用户组新增角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月4日下午10:36:32
	 * @param map
	 * @return
	 * @return: int
	 */
	public int addGroupRoleInfo(Map<String, Object> map);

	/**
	 * @方法名: getRoleCount
	 * @方法说明: 查询角色总条数
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午8:58:18
	 * @param user
	 * @return
	 * @return: int
	 */
	public int getRoleCount(RoleInfo role);

	/**
	 * @方法名: findRoleInfo
	 * @方法说明: 查询角色列表
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午9:03:44
	 * @param map
	 * @return
	 * @return: List<RoleInfo>
	 */
	public List<RoleInfo> findRoleInfo(Map<?, ?> map);

	/**
	 * @方法名: deleteRoleAuth
	 * @方法说明: 删除角色权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日下午5:40:37
	 * @param roleId
	 * @return
	 * @return: int
	 */
	public int deleteRoleAuth(Integer roleId);

	/**
	 * @方法名: addRoleAuth
	 * @方法说明: 新增角色权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日下午5:40:50
	 * @param map
	 * @return
	 * @return: int
	 */
	public int addRoleAuth(Map<String, Object> map);

	/**
	 * @方法名: getRoleName
	 * @方法说明: 角色名唯一验证
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:06:58
	 * @param roleInfo
	 * @return
	 * @return: int
	 */
	public int getRoleName(RoleInfo roleInfo);

	/**
	 * @方法名: getRoleId1
	 * @方法说明: 查询角色是否被占用
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:06:58
	 * @param roleInfo
	 * @return
	 * @return: int
	 */
	public int getRoleId1(RoleInfo roleInfo);
	
	/**
	 * @方法名: getRoleId2
	 * @方法说明: 查询角色是否被占用
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:06:58
	 * @param roleInfo
	 * @return
	 * @return: int
	 */
	public int getRoleId2(RoleInfo roleInfo);

	/**
	 * @方法名: saveRoleInfo
	 * @方法说明: 新增角色保存
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:32:52
	 * @param roleInfo
	 * @return
	 * @return: int
	 */
	public int saveRoleInfo(RoleInfo roleInfo);

	/**
	 * @方法名: updateRoleState
	 * @方法说明:启用禁用角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:54:30
	 * @param roleInfo
	 * @return
	 * @return: int
	 */

	public int updateRoleState(RoleInfo roleInfo);

	
}
