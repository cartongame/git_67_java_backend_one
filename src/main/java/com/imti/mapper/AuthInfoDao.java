/**
 * @filename AuthInfoDao.java
 * @author lg
 * @date 2020年7月30日 下午1:56:59
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.mapper;

import java.util.List;
import java.util.Map;

import com.imti.bean.pojo.AuthInfo;
import com.imti.bean.pojo.AuthVo;
import com.imti.bean.pojo.AuthVos;
import com.imti.bean.pojo.RoleInfo;
import com.imti.bean.pojo.UserInfo;

public interface AuthInfoDao {
	/**
	 * @comment 查询当前用户的权限
	 * @param map:
	 *            userId goupId
	 * @return 权限集合
	 * @version 1.0
	 */
	public List<AuthInfo> findAuthInfo(Map<?, ?> map);

	/**
	 * @方法名: findAuthInfo
	 * @方法说明: 查询所有用户的权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午1:22:38
	 * @param map
	 * @return
	 * @return: List<AuthInfo>
	 */
	public List<AuthInfo> findAllAuthInfo();

	/**
	 * @方法名: findAuthInfo
	 * @方法说明: 查询所有用户的权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午1:22:38
	 * @param map
	 * @return
	 * @return: List<AuthInfo>
	 */
	public List<AuthInfo> findAllAuthInfos();

	/**
	 * @方法名: findRoleAuthInfo
	 * @方法说明: 查询角色权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午10:13:37
	 * @return
	 * @return: List<AuthVo>
	 */
	public List<AuthVo> findRoleAuthInfo(RoleInfo roleInfo);

	/**
	 * @方法名: findUserInfoAuthInfo
	 * @方法说明: 查询用户的所有权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月8日上午11:35:46
	 * @param userInfo
	 * @return
	 * @return: List<AuthVo>
	 */
	public List<AuthVos> findUserInfoAuthInfo(UserInfo userInfo);

	/**
	 * @方法名: getAuthName
	 * @方法说明: 查询菜单是否存在
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:15:44
	 * @param authInfo
	 * @return
	 * @return: int
	 */
	public int getAuthName(AuthInfo authInfo);

	/**
	 * @方法名: getAuthUrl
	 * @方法说明: 查询权限路径是否存在
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月8日上午12:21:21
	 * @param authInfo
	 * @return
	 * @return: int
	 */
	public int getAuthUrl(AuthInfo authInfo);

	/**
	 * @方法名: saveAuthInfo
	 * @方法说明: 新增菜单
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:54:55
	 * @param authInfo
	 * @return
	 * @return: int
	 */
	public int saveAuthInfo(AuthInfo authInfo);

	/**
	 * @方法名: getUserButtonAuth
	 * @方法说明: 查询当前用户所拥有三级权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月10日下午4:50:23
	 * @param map
	 * @return
	 * @return: String
	 */
	public String getUserButtonAuth(Map<String, Object> map);
}
