
package com.imti.mapper;

import java.util.List;
import java.util.Map;

import com.imti.bean.pojo.GroupInfo;
import com.imti.bean.pojo.UserInfo;

/**
 * @文件名: UserInfoDao.java
 * @类功能说明: 用户映射接口
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月16日下午3:03:43
 * @修改说明:<br> 
 * <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月16日下午3:03:43</li> 
 *	 <li>内容: </li>
 * </pre>
 */
public interface UserInfoDao {
	/**
	 * @comment 查询用户总条数
	 * @param user
	 *            用户对象
	 * @return 总条数
	 * @version 1.0
	 */
	public int getUserCount(UserInfo user);

	/**
	 * @comment 查询用户列表
	 * @param map:
	 *            user、 page
	 * @return 用户集合
	 * @version 1.0
	 */
	public List<UserInfo> findUserInfo(Map<String,Object> map);

	/**
	 * @comment 密码重置
	 * @param user
	 * @return
	 * @version 1.0
	 */
	public int updatePwd(UserInfo user);

	/**
	 * @方法名: findUserInfoByUserNameAndUserPwd
	 * @方法说明: 管理员登陆
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月3日下午6:31:09
	 * @param map
	 * @return
	 * @return: UserInfo
	 */
	public UserInfo findUserInfoByUserNameAndUserPwd(Map<String, Object> map);

	/**
	 * @方法名: getUserName
	 * @方法说明: 查询用户是否存在
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月5日上午9:07:36
	 * @param userInfo
	 * @return
	 * @return: int
	 */
	public int getUserName(UserInfo userInfo);

	/**
	 * @方法名: setUserInfo
	 * @方法说明: 新增用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月5日上午9:08:13
	 * @param userInfo
	 * @return
	 * @return: int
	 */
	public int setUserInfo(UserInfo userInfo);

	/**
	 * @方法名: findAllUserGroup
	 * @方法说明: 查询所有用户组
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月5日上午9:46:56
	 * @return
	 * @return: UserGroup
	 */
	public List<GroupInfo> findAllUserGroup();

	/**
	 * @方法名: deleteUserRole
	 * @方法说明: 删除用户角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午12:45:05
	 * @param user
	 * @return
	 * @return: int
	 */
	public int deleteUserRole(UserInfo user);

	/**
	 * @方法名: deleteUser
	 * @方法说明: 逻辑删除用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午12:43:38
	 * @param user
	 * @return
	 * @return: int
	 */
	public int deleteUser(UserInfo user);

	/**
	 * @方法名: deleteUserInfo
	 * @方法说明: 逻辑删除用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日下午2:57:24
	 * @param userId
	 * @return
	 * @return: int
	 */
	public int deleteUserInfo(Integer userId);

	/**
	 * @方法名: deleteUserInfoRoleInfo
	 * @方法说明: 删除用户角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日下午2:57:32
	 * @param userId
	 * @return
	 * @return: int
	 */
	public int deleteUserInfoRoleInfo(Integer userId);

	/**
	 * @方法名: deleteUserAuth
	 * @方法说明: 删除用户权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日下午5:40:37
	 * @param roleId
	 * @return
	 * @return: int
	 */
	public int deleteUserAuth(Integer userId);

	/**
	 * @方法名: addRoleAuth
	 * @方法说明: 新增角色权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日下午5:40:50
	 * @param map
	 * @return
	 * @return: int
	 */
	public int addUserAuth(Map<String, Object> map);

	/**
	 * @方法名: updateUserState
	 * @方法说明: 启用禁用用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午6:45:50
	 * @param user
	 * @return
	 * @return: int
	 */
	public int updateUserState(UserInfo user);

	/**
	 * @方法名: updateUserState2
	 * @方法说明: 启用用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午6:46:20
	 * @param user
	 * @return
	 * @return: int
	 */
	public int updateUserState2(UserInfo user);
	/**
	 * @方法名: getUserGroupByUserId
	 * @方法说明: 查询用户用户组
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月17日上午12:27:33
	 * @param userInfo
	 * @return
	 * @return: int
	 */
	public Integer getUserGroupByUserId(UserInfo userInfo);
	/**
	 * @方法名: setUserGroup
	 * @方法说明: 分配用户组
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月17日上午1:10:32
	 * @param groupId
	 * @param userId
	 * @return
	 * @return: int
	 */
	public int setUserGroup(UserInfo user);

}
