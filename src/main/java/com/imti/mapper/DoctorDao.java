/**
 * @filename DoctorDao.java
 * @author lg
 * @date 2020年11月11日 下午2:48:37
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import com.imti.bean.pojo.DeskInfo;
import com.imti.bean.pojo.Doctor;

/**
 * 医生持久化接口
 *@comment
 *@author lg
 *@date 2020年11月11日 下午2:48:47
 *@modifyUser lg
 *@modifyDate 2020年11月11日 下午2:48:47
 *@modifyDesc  TODO
 *@version TODO
 */
public interface DoctorDao {
	
	 @Select("select desk_id as deskId,name from desk_work")
	 public List<DeskInfo> findAllDesk();
  /**
   * 查询医生数量
   * @comment 
   * @param doc
   * @return
   * @version 1.0
   */
  /*@Select("<script>SELECT COUNT(0) FROM `doctor` WHERE 1=1 <if test=\"deskId!=null and deskId!=''\"> AND `desk_id`=#{deskId}</if>"
   +"<if test=\"job!=null and job!=''\"> AND `job`=#{job}</if> <if test=\"phone!=null and phone!=''\"> AND `phone`=#{phone}</if>"
   +"<if test=\"name!=null and name!=''\">AND `name` LIKE CONCAT('%',#{name},'%')</if></script>")
  public int getDoctorCount(Doctor doc);*/	
  
	@SelectProvider(type=DoctorInnerDao.class,method="getDoctorCount")
	public int getDoctorCount(Doctor doc);
  /**
   * 分页查询医生列表信息
   * @comment 
   * @param map ：doc 医生 page 分页对象
   * @return
   * @version 1.0
   */
 /* @Select("<script>SELECT d.*,FN_DeskName(d.desk_id) as deskName FROM `doctor` d WHERE 1=1 <if test=\"doc.deskId!=null and doc.deskId!=''\"> AND `desk_id`=#{doc.deskId}</if>"
		   +"<if test=\"doc.job!=null and doc.job!=''\"> AND `job`=#{doc.job}</if> <if test=\"doc.phone!=null and doc.phone!=''\"> AND `phone`=#{doc.phone}</if>"
		   +"<if test=\"doc.name!=null and doc.name!=''\">AND `name` LIKE CONCAT('%',#{doc.name},'%')</if>"
		   + " LIMIT #{page.pageIndex},#{page.page}</script>")
  @Results(id="docResultMap",value={
	@Result(id=true,property="docId",column="doc_id"),
	@Result(property="deskId",column="desk_id"),
	@Result(property="accId",column="acc_id")
  })
  public List<Doctor> findAll(Map map);*/
	
	@SelectProvider(type=DoctorInnerDao.class,method="findAll")
	 @Results(id="docResultMap",value={
		@Result(id=true,property="docId",column="doc_id"),
		@Result(property="deskId",column="desk_id"),
		@Result(property="accId",column="acc_id")
	 })
	public List<Doctor> findAll(Map map);
	/**
   * 内部类实现动态sql分页查询
   *@comment
   *@author lg
   *@date 2020年11月11日 下午3:37:43
   *@modifyUser lg
   *@modifyDate 2020年11月11日 下午3:37:43
   *@modifyDesc  TODO
   *@version TODO
   */
  class DoctorInnerDao{
	  /**
	   * 查询医生总条数
	   * @comment 
	   * @param doc
	   * @return
	   * @version 1.0
	   */
	  public String getDoctorCount(Doctor doc){
		  StringBuilder sql=new StringBuilder();
		  sql.append("SELECT COUNT(0) FROM `doctor` WHERE 1=1 ");
		  if(doc.getDeskId()!=null && !"".equals(doc.getDeskId())){
			  sql.append(" AND `desk_id`=#{deskId}");
		  }
		  if(doc.getJob()!=null && !"".equals(doc.getJob())){
			  sql.append(" AND `job`=#{job}");
		  }
		  if(doc.getPhone()!=null && !"".equals(doc.getPhone())){
			  sql.append(" AND `phone`=#{phone}");
		  }
		  if(doc.getName()!=null && !"".equals(doc.getName())){
			  sql.append(" AND `name` LIKE CONCAT('%',#{name},'%')");
		  }
		  return sql.toString();
	  }
	  
	  /**
	   * 分页查询医生列表
	   * @comment 
	   * @param map
	   * @return
	   * @version 1.0
	   */
	  public String findAll(Map map){
		  StringBuilder sql=new StringBuilder();
		  sql.append("SELECT d.*,FN_DeskName(d.desk_id) as deskName FROM `doctor` d WHERE 1=1 ");
		  Doctor doc=(Doctor)map.get("doc");
		  if(doc.getDeskId()!=null && !"".equals(doc.getDeskId())){
			  sql.append(" AND `desk_id`=#{doc.deskId}");
		  }
		  if(doc.getJob()!=null && !"".equals(doc.getJob())){
			  sql.append(" AND `job`=#{doc.job}");
		  }
		  if(doc.getPhone()!=null && !"".equals(doc.getPhone())){
			  sql.append(" AND `phone`=#{doc.phone}");
		  }
		  if(doc.getName()!=null && !"".equals(doc.getName())){
			  sql.append(" AND `name` LIKE CONCAT('%',#{doc.name},'%')");
		  }
		  sql.append(" LIMIT #{page.pageIndex},#{page.page}");
		  return sql.toString();
	  }
  }
}
