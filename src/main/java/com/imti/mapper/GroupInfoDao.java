package com.imti.mapper;

import java.util.List;
import java.util.Map;

import com.imti.bean.pojo.GroupInfo;


/**
 * @文件名: GroupInfoDao.java
 * @类功能说明:
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月8日下午3:18:22
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月8日下午3:18:22</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
public interface GroupInfoDao {
	/**
	 * @方法名: getGroupCount
	 * @方法说明: 查询用户组条数
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月8日下午3:48:09
	 * @param user
	 * @return
	 * @return: int
	 */
	public int getGroupCount(GroupInfo group);

	/**
	 * @方法名: findGroupInfo
	 * @方法说明: 查询用户组列表
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月8日下午3:49:43
	 * @param map
	 * @return
	 * @return: List<UserInfo>
	 */
	public List<GroupInfo> findGroupInfo(Map<String, Object> map);

	/**
	 * @方法名: getRoleInfoByGroupId
	 * @方法说明:
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午11:55:57
	 * @param group
	 * @return
	 * @return: String
	 */
	public String getRoleInfoByGroupId(GroupInfo group);

	/**
	 * @方法名: getGroup
	 * @方法说明:用户组名和用户code唯一验证
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月10日上午1:04:49
	 * @param group
	 * @return
	 * @return: int
	 */
	public int getGroup(GroupInfo group);

	/**
	 * @方法名: saveGroupInfo
	 * @方法说明: 保存新增用户组
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月10日上午1:15:35
	 * @param group
	 * @return
	 * @return: int
	 */
	public int saveGroupInfo(GroupInfo group);

	/**
	 * @方法名: updateGroupState
	 * @方法说明:启用禁用用户组
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:54:30
	 * @param group
	 * @return
	 * @return: int
	 */
	public int updateGroupState(GroupInfo group);

	
	/**
	 * @方法名: getGroupId
	 * @方法说明: 查询用户组是否有用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月10日下午3:55:37
	 * @param group
	 * @return
	 * @return: int
	 */
	public int getGroupId(GroupInfo group);

}
