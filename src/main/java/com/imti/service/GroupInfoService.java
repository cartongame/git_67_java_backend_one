package com.imti.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.imti.bean.pojo.GroupInfo;
import com.imti.util.PageBean;

/**
 * @文件名: GroupInfoService.java
 * @类功能说明:
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月8日下午3:26:42
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月8日下午3:26:42</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
public interface GroupInfoService {
	/**
	 * @方法名: pageGroupInfo
	 * @方法说明: 分页查询用户组列表
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月8日下午3:39:33
	 * @param map
	 * @return
	 * @return: PageBean
	 */
	public PageBean pageGroupInfo(Map<String, Object> map);

	/**
	 * @方法名: getGroupRole
	 * @方法说明: 查询用户组角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午11:50:13
	 * @param group
	 * @param req
	 * @return: void
	 */
	public void getGroupRole(GroupInfo group, HttpServletRequest req);

	/**
	 * @方法名: getGroup
	 * @方法说明:用户组名和用户code唯一验证
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月10日上午1:04:49
	 * @param group
	 * @return
	 * @return: int
	 */
	public int getGroup(GroupInfo group);

	/**
	 * @方法名: saveGroupInfo
	 * @方法说明: 保存新增用户组
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月10日上午1:15:35
	 * @param group
	 * @return
	 * @return: int
	 */
	public int saveGroupInfo(GroupInfo group);

	/**
	 * @方法名: updateGroupState
	 * @方法说明:启用禁用用户组
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:54:30
	 * @param group
	 * @return
	 * @return: int
	 */
	public int updateGroupState(GroupInfo group);

	
}
