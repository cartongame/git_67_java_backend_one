/**
 * @filename DoctorService.java
 * @author lg
 * @date 2020年11月11日 下午4:06:12
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service;

import java.util.List;
import java.util.Map;

import com.imti.bean.pojo.DeskInfo;
import com.imti.util.PageBean;

public interface DoctorService {
	/**
	 * 查询所有科室
	 * @comment 
	 * @return
	 * @version 1.0
	 */
	 public List<DeskInfo> findAllDesk();
	/**
	 * 分页查询医生信息
	 * @comment 
	 * @param map
	 * @return
	 * @version 1.0
	 */
	public PageBean findAllDoc(Map map);
}
