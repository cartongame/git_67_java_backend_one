/**
 * @filename UserInfoService.java
 * @author lg
 * @date 2020年7月31日 上午10:14:22
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.imti.bean.pojo.GroupInfo;
import com.imti.bean.pojo.UserInfo;
import com.imti.util.PageBean;

/**
 * @comment 用户业务接口
 * @author sdb
 * @date 2020年7月31日 上午10:14:59
 * @modifyUser sdb
 * @modifyDate 2020年7月31日 上午10:14:59
 */
public interface UserInfoService {

	/**
	 * @comment 查询用户列表
	 * @param map:
	 *            user、 page
	 * @return 用户集合
	 * @version 1.0
	 */
	public PageBean pageUserInfo(Map<String,Object> map);

	/**
	 * @comment 密码重置
	 * @param user
	 * @return
	 * @version 1.0
	 */
	public int updatePwd(UserInfo user);

	/**
	 * @方法名: deleteUser
	 * @方法说明: 逻辑删除用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午12:43:49
	 * @param user
	 * @return
	 * @return: int
	 */
	public int deleteUser(UserInfo user);

	/**
	 * @方法名: updateUserState
	 * @方法说明: 启用禁用用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午6:45:50
	 * @param user
	 * @return
	 * @return: int
	 */
	public int updateUserState(UserInfo user);

	

	/**
	 * @方法名: findUserInfoByUserNameAndUserPwd
	 * @方法说明: 管理员登陆
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月3日下午6:31:48
	 * @param map
	 * @return
	 * @return: UserInfo
	 */
	public UserInfo findUserInfoByUserNameAndUserPwd(Map<String, Object> map);

	/**
	 * @方法名: getUserName
	 * @方法说明: 查询用户是否存在
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月5日上午9:15:13
	 * @param userInfo
	 * @return
	 * @return: int
	 */
	public int getUserName(UserInfo userInfo);

	/**
	 * @方法名: setUserInfo
	 * @方法说明: 新增用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月5日上午9:08:13
	 * @param userInfo
	 * @return
	 * @return: int
	 */
	public int saveUserInfo(UserInfo userInfo);

	/**
	 * @方法名: findAllUserGroup
	 * @方法说明: 查询所有用户组
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月5日上午9:49:25
	 * @return
	 * @return: UserGroup
	 */
	public List<GroupInfo> findAllUserGroup();

	/**
	 * @方法名: deleteUsers
	 * @方法说明: 批量删除用户
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午11:53:39
	 * @param userIds
	 * @return
	 * @return: int
	 */
	public int deleteUsers(String userIds);

	/**
	 * @方法名: setUserAuth
	 * @方法说明: 分配用户权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午6:55:15
	 * @param authIds
	 * @param userId
	 * @return
	 * @return: int
	 */
	public int setUserAuth(String authIds, Integer userId);
	/**
	 * @方法名: getUserRole
	 * @方法说明: 查询用户用户组
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月17日上午12:22:19
	 * @param user
	 * @param req
	 * @return: void
	 */
	public void getUserGroup(UserInfo user, HttpServletRequest req);
	/**
	 * @方法名: setUserGroup
	 * @方法说明: 分配用户组
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月17日上午1:08:45
	 * @param groupId
	 * @param userId
	 * @return
	 * @return: int
	 */
	public int setUserGroup(UserInfo user);
}
