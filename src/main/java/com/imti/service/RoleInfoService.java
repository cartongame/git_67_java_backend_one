/**
 * @filename RoleService.java
 * @author sdb
 * @date 2020年8月4日 下午2:23:53
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.imti.bean.pojo.RoleInfo;
import com.imti.bean.pojo.UserInfo;
import com.imti.util.PageBean;

/**
 * @comment 角色业务接口
 * @author sdb
 * @date 2020年8月4日 下午2:23:59
 * @modifyUser sdb
 * @modifyDate 2020年8月4日 下午2:23:59
 */
public interface RoleInfoService {
	/**
	 * @comment 查询用户角色
	 * @param user
	 * @version 1.0
	 */
	public void getUserRole(UserInfo user, HttpServletRequest req);

	/**
	 * @方法名: setUserInfoRoleInfo
	 * @方法说明: 给用户分配角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月4日下午10:28:28
	 * @param roleIds
	 * @param userId
	 * @return
	 * @return: int
	 */
	public int setUserInfoRoleInfo(String roleIds, Integer userId);

	/**
	 * @方法名: setGroupInfoRoleInfo
	 * @方法说明: 给用户组分配角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月4日下午10:28:28
	 * @param roleIds
	 * @param groupId
	 * @return
	 * @return: int
	 */
	public int setGroupInfoRoleInfo(String roleIds, Integer groupId);

	/**
	 * @方法名: pageRoleInfo
	 * @方法说明: 查询角色列表
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午9:02:55
	 * @param map
	 * @return
	 * @return: PageBean
	 */
	public PageBean pageRoleInfo(Map<String,Object> map);

	/**
	 * @方法名: setUserInfoRoleInfo
	 * @方法说明: 给角色分配权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日下午5:33:59
	 * @param roleIds
	 * @param userId
	 * @return
	 * @return: int
	 */
	public int setRoleAuth(String authIds, Integer roleId);

	/**
	 * @方法名: getRoleName
	 * @方法说明: 角色名唯一验证
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:05:20
	 * @param userInfo
	 * @return
	 * @return: int
	 */
	public int getRoleName(RoleInfo roleInfo);

	/**
	 * @方法名: saveRoleInfo
	 * @方法说明: 新增角色保存
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:32:52
	 * @param roleInfo
	 * @return
	 * @return: int
	 */
	public int saveRoleInfo(RoleInfo roleInfo);

	/**
	 * @方法名: updateRoleState
	 * @方法说明:启用禁用角色
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:54:30
	 * @param roleInfo
	 * @return
	 * @return: int
	 */

	public int updateRoleState(RoleInfo roleInfo);

	
}
