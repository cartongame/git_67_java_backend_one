package com.imti.service;

import com.imti.bean.pojo.Nurse;
import com.imti.util.PageBean;

import java.util.Map;

public interface NurseService {

    PageBean pageNurseInfo(Map<String, Object> map);

    public abstract Boolean saveNurse(Nurse nurse);
}
