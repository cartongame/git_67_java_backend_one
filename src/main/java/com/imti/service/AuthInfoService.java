/**
 * @filename AuthInfoDao.java
 * @author lg
 * @date 2020年7月30日 下午1:56:59
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.imti.bean.pojo.AuthInfo;
import com.imti.bean.pojo.RoleInfo;
import com.imti.bean.pojo.UserInfo;

import net.sf.json.JSONArray;

/**
 * @comment 权限映射业务接口
 * @author sdb
 * @date 2020年7月30日 下午1:57:13
 * @modifyUser sdb
 * @modifyDate 2020年7月30日 下午1:57:13
 */
public interface AuthInfoService {
	/**
	 * @comment 查询当前用户的权限
	 * @param map:
	 *            userId goupId
	 * @return 权限集合
	 * @version 1.0
	 */
	public List<AuthInfo> findAuthInfo(Map<String,Object> map, HttpServletRequest request);

	/**
	 * @方法名: findAllAuthInfo
	 * @方法说明: 查询所有用户的权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午1:30:02
	 * @return
	 * @return: JSONArray
	 */
	public JSONArray findAllAuthInfo();

	/**
	 * @方法名: findRoleInfoAuthInfo
	 * @方法说明: 查询角色的所有权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月6日上午10:33:31
	 * @return
	 * @return: JSONArray
	 */
	public JSONArray findRoleInfoAuthInfo(RoleInfo roleInfo);

	/**
	 * @方法名: getAuthName
	 * @方法说明: 查询菜单是否存在
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:12:14
	 * @param authInfo
	 * @return
	 * @return: int
	 */
	public int getAuthName(AuthInfo authInfo);

	/**
	 * @方法名: getAuthUrl
	 * @方法说明: 查询权限路径是否存在
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月8日上午12:21:21
	 * @param authInfo
	 * @return
	 * @return: int
	 */
	public int getAuthUrl(AuthInfo authInfo);

	/**
	 * @方法名: saveAuthInfo
	 * @方法说明: 新增权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:52:54
	 * @param authInfo
	 * @return
	 * @return: int
	 */
	public int saveAuthInfo(AuthInfo authInfo);

	/**
	 * @方法名: findUserInfoAuthInfo
	 * @方法说明: 用户分配权限
	 * @作者: ShiDaBao @邮箱：1471262032
	 * @日期: 2020年8月8日上午11:27:39
	 * @param roleInfo
	 * @return
	 * @return: JSONArray
	 */
	public JSONArray findUserInfoAuthInfo(UserInfo userInfo);
	
	
}
