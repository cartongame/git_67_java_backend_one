package com.imti.service.impl;

import com.imti.bean.dto.NurseDto;
import com.imti.bean.pojo.Nurse;
import com.imti.mapper.NurseMapper;
import com.imti.service.NurseService;
import com.imti.util.PageBean;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class NurseServiceImpl implements NurseService {

    @Resource
    private NurseMapper nurseMapper;

    @Override
    public PageBean pageNurseInfo(Map<String, Object> map) {
        // 1、查询总条数
        Nurse nurse = (Nurse) map.get("nurse");
        PageBean pageBean = (PageBean) map.get("page");
        Integer pageLimt = (pageBean.getPage() == null) ? 5 : pageBean.getPage();
        Integer currNum = (pageBean.getCurrNo() == null) ? 1 : pageBean.getCurrNo();
        int totalCount = nurseMapper.getNurseCount(nurse);
        // 2、查询列表
        PageBean page = new PageBean(pageLimt, currNum);
        map.put("page", page);
        List<NurseDto> list = nurseMapper.findNurseInfo(map);
        // 3、封装pagebean
        PageBean npage = new PageBean(pageLimt, totalCount, currNum, list, "/nurse/page", getUserParams(nurse));
        return npage;
    }

    @Override
    public Boolean saveNurse(Nurse nurse) {
        return nurseMapper.saveNurse(nurse);
    }

    /**
     * @comment 拼接user查询参数
     * @param nurse
     * @return
     * @version 1.0
     */
    private String getUserParams(Nurse nurse) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(nurse.getNurseName())) {
            sb.append("&userName=").append(nurse.getNurseName());
        }
        if (StringUtils.isNotBlank(nurse.getState())) {
            sb.append("&state=").append(nurse.getState());
        }
//        if (StringUtils.isNotBlank(user.getStart())) {
//            sb.append("&start=").append(user.getStart());
//        }
//        if (StringUtils.isNotBlank(user.getEnd())) {
//            sb.append("&end=").append(user.getEnd());
//        }
        return sb.toString();// &userName=34343&userState=1&beginTime=2020-01-03
    }
}
