/**
 * @filename AuthInfoServiceImpl.java
 * @author lg
 * @date 2020年7月30日 下午2:28:27
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imti.bean.pojo.AuthInfo;
import com.imti.bean.pojo.AuthVo;
import com.imti.bean.pojo.AuthVos;
import com.imti.bean.pojo.RoleInfo;
import com.imti.bean.pojo.UserInfo;
import com.imti.mapper.AuthInfoDao;
import com.imti.service.AuthInfoService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @comment 权限业务实现类
 * @author sdb
 * @date 2020年7月30日 下午2:29:06
 * @modifyUser sdb
 * @modifyDate 2020年7月30日 下午2:29:06
 */
@Service
public class AuthInfoServiceImpl implements AuthInfoService {
	// 注入authDao接口
	@Autowired
	AuthInfoDao authInfoDao;

	/**
	 * @comment 查询当前用户的权限
	 * @param map:
	 *            userId goupId
	 * @return 权限集合
	 * @version 1.0
	 */
	public List<AuthInfo> findAuthInfo(Map<String,Object> map , HttpServletRequest request) {
		// 查询一级权限id
		map.put("parentId", 0);
		List<AuthInfo> firstList = authInfoDao.findAuthInfo(map);
		String sb="";
		
		// 根据一级权限查询子权限
		for (AuthInfo auth : firstList) {
			map.put("parentId", auth.getAuthId());
			List<AuthInfo> childList = authInfoDao.findAuthInfo(map);
			// 设置父子关系
			auth.setChildList(childList);
			for (AuthInfo second : childList) {
				if(second.getAuthType().equals("2")){
			sb=sb+second.getAuthUrl()+",";}
			
			map.put("parentId", second.getAuthId());
			List<AuthInfo> thirdList = authInfoDao.findAuthInfo(map);
			second.setChildList(thirdList);
			for (AuthInfo third : thirdList) {
				sb=sb+third.getAuthUrl()+",";
				map.put("parentId", third.getAuthId());
				List<AuthInfo> fouthList = authInfoDao.findAuthInfo(map);
				if(fouthList!=null){
					for (AuthInfo fouth : fouthList) {
						sb=sb+fouth.getAuthUrl()+",";
						
					}
				}
			}
			}
		}
		
		request.getSession().setAttribute("authurls", sb);
		
		request.getSession().setAttribute("authCodes", authInfoDao.getUserButtonAuth(map));
		// 返回
		return firstList;
	}
	

	/**
	 * 查询所有用户的权限
	 */
	public JSONArray findAllAuthInfo() {
		JSONArray jsonArray = new JSONArray();
		List<AuthInfo> authInfoList = authInfoDao.findAllAuthInfo();
		for (AuthInfo authInfo : authInfoList) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", authInfo.getAuthId());
			jsonObject.put("pId", authInfo.getParentId());
			jsonObject.put("name", authInfo.getAuthName());
			jsonObject.put("authType", authInfo.getAuthType());
			jsonObject.put("open", false);
			jsonArray.add(jsonObject);
		}

		return jsonArray;
	}

	/**
	 * 查询角色的所有权限
	 */
	public JSONArray findRoleInfoAuthInfo(RoleInfo roleInfo) {
		JSONArray jsonArray = new JSONArray();
		List<AuthInfo> authInfoList = authInfoDao.findAllAuthInfos();
		List<AuthVo> authVoList = authInfoDao.findRoleAuthInfo(roleInfo);
		for (AuthInfo authInfo : authInfoList) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", authInfo.getAuthId());
			jsonObject.put("pId", authInfo.getParentId());
			jsonObject.put("name", authInfo.getAuthName());
			jsonObject.put("open", false);
			for (AuthVo authVo : authVoList) {
				if (authVo.getAuthId() == authInfo.getAuthId()) {
					jsonObject.put("checked", true);

				}
			}
			jsonArray.add(jsonObject);
		}

		return jsonArray;
	}

	/**
	 * 查询菜单是否存在
	 */
	public int getAuthName(AuthInfo authInfo) {

		return authInfoDao.getAuthName(authInfo);
	}

	/**
	 * 新增权限
	 */
	public int saveAuthInfo(AuthInfo authInfo) {
		if (authInfo.getAuthUrl() == "") {
			authInfo.setAuthUrl(null);
		}
		if (authInfo.getAuthCode() == "") {
			authInfo.setAuthCode(null);
		}
		return authInfoDao.saveAuthInfo(authInfo);
	}

	/**
	 * 路径唯一验证
	 */
	public int getAuthUrl(AuthInfo authInfo) {

		return authInfoDao.getAuthUrl(authInfo);
	}

	/**
	 * 用户分配权限
	 */
	public JSONArray findUserInfoAuthInfo(UserInfo userInfo) {
		JSONArray jsonArray = new JSONArray();
		List<AuthInfo> authInfoList = authInfoDao.findAllAuthInfos();
		List<AuthVos> authVosList = authInfoDao.findUserInfoAuthInfo(userInfo);
		for (AuthInfo authInfo : authInfoList) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", authInfo.getAuthId());
			jsonObject.put("pId", authInfo.getParentId());
			jsonObject.put("name", authInfo.getAuthName());
			jsonObject.put("open", false);
			for (AuthVos authVos : authVosList) {
				if (authVos.getAuthId() == authInfo.getAuthId()) {
					jsonObject.put("checked", true);

				}
			}
			jsonArray.add(jsonObject);
		}

		return jsonArray;
	}

	

}
