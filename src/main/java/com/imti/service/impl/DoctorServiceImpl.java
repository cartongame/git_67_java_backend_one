/**
 * @filename DoctorServiceImpl.java
 * @author lg
 * @date 2020年11月11日 下午4:08:40
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imti.bean.pojo.DeskInfo;
import com.imti.bean.pojo.Doctor;
import com.imti.mapper.DoctorDao;
import com.imti.service.DoctorService;
import com.imti.util.PageBean;
/**
 * 医生分页业务实现类
 *@comment
 *@author lg
 *@date 2020年11月11日 下午4:09:16
 *@modifyUser lg
 *@modifyDate 2020年11月11日 下午4:09:16
 *@modifyDesc  TODO
 *@version TODO
 */
@Service
public class DoctorServiceImpl implements DoctorService{
	//注入mapper接口
	@Autowired
	DoctorDao doctorDao;
	/**
	 * 查询所有科室
	 * @comment 
	 * @return
	 * @version 1.0
	 */
	 public List<DeskInfo> findAllDesk(){
		 return doctorDao.findAllDesk();
	 }
	/**
	 * 分页查询医生信息
	 * @comment 
	 * @param map
	 * @return
	 * @version 1.0
	 */
	public PageBean findAllDoc(Map map) {
		//查询医生总条数
		Doctor doc=(Doctor)map.get("doc");
		int totalCount=doctorDao.getDoctorCount(doc);
		//查询医生列表
		PageBean page=(PageBean)map.get("page");
		List<Doctor> list=doctorDao.findAll(map);
		//封装分页对象
		return new PageBean(page.getPage(), totalCount, page.getCurrNo(), list,"doc/page",getParams(doc));
	}
	/**
	 * 封装查询参数
	 * @comment 
	 * @param doc
	 * @return
	 * @version 1.0
	 */
	public  String getParams(Doctor doc){
		StringBuilder sb=new StringBuilder();
		if(doc.getDeskId()!=null){
			sb.append("&deskId=").append(doc.getDeskId());
		}
		if(doc.getJob()!=null){
			sb.append("&job=").append(doc.getJob());
		}
		if(doc.getPhone()!=null){
			sb.append("&phone=").append(doc.getPhone());
		}
		if(doc.getName()!=null){
			sb.append("&name=").append(doc.getName());
		}
		return sb.toString();
	}

}
