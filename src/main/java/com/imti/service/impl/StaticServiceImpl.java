/**
 * @filename StaticServiceImpl.java
 * @author lg
 * @date 2020年11月13日 上午9:59:02
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imti.bean.pojo.StaticVo;
import com.imti.mapper.StaticDao;
import com.imti.service.StaticService;

@Service
public class StaticServiceImpl implements StaticService {
	//注入mapper接口
	@Autowired
	StaticDao staticDao;
	/**
	 * 统计查询科室的贡献值
	 * @comment 
	 * @param date ：202010
	 * @return
	 * @version 1.0
	 */
	public List<StaticVo> findDeskMoney(String date){
		if(date==null || "".equals(date)){
			//上个月份
			date=staticDao.findPreMoney();
		}
		return staticDao.findDeskMoney(date);
	}

}
