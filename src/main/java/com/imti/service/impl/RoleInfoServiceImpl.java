/**
 * @filename RoleServiceImpl.java
 * @author sdb
 * @date 2020年8月4日 下午2:47:00
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.imti.bean.pojo.RoleInfo;
import com.imti.bean.pojo.UserInfo;
import com.imti.mapper.RoleInfoDao;
import com.imti.service.RoleInfoService;
import com.imti.util.PageBean;

@Service
@Transactional
/**
 * @文件名: RoleInfoServiceImpl.java
 * @类功能说明: 角色业务实现类
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月11日上午8:48:28
 * @修改说明:<br> 
 * <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月11日上午8:48:28</li> 
 *	 <li>内容: </li>
 * </pre>
 */
public class RoleInfoServiceImpl implements RoleInfoService{
//注入dao接口
	@Autowired
	RoleInfoDao roleInfoDao;
	/**
	 * @comment 查询用户角色
	 * @param user
	 * @version 1.0
	 */
	public void getUserRole(UserInfo userInfo,HttpServletRequest request){
	   //查询所有角色
		List<RoleInfo> roleInfoList=roleInfoDao.findAllRoleInfo();
		
		
		
		//查询已绑定角色   ,2,3,11,111,10,1,
				String roleIds=","+roleInfoDao.getRoleInfoByUserId(userInfo)+",";
				for(RoleInfo role:roleInfoList){
					if(roleIds!=null && roleIds.indexOf(","+role.getRoleId()+",")!=-1){
					  role.setCh("1");//复选框选中
					}else{
						role.setCh("0");
					}
				}
		
		
		
		request.setAttribute("roleInfoList", roleInfoList);
		
	}
	/**
	 * 分配角色
	 */
	
	public int setUserInfoRoleInfo(String roleIds, Integer userId) {
		
			int result=0;
			String role_ids[]=roleIds.split(",");
			roleInfoDao.deleteUserInfoRoleInfo(userId);
			
	        if(role_ids[0]==""){
	        	result=1;
	        }else{
	    		
	    			for(int i=0;i<role_ids.length;i++){
	    				Map<String, Object> map = new HashMap<String, Object>();
	    				map.put("userId", userId);
	    				map.put("roleId", Integer.parseInt(role_ids[i]));
	    			roleInfoDao.addUserInfoRoleInfo(map);
	    			result++;
	    		}
	    		
	        }
			
		
			
		
			return result;
		}
	
	public PageBean pageRoleInfo(Map<String,Object> map) {
		//1、查询总条数
		RoleInfo role=(RoleInfo)map.get("role");
				PageBean pageBean=(PageBean)map.get("page");
				Integer pageLimt=(pageBean.getPage()==null)?5:pageBean.getPage();
				Integer currNum= (pageBean.getCurrNo()==null)?1:pageBean.getCurrNo();
				int totalCount=roleInfoDao.getRoleCount(role);
				//2、查询列表
				PageBean page=new PageBean(pageLimt, currNum);
				map.put("page", page);
				List<RoleInfo> list=roleInfoDao.findRoleInfo(map);
				//3、封装pagebean
				PageBean npage=new PageBean(pageLimt, totalCount, currNum, list, "/role/page", getRoleParams(role));
				return npage;
	}
	
	public String getRoleParams(RoleInfo role){
		StringBuilder sb=new StringBuilder();
		if(StringUtils.isNotBlank(role.getRoleName())){
			sb.append("&roleName=").append(role.getRoleName());
		}
		if(StringUtils.isNotBlank(role.getRoleState())){
			sb.append("&roleState=").append(role.getRoleState());
		}
		
		return sb.toString();//&userName=34343&userState=1&beginTime=2020-01-03
	}
	/**
	 * 给角色分配权限
	 */
	public int setRoleAuth(String authIds, Integer roleId) {
		int result=0;
		String authId[]=authIds.split(",");
		roleInfoDao.deleteRoleAuth(roleId);
		
        if(authId[0]==""){
        	result=1;
        }else{
    		
    			for(int i=0;i<authId.length;i++){
    				Map<String, Object> map = new HashMap<String, Object>();
    				map.put("roleId", roleId);
    				map.put("authId", Integer.parseInt(authId[i]));
    			roleInfoDao.addRoleAuth(map);
    			result++;
    		}
    		
        }
		
	
		
	
		return result;
	}
	/**
	 * 角色名唯一验证
	 */
	public int getRoleName(RoleInfo roleInfo) {
		return roleInfoDao.getRoleName(roleInfo);
	}
	/**
	 * 新增角色保存
	 */
	public int saveRoleInfo(RoleInfo roleInfo) {
		return roleInfoDao.saveRoleInfo(roleInfo);
	}
	/**
	 * 角色启用禁用
	 */
	public int updateRoleState(RoleInfo roleInfo) {
		
		
		
		if(roleInfo.getRoleState().equals("1")){
			
			int res1=roleInfoDao.getRoleId1(roleInfo);
			int res2=roleInfoDao.getRoleId2(roleInfo);
			if(res1>0 || res2>0){
				return 0;
			}else{
				roleInfo.setRoleState("0");
			return roleInfoDao.updateRoleState(roleInfo);
			
			}
		}else{
			roleInfo.setRoleState("1");
			return roleInfoDao.updateRoleState(roleInfo);
		}

		
		
		
	}
	
	/**
	 * 用户组分配角色
	 */
	public int setGroupInfoRoleInfo(String roleIds, Integer groupId) {
		int result=0;
		String role_ids[]=roleIds.split(",");
		roleInfoDao.deleteGroupRoleInfo(groupId);
		
        if(role_ids[0]==""){
        	result=1;
        }else{
    		
    			for(int i=0;i<role_ids.length;i++){
    				Map<String, Object> map = new HashMap<String, Object>();
    				map.put("groupId", groupId);
    				map.put("roleId", Integer.parseInt(role_ids[i]));
    			roleInfoDao.addGroupRoleInfo(map);
    			result++;
    		}
    		
        }
		
	
		
	
		return result;
	}
	
}
