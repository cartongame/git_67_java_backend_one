package com.imti.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.imti.bean.pojo.GroupInfo;
import com.imti.bean.pojo.RoleInfo;
import com.imti.mapper.GroupInfoDao;
import com.imti.mapper.RoleInfoDao;
import com.imti.service.GroupInfoService;
import com.imti.util.PageBean;

/**
 * @文件名: GroupInfoServiceImpl.java
 * @类功能说明:
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月8日下午3:30:16
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月8日下午3:30:16</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
@Service
public class GroupInfoServiceImpl implements GroupInfoService {
	@Autowired
	GroupInfoDao groupInfoDao;
	@Autowired
	RoleInfoDao roleInfoDao;

	/**
	 * 用户组分页
	 */
	public PageBean pageGroupInfo(Map<String, Object> map) {
		// 1、查询总条数
		GroupInfo group = (GroupInfo) map.get("group");
		PageBean pageBean = (PageBean) map.get("page");
		Integer pageLimt = (pageBean.getPage() == null) ? 5 : pageBean.getPage();
		Integer currNum = (pageBean.getCurrNo() == null) ? 1 : pageBean.getCurrNo();
		int totalCount = groupInfoDao.getGroupCount(group);
		// 2、查询列表
		PageBean page = new PageBean(pageLimt, currNum);
		map.put("page", page);
		List<GroupInfo> list = groupInfoDao.findGroupInfo(map);
		// 3、封装pagebean
		PageBean npage = new PageBean(pageLimt, totalCount, currNum, list, "/group/page", getGroupParams(group));
		return npage;
	}

	/**
	 * @方法说明: 拼接group参数
	 */

	public String getGroupParams(GroupInfo group) {
		StringBuilder sb = new StringBuilder();
		if (StringUtils.isNotBlank(group.getGroupName())) {
			sb.append("&groupName=").append(group.getGroupName());
		}
		if (StringUtils.isNotBlank(group.getGroupState())) {
			sb.append("&groupState=").append(group.getGroupState());
		}

		return sb.toString();// &userName=34343&userState=1&beginTime=2020-01-03
	}

	/**
	 * 查询用户组角色
	 */
	public void getGroupRole(GroupInfo group, HttpServletRequest request) {
		// 查询所有角色
		List<RoleInfo> roleList = roleInfoDao.findAllRoleInfo();

		// 查询用户组已绑定角色 ,2,3,11,111,10,1,
		String roleIds = "," + groupInfoDao.getRoleInfoByGroupId(group) + ",";
		for (RoleInfo role : roleList) {
			if (roleIds != null && roleIds.indexOf("," + role.getRoleId() + ",") != -1) {
				role.setCh("1");// 复选框选中
			} else {
				role.setCh("0");
			}
		}

		request.setAttribute("roleList", roleList);

	}

	/**
	 * 用户组名和用户code唯一验证
	 */
	public int getGroup(GroupInfo group) {
		
		return groupInfoDao.getGroup(group);
	}

	/**
	 * 保存新增用户组
	 */
	public int saveGroupInfo(GroupInfo group) {
		return groupInfoDao.saveGroupInfo(group);
	}

	/**
	 * 启用禁用用户组
	 */
	public int updateGroupState(GroupInfo group) {
		
		
		
if(group.getGroupState().equals("1")){
			
	int res = groupInfoDao.getGroupId(group);
			if(res>0){
				return 0;
			}else{
				group.setGroupState("0");
			return groupInfoDao.updateGroupState(group);
			
			}
		}else{
			group.setGroupState("1");
			return groupInfoDao.updateGroupState(group);
		}
		
		
			

		}
	

	

}
