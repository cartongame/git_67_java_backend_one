/**
 * @filename UserInfoServiceImpl.java
 * @author lg
 * @date 2020年7月31日 上午10:15:35
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.imti.bean.pojo.GroupInfo;

import com.imti.bean.pojo.UserInfo;
import com.imti.mapper.UserInfoDao;
import com.imti.service.UserInfoService;
import com.imti.util.PageBean;

/**
 * @comment 用户业务实现类
 * @author sdb
 * @date 2020年7月31日 上午10:16:05
 * @modifyUser sdb
 * @modifyDate 2020年7月31日 上午10:16:05
 */
@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {
	// 注入userInfoDao接口
	@Autowired
	UserInfoDao userInfoDao;

	/**
	 * @comment 查询用户列表
	 * @param map:
	 *            user、 page
	 * @return 用户集合
	 * @version 1.0
	 */
	public PageBean pageUserInfo(Map<String,Object> map) {
		// 1、查询总条数
		UserInfo user = (UserInfo) map.get("user");
		PageBean pageBean = (PageBean) map.get("page");
		Integer pageLimt = (pageBean.getPage() == null) ? 5 : pageBean.getPage();
		Integer currNum = (pageBean.getCurrNo() == null) ? 1 : pageBean.getCurrNo();
		int totalCount = userInfoDao.getUserCount(user);
		// 2、查询列表
		PageBean page = new PageBean(pageLimt, currNum);
		map.put("page", page);
		List<UserInfo> list = userInfoDao.findUserInfo(map);
		// 3、封装pagebean
		PageBean npage = new PageBean(pageLimt, totalCount, currNum, list, "/user/page", getUserParams(user));
		return npage;
	}

	/**
	 * @comment 密码重置
	 * @param user
	 * @return
	 * @version 1.0
	 */
	public int updatePwd(UserInfo user) {
		return userInfoDao.updatePwd(user);
	}

	/**
	 * @comment 拼接user查询参数
	 * @param user
	 * @return
	 * @version 1.0
	 */
	public String getUserParams(UserInfo user) {
		StringBuilder sb = new StringBuilder();
		if (StringUtils.isNotBlank(user.getUserName())) {
			sb.append("&userName=").append(user.getUserName());
		}
		if (StringUtils.isNotBlank(user.getUserState())) {
			sb.append("&userState=").append(user.getUserState());
		}
		if (StringUtils.isNotBlank(user.getStart())) {
			sb.append("&start=").append(user.getStart());
		}
		if (StringUtils.isNotBlank(user.getEnd())) {
			sb.append("&end=").append(user.getEnd());
		}
		return sb.toString();// &userName=34343&userState=1&beginTime=2020-01-03
	}

	/**
	 * 管理员登陆
	 */

	public UserInfo findUserInfoByUserNameAndUserPwd(Map<String, Object> map) {
		return userInfoDao.findUserInfoByUserNameAndUserPwd(map);
	}

	/**
	 * 查询用户是否存在
	 */
	public int getUserName(UserInfo userInfo) {
		return userInfoDao.getUserName(userInfo);
	}

	/**
	 * 新增用户
	 */
	public int saveUserInfo(UserInfo userInfo) {
		return userInfoDao.setUserInfo(userInfo);
	}

	/**
	 * 查询所有用户组
	 */
	public List<GroupInfo> findAllUserGroup() {
		return userInfoDao.findAllUserGroup();
	}

	/**
	 * 逻辑删除用户
	 */
	public int deleteUser(UserInfo user) {

		int res1 = userInfoDao.deleteUser(user);
		int res2 = 0;
		int res3 = 0;
		if (res1 > 0) {

			res2 = userInfoDao.deleteUserRole(user);
			res3 = userInfoDao.deleteUserAuth(user.getUserId());
		}
		return res1 + res2 + res3;
	}

	/**
	 * 逻辑删除用户
	 */
	public int deleteUsers(String userIds) {
		int res1 = 0;
		int res2 = 0;
		int res3 = 0;
		String user_ids[] = userIds.split(",");

		for (int i = 0; i < user_ids.length; i++) {

			res1 = userInfoDao.deleteUserInfo(Integer.parseInt(user_ids[i]));

			if (res1 > 0) {

				res2 = userInfoDao.deleteUserInfoRoleInfo(Integer.parseInt(user_ids[i]));
				res3 = userInfoDao.deleteUserAuth(Integer.parseInt(user_ids[i]));
			}

		}

		return res1 + res2 + res3;
	}

	/**
	 * 分配用户权限
	 */
	public int setUserAuth(String authIds, Integer userId) {
		int result = 0;
		String authId[] = authIds.split(",");
		userInfoDao.deleteUserAuth(userId);

		if (authId[0] == "") {
			result = 1;
		} else {

			for (int i = 0; i < authId.length; i++) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("userId", userId);
				map.put("authId", Integer.parseInt(authId[i]));
				userInfoDao.addUserAuth(map);
				result++;
			}

		}

		return result;
	}

	/**
	 * 启用禁用用户
	 */
	public int updateUserState(UserInfo user) {
		
		if(user.getUserState().equals("0")){
			user.setUserState("1");
			
		}else{
			user.setUserState("0");	
		}

		return userInfoDao.updateUserState(user);
	}

	/**
	 * 查询用户用户组
	 */
	public void getUserGroup(UserInfo user, HttpServletRequest req) {
		List<GroupInfo> userGroupLists=userInfoDao.findAllUserGroup();
		Integer groupId=userInfoDao.getUserGroupByUserId(user);
		for (GroupInfo groupInfo : userGroupLists) {
			if(groupId!=null && groupInfo.getGroupId()==groupId){
				groupInfo.setSe("1");
		}else{groupInfo.setSe("0");}
		}
		
		req.setAttribute("userGroupLists", userGroupLists);
	}

	/**
	 * 分配用户组
	 */
	public int setUserGroup(UserInfo user) {
		
		return userInfoDao.setUserGroup(user);
	}

	
}
