/**
 * @filename StaticService.java
 * @author lg
 * @date 2020年11月13日 上午9:56:52
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.service;

import java.util.List;

import com.imti.bean.pojo.StaticVo;

/**
 * 统计查询业务接口
 *@comment
 *@author lg
 *@date 2020年11月13日 上午9:56:59
 *@modifyUser lg
 *@modifyDate 2020年11月13日 上午9:56:59
 *@modifyDesc  TODO
 *@version TODO
 */
public interface StaticService {

	/**
	 * 统计查询科室的贡献值
	 * @comment 
	 * @param date ：202010
	 * @return
	 * @version 1.0
	 */
	public List<StaticVo> findDeskMoney(String date);

}
