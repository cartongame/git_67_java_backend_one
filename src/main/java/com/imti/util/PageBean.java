
package com.imti.util;

import java.util.List;

/**
 * @文件名: PageBean.java
 * @类功能说明: 分页工具类
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月14日下午5:18:25
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月14日下午5:18:25</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
public class PageBean {

	// 每页显示条数
	private Integer page = 10;
	// 总条数
	private Integer totalNum = 0;
	// 当前页码
	private Integer currNo = 1;
	// 总页数
	private Integer totalPage = 0;
	// 查询下标位置，limit 中的第一个参数
	@SuppressWarnings("unused")
	private Integer pageIndex = 0;

	// 存储返回的集合对象
	private List<?> resultList;
	// 请求地址, user/list.action
	private String url;
	// 请求参数(搜索条件参数的封装), &userCode=admin&userType=1&userState=1
	private String params;

	public PageBean() {

	}

	/**
	 * @param page
	 *            每页显示条数
	 * @param currNo
	 *            当前页
	 */
	public PageBean(Integer page, Integer currNo) {
		this.page = page;
		this.currNo = currNo;
	}

	/**
	 * @param page
	 *            每页显示条数
	 * @param totalNum
	 *            总条数
	 * @param currNo
	 *            当前页
	 * @param resultList
	 *            结果集
	 * @param url
	 *            请求地址
	 * @param params
	 *            请求参数
	 */
	public PageBean(Integer page, Integer totalNum, Integer currNo, List<?> resultList, String url, String params) {
		super();
		this.page = page;
		this.totalNum = totalNum;
		this.currNo = currNo;
		this.resultList = resultList;
		this.url = url;
		this.params = params;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public Integer getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}

	// 当前页码
	public Integer getCurrNo() {
		if (currNo == 0) {
			currNo = 1;
		}
		return currNo;
	}

	public void setCurrNo(Integer currNo) {
		this.currNo = currNo;
	}

	// 总页数
	public Integer getTotalPage() {
		totalPage = (totalNum % page == 0) ? (totalNum / page) : (totalNum / page) + 1;
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	// 查询下标位置
	public Integer getPageIndex() {
		return page * (currNo - 1);
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public List<?> getResultList() {
		return resultList;
	}

	public void setResultList(List<?> resultList) {
		this.resultList = resultList;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

}
