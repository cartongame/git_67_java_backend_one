
package com.imti.util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * @文件名: UserAuthFilter.java
 * @类功能说明: 用户权限过滤器
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月13日上午10:29:02
 * @修改说明:<br>
 * 
 *            <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月13日上午10:29:02</li> 
 *	 <li>内容: </li>
 *            </pre>
 */
//@Component
public class UserAuthFilter implements Filter {

	public void destroy() {

	}


	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;

		// 判断用户是否登录
		Object obj = request.getSession().getAttribute("currentUserInfo");

		String path = request.getServletPath();
		//截取请求地址中的5中方法
		/*request.getContextPath(); //获取项目名称:
		request.getRealPath(""); //
		request.getServletPath(); //
		request.getRequestURI(); //
		request.getRequestURL(); //*/
		if (obj == null && !path.endsWith("/login.jsp") && !path.endsWith("/user/login")
				&& !path.endsWith("/images.jsp") && !path.endsWith(".css") && !path.endsWith(".js")
				&& !path.endsWith(".png") && !path.endsWith(".gif")) {// session
																		// 过期或没有登录
			// 跳转到登录页面

			resp.setContentType("text/html; charset=UTF-8");
			resp.getWriter().println("<script>location.href='" + request.getContextPath() + "/login.jsp';</script>");
			return;
		}
		if (obj != null && path.endsWith("/auth/welcome")) {
			String urls = (String) request.getSession().getAttribute("authurls");

			if (urls == "") {
				resp.setContentType("text/html; charset=UTF-8");
				resp.getWriter().println(
						"<script>alert('无权访问');location.href='" + request.getContextPath() + "/login.jsp';</script>");
				return;
			}

		}
		if (obj != null && path.endsWith("/page")) {
			String urls = request.getSession().getAttribute("authurls") + "";
			if (!(urls.indexOf(path) != -1)) {
				resp.setContentType("text/html; charset=UTF-8");
				resp.getWriter().println("<script>alert('无权访问')</script>");
				return;
			}
		}
		if ((obj != null && path.endsWith("/user/deleteUsers")) 
				|| (obj != null && path.endsWith("/user/deleteUser"))
				|| (obj != null && path.endsWith("/user/addUserInfo"))
				|| (obj != null && path.endsWith("/role/getUserRole"))
				|| (obj != null && path.endsWith("/auth/findUserInfoAuthInfo"))
				|| (obj != null && path.endsWith("/user/resetPwd"))
				|| (obj != null && path.endsWith("/user/updateUserState"))
				|| (obj != null && path.endsWith("/user/getUserGroup"))
				|| (obj != null && path.endsWith("/role/addRoleInfo"))
				|| (obj != null && path.endsWith("/role/updateRoleState"))
				|| (obj != null && path.endsWith("/group/addGroupInfo"))
				|| (obj != null && path.endsWith("/group/getGroupRole"))
				|| (obj != null && path.endsWith("/group/updateGroupState"))
				|| (obj != null && path.endsWith("/auth/addAuthInfo"))
				|| (obj != null && path.endsWith("/auth/findRoleInfoAuthInfo"))) {
			String urls = request.getSession().getAttribute("authurls") + "";
			if (!(urls.indexOf(path) != -1)) {
				resp.setContentType("text/html; charset=UTF-8");
				resp.getWriter().println("<script>alert('无权访问')</script>");
				return;
			}
		}
		chain.doFilter(req, resp);

	}


	public void init(FilterConfig arg0) throws ServletException {

	}

}
