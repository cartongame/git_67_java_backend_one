/**
 * @filename RoleController.java
 * @author sdb
 * @date 2020年8月4日 下午2:51:35
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.controller;

import java.util.HashMap;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.imti.bean.pojo.RoleInfo;

import com.imti.bean.pojo.UserInfo;
import com.imti.service.RoleInfoService;
import com.imti.util.PageBean;

import net.sf.json.JSONObject;
@Controller
@RequestMapping("/role")
public class RoleInfoController {
   //注入service
	@Autowired
	RoleInfoService roleInfoService;
	/**
	 * @comment 查询用户角色
	 * @param user
	 * @version 1.0
	 */
	@RequestMapping("/getUserRole")
	public String getUserRole(UserInfo user,HttpServletRequest req){
		roleInfoService.getUserRole(user, req);
		return "user-role";
	}
	
	/**
	 * @方法名: setUserInfoRoleInfo
	 * @方法说明: 分配角色
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月6日下午5:18:49
	 * @param roleIds
	 * @param userId
	 * @return
	 * @return: JSONObject
	 */
	@RequestMapping("/setUserInfoRoleInfo")
	@ResponseBody
	public JSONObject setUserInfoRoleInfo(String roleIds,Integer userId) {
		JSONObject jsobject = new JSONObject();
		int result = roleInfoService.setUserInfoRoleInfo(roleIds,userId);
		if(result>0){
			jsobject.put("msg", true);
		}else{
			jsobject.put("msg", false);
		}
		return jsobject;
	}
	/**
	 * @方法名: setGroupInfoRoleInfo
	 * @方法说明: 分配角色
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月6日下午5:18:49
	 * @param roleIds
	 * @param userId
	 * @return
	 * @return: JSONObject
	 */
	@RequestMapping("/setGroupInfoRoleInfo")
	@ResponseBody
	public String setGroupInfoRoleInfo(String roleIds,Integer groupId) {
		
		int result = roleInfoService.setGroupInfoRoleInfo(roleIds,groupId);
		return result > 0 ? "1" : "0";
	}
	/**
	 * @方法名: pageUserInfo
	 * @方法说明: 分页查询角色列表
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月6日上午8:55:49
	 * @param user
	 * @param page
	 * @param request
	 * @return
	 * @return: String
	 */
	
	@RequestMapping("/page")
	public String pageRoleInfo(RoleInfo role, PageBean page, HttpServletRequest request) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("role", role);
		map.put("page", page);
		PageBean p = roleInfoService.pageRoleInfo(map);
		request.setAttribute("page", p);
		return "/role-list";
	}
	
	@RequestMapping("/setRoleAuth")
	@ResponseBody
	public String setRoleAuth(String authIds,Integer roleId) {
		
		int result = roleInfoService.setRoleAuth(authIds,roleId);
		return result > 0 ? "1" : "0";
	}
	/**
	 * @方法名: addRoleInfo
	 * @方法说明: 角色新增页面
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月9日下午7:41:51
	 * @return
	 * @return: String
	 */
	@RequestMapping("/addRoleInfo")

	public String addRoleInfo() {

		
		return "role-add";

	}
	/**
	 * @方法名: getRoleName
	 * @方法说明: 角色名唯一验证
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:32:06
	 * @param roleInfo
	 * @return
	 * @return: String
	 */
	@RequestMapping("/getRoleName")
	@ResponseBody
	public String getRoleName(RoleInfo roleInfo) {

		int res = roleInfoService.getRoleName(roleInfo);
		return res > 0 ? "1" : "0";

	}
	/**
	 * @方法名: saveRoleInfo
	 * @方法说明: 新增角色保存
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:32:24
	 * @param roleInfo
	 * @return
	 * @return: String
	 */
	@RequestMapping("/saveRoleInfo")
	@ResponseBody
	public String saveRoleInfo(RoleInfo roleInfo) {

		int res = roleInfoService.saveRoleInfo(roleInfo);
		return "redirect:/role/page?res=" + res;

	}
	/**
	 * @方法名: updateRoleState1
	 * @方法说明:启用禁用角色
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:54:30
	 * @param roleInfo
	 * @return
	 * @return: String
	 */
	@RequestMapping("/updateRoleState")
	@ResponseBody
	public String updateRoleState(RoleInfo roleInfo) {
		return roleInfoService.updateRoleState(roleInfo) + "";
	}
	
}
