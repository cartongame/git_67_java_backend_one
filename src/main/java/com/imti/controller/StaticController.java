/**
 * @filename StaticController.java
 * @author lg
 * @date 2020年11月13日 上午10:04:03
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.imti.bean.pojo.StaticVo;
import com.imti.service.StaticService;
@Controller
public class StaticController {
//注入service
	@Autowired
	StaticService staticService;
	/**
	 * 统计查询科室的贡献值
	 * @comment 
	 * @param date ：202010
	 * @return
	 * @version 1.0
	 */
	@RequestMapping("/findDeskMoney")
	public String findDeskMoney(String date,HttpServletRequest request){
		List<StaticVo>  list=staticService.findDeskMoney(date);
	    //['骨科', '儿科']
		List deskKey=new ArrayList();
		//[150.00,320.00]
		List deskVule=new ArrayList();
		for(StaticVo s:list){
			deskKey.add("'"+s.getName()+"'");
			deskVule.add(s.getTotal());
		}
		request.setAttribute("deskKey", deskKey);
		request.setAttribute("deskVule", deskVule);
		return "desk-money";
	}
	
}
