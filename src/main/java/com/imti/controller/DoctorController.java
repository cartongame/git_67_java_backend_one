/**
 * @filename DoctorController.java
 * @author lg
 * @date 2020年11月11日 下午4:24:39
 * @version 1.0
 * Copyright (C) 2020 
 */

package com.imti.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.imti.bean.pojo.Doctor;
import com.imti.service.DoctorService;
import com.imti.util.PageBean;
/**
 * 医生业务控制类
 *@comment
 *@author lg
 *@date 2020年11月11日 下午4:24:54
 *@modifyUser lg
 *@modifyDate 2020年11月11日 下午4:24:54
 *@modifyDesc  TODO
 *@version TODO
 */
@Controller
@RequestMapping("/doc")
public class DoctorController {
//注入service接口
	@Autowired
	DoctorService doctorService;
	
	@RequestMapping("/page")
	public String findAllDoc(Doctor doc,PageBean page,HttpServletRequest request) {
		HashMap map=new HashMap();
		map.put("doc", doc);
		map.put("page", page);
		PageBean pageBean=doctorService.findAllDoc(map);
		request.setAttribute("page", pageBean);
		request.setAttribute("deskList", doctorService.findAllDesk());
		return "doc-list";
	}

}
