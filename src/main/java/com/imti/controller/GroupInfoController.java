package com.imti.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.imti.bean.pojo.GroupInfo;
import com.imti.service.GroupInfoService;
import com.imti.util.PageBean;

/**@文件名: GroupInfoController.java
 * @类功能说明: 
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月8日下午2:55:11
 * @修改说明:<br> 
 * <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月8日下午2:55:11</li> 
 *	 <li>内容: </li>
 * </pre>
 */
@Controller
@RequestMapping("/group")
public class GroupInfoController {
	
	@Autowired
	GroupInfoService groupInfoService;

	
	/**
	 * @方法名: pageUserInfo
	 * @方法说明: 分页查询用户组列表
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月8日下午3:19:44
	 * @param user
	 * @param page
	 * @param request
	 * @return
	 * @return: String
	 */
	@RequestMapping("/page")
	public String pageGroupInfo(GroupInfo group, PageBean page, HttpServletRequest request) {



		Map<String,Object> map = new HashMap<String,Object>();
		map.put("group", group);
		map.put("page", page);
		PageBean p = groupInfoService.pageGroupInfo(map);
		request.setAttribute("page", p);
		return "/group-list";
	}
	/**
	 * @方法名: getGroupRole
	 * @方法说明: 获取用户组角色
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月10日上午12:14:26
	 * @param group
	 * @param req
	 * @return
	 * @return: String
	 */
	@RequestMapping("/getGroupRole")
	public String getGroupRole(GroupInfo group,HttpServletRequest req){
		groupInfoService.getGroupRole(group, req);
		return "group-role";
	}
	/**
	 * @方法名: addGroupInfo
	 * @方法说明: 
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月10日上午12:15:07
	 * @return
	 * @return: String
	 */
	@RequestMapping("/addGroupInfo")

	public String addGroupInfo() {

		
		return "group-add";

	}
	/**
	 * @方法名: getGroup
	 * @方法说明: 用户组名和用户code唯一验证
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:32:06
	 * @param GroupInfo
	 * @return
	 * @return: String
	 */
	@RequestMapping("/getGroup")
	@ResponseBody
	public String getGroup(GroupInfo group) {

		int res = groupInfoService.getGroup(group);
		return res > 0 ? "1" : "0";

	}
	
	@RequestMapping("/saveGroupInfo")
	@ResponseBody
	public String saveGroupInfo(GroupInfo group) {

		int res = groupInfoService.saveGroupInfo(group);
		return "redirect:/group/page?res=" + res;

	}
	/**
	 * @方法名: updateGroupState1
	 * @方法说明: 启用停用用户组
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月9日下午6:19:34
	 * @param group
	 * @return
	 * @return: String
	 */
	@RequestMapping("/updateGroupState")
	@ResponseBody
	public String updateGroupState(GroupInfo group) {
		return groupInfoService.updateGroupState(group) + "";
	}
	

}
