
package com.imti.controller;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.imti.bean.pojo.GroupInfo;

import com.imti.bean.pojo.UserInfo;
import com.imti.service.UserInfoService;
import com.imti.util.PageBean;

import net.sf.json.JSONObject;

/**
 * @文件名: UserInfoController.java
 * @类功能说明: 用户控制层
 * @作者: ShiDaBao
 * @Email: 1471262032@qq.com
 * @日期: 2020年8月13日上午9:46:10
 * @修改说明:<br> 
 * <pre>
 * 	 <li>作者: ShiDaBao</li> 
 * 	 <li>日期: 2020年8月13日上午9:46:10</li> 
 *	 <li>内容: </li>
 * </pre>
 */
@Controller
@RequestMapping("/user")
public class UserInfoController {
	// 注入userService
	@Autowired
	UserInfoService userInfoService;

	/**
	 * @方法名: pageUserInfo
	 * @方法说明: 分页查询用户列表
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月13日上午9:47:43
	 * @param user
	 * @param page
	 * @param request
	 * @return
	 * @return: String
	 */
	
	@RequestMapping("/page")
	public String pageUserInfo(UserInfo user, PageBean page, HttpServletRequest request) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("user", user);
		map.put("page", page);
		PageBean p = userInfoService.pageUserInfo(map);
		request.setAttribute("page", p);
		return "/user-list";
	}

	/**
	 * @方法名: resetUserPwd
	 * @方法说明: 密码重置
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月13日上午9:48:56
	 * @param user
	 * @return
	 * @return: String
	 */
	@RequestMapping("/resetPwd")
	@ResponseBody
	public String resetUserPwd(UserInfo user) {
		return userInfoService.updatePwd(user) + "";
	}
    /**
     * @方法名: deleteUser
     * @方法说明: 逻辑删除用户
     * @作者: ShiDaBao
     * @邮箱：1471262032
     * @日期: 2020年8月6日上午12:41:54
     * @param user
     * @return
     * @return: String
     */
	@RequestMapping("/deleteUser")
	@ResponseBody
	public String deleteUser(UserInfo user) {
		return userInfoService.deleteUser(user) + "";
	}
	/**
	 * @方法名: login
	 * @方法说明: 用户登陆
	 * @作者: ShiDaBao 
	 * @邮箱：1471262032
	 * @日期: 2020年8月3日下午7:22:59
	 * @param userInfo
	 * @param code
	 * @param request
	 * @return
	 * @throws Exception
	 * @return: String
	 * @throws Exception
	 */
	
	@RequestMapping("/login")
	public String login(UserInfo userInfo, String code, HttpServletRequest request,HttpServletResponse response,String remember) throws Exception {

		HttpSession session = request.getSession();
		
		String rand01 = (String) session.getAttribute("sRand");
		
			if (code == null) {
				return "redirect:../login.jsp";
			} else {
				if (code.equals(rand01)) {

					Map<String, Object> map = new HashMap<String, Object>();
					map.put("userName", userInfo.getUserName());
					map.put("userPwd", userInfo.getUserPwd());

					UserInfo currentUserInfo = userInfoService.findUserInfoByUserNameAndUserPwd(map);
					if (currentUserInfo != null) {
						
						session.setAttribute("currentUserInfo", currentUserInfo);
			
						return "redirect:/auth/index";
						
					} else {
						String userName = URLEncoder.encode(userInfo.getUserName(), "utf-8");
						return "redirect:../login.jsp?error=1" + "&userName=" + userName + "&userPwd="
								+ userInfo.getUserPwd() + "&code=" + code;
					}
				} else {
					String userName = URLEncoder.encode(userInfo.getUserName(), "utf-8");
					return "redirect:../login.jsp?error=2" + "&userName=" + userName + "&userPwd="
							+ userInfo.getUserPwd() + "&code=" + code;
				}

			}

		

	}
    /**
     * @方法名: getUserName
     * @方法说明: 用户名唯一验证
     * @作者: ShiDaBao
     * @邮箱：1471262032
     * @日期: 2020年8月9日下午8:09:18
     * @param userInfo
     * @return
     * @return: String
     */
	@RequestMapping("/getUserName")
	@ResponseBody
	public String getUserName(UserInfo userInfo) {

		int res = userInfoService.getUserName(userInfo);
		return res > 0 ? "1" : "0";

	}
    /**
     * @方法名: saveUserInfo
     * @方法说明: 保存新增用户
     * @作者: ShiDaBao
     * @邮箱：1471262032
     * @日期: 2020年8月9日下午8:09:05
     * @param userInfo
     * @return
     * @return: String
     */
	@RequestMapping("/saveUserInfo")
	@ResponseBody
	public String saveUserInfo(UserInfo userInfo) {

		int res = userInfoService.saveUserInfo(userInfo);
		return "redirect:/user/page?res=" + res;

	}
    /**
     * @方法名: addUserInfo
     * @方法说明: 用户新增页面
     * @作者: ShiDaBao
     * @邮箱：1471262032
     * @日期: 2020年8月9日下午8:08:48
     * @param request
     * @return
     * @return: String
     */
	@RequestMapping("/addUserInfo")

	public String addUserInfo(HttpServletRequest request) {

		List<GroupInfo> userGroupList = userInfoService.findAllUserGroup();
		request.setAttribute("userGroupList", userGroupList);
		return "user-add";

	}
	
	/**
	 * @方法名: login
	 * @方法说明: 员工退出
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月9日下午8:08:26
	 * @param request
	 * @return
	 * @return: String
	 */
		@RequestMapping("/logout")
		public String login(HttpServletRequest request) {
			
			HttpSession session = request.getSession();
			session.invalidate();
			
			return "redirect:../login.jsp";

		}
		/**
		 * @方法名: deleteUsers
		 * @方法说明: 批量删除用户
		 * @作者: ShiDaBao
		 * @邮箱：1471262032
		 * @日期: 2020年8月6日上午11:52:51
		 * @param userIds
		 * @return
		 * @return: String
		 */
		@RequestMapping("/deleteUsers")
		@ResponseBody
		public String deleteUsers(String userIds) {
			
			int result = userInfoService.deleteUsers(userIds);
			
			return result+"";
		}
		/**
		 * @方法名: setUserAuth
		 * @方法说明: 保存用户角色
		 * @作者: ShiDaBao
		 * @邮箱：1471262032
		 * @日期: 2020年8月8日上午11:48:13
		 * @param authIds
		 * @param userId
		 * @return
		 * @return: String
		 */
		@RequestMapping("/setUserAuth")
		@ResponseBody
		public String setUserAuth(String authIds,Integer userId) {
			
			int result = userInfoService.setUserAuth(authIds,userId);
			return result > 0 ? "1" : "0";
		}
		/**
		 * @方法名: updateUserState
		 * @方法说明: 启用禁用用户
		 * @作者: ShiDaBao
		 * @邮箱：1471262032
		 * @日期: 2020年8月9日下午6:19:34
		 * @param user
		 * @return
		 * @return: String
		 */
		@RequestMapping("/updateUserState")
		@ResponseBody
		public String updateUserState(UserInfo user) {
			return userInfoService.updateUserState(user) + "";
		}
		
		/**
		 * @comment 查询用户用户组
		 * @param user
		 * @version 1.0
		 */
		@RequestMapping("/getUserGroup")
		public String getUserGroup(UserInfo user,HttpServletRequest req){
			userInfoService.getUserGroup(user, req);
			return "user-group";
		}
		/**
		 * @方法名: setUserGroup
		 * @方法说明: 分配用户组
		 * @作者: ShiDaBao
		 * @邮箱：1471262032
		 * @日期: 2020年8月17日上午1:06:19
		 * @param  user
		 * @return
		 * @return: JSONObject
		 */
		@RequestMapping("/setUserGroup")
		@ResponseBody
		public JSONObject setUserGroup(UserInfo user) {
			JSONObject jsobject = new JSONObject();
			int result = userInfoService.setUserGroup(user);
			if(result>0){
				jsobject.put("msg", true);
			}else{
				jsobject.put("msg", false);
			}
			return jsobject;
		}
}
