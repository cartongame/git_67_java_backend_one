

package com.imti.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.imti.bean.pojo.AuthInfo;
import com.imti.bean.pojo.RoleInfo;
import com.imti.bean.pojo.UserInfo;
import com.imti.service.AuthInfoService;

import net.sf.json.JSONArray;
/**
 *@comment 权限业务控制类
 *@author sdb
 *@date 2020年7月30日 下午2:39:30
 *@modifyUser sdb
 *@modifyDate 2020年7月30日 下午2:39:30
 *@modifyDesc  TODO
 *@version TODO
 */
@Controller
@RequestMapping("/auth")
public class AuthInfoController {
//注入service接口
	@Autowired
	AuthInfoService authInfoService;
	/** 
	 * @comment 查询权限信息
	 * @param user
	 * @return
	 * @version 1.0
	 * @throws IOException 
	 */
	@RequestMapping("/index")
	public String findAuthInfo(UserInfo userInfo,HttpServletRequest request) {
		Map<String,Object> map=new HashMap<String,Object>();
		HttpSession session = request.getSession();
		UserInfo sessionUserInfo = (UserInfo) session.getAttribute("currentUserInfo");
		
		map.put("userId", sessionUserInfo.getUserId());
		map.put("groupId", sessionUserInfo.getGroupId());
		map.put("userName", sessionUserInfo.getUserName());
		
		
		List<AuthInfo> authList=authInfoService.findAuthInfo(map,request);
		
		request.setAttribute("authList", authList);
		return "index";
	}
	/**
	 * @方法名: welcome
	 * @方法说明: 欢迎页面
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:08:42
	 * @return
	 * @return: String
	 */
	@RequestMapping("/welcome")
	public String welcome() {
		return "welcome";
	}
	/**
	 * @方法名: allAuth
	 * @方法说明: 所有权限
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:08:57
	 * @param request
	 * @return
	 * @return: String
	 */
	@RequestMapping("/page")
	public String allAuth(HttpServletRequest request) {
		JSONArray authjsonArray=authInfoService.findAllAuthInfo();
		request.setAttribute("authjsonArray", authjsonArray);
		return "auth";
	}
	/**
	 * @方法名: findRoleInfoAuthInfo
	 * @方法说明: 用户所有权限
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:09:12
	 * @param roleInfo
	 * @param request
	 * @return
	 * @return: String
	 */
	@RequestMapping("/findRoleInfoAuthInfo")
	public String findRoleInfoAuthInfo(RoleInfo roleInfo,HttpServletRequest request) {
		JSONArray authjsonArrays=authInfoService.findRoleInfoAuthInfo(roleInfo);
		request.setAttribute("authjsonArrays", authjsonArrays);
		return "role-auth";
	}
	/**
	 * @方法名: addAuthInfo
	 * @方法说明: 新增菜单页面
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:09:35
	 * @return
	 * @return: String
	 */
	@RequestMapping("/addAuthInfo")
	public String addAuthInfo() {
		
		return "auth-add";
	}
	/**
	 * @方法名: getAuthName
	 * @方法说明: 查询权限名字是否存在
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月7日下午5:09:50
	 * @param userInfo
	 * @return
	 * @return: String
	 */
	@RequestMapping("/getAuthName")
	@ResponseBody
	public String getAuthName(AuthInfo authInfo) {

		int res = authInfoService.getAuthName(authInfo);
		return res > 0 ? "1" : "0";

	}
	/**
	 * @方法名: getAuthUrl
	 * @方法说明: 查询权限路径是否存在
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月8日上午12:19:49
	 * @param authInfo
	 * @return
	 * @return: String
	 */
	@RequestMapping("/getAuthUrl")
	@ResponseBody
	public String getAuthUrl(AuthInfo authInfo) {

		int res = authInfoService.getAuthUrl(authInfo);
		return res > 0 ? "1" : "0";

	}
	/**
	 * @方法名: saveAuthInfo
	 * @方法说明: 新增权限
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月8日上午12:18:07
	 * @param authInfo
	 * @return
	 * @return: String
	 */
	@RequestMapping("/saveAuthInfo")
	@ResponseBody
	public String saveAuthInfo(AuthInfo authInfo) {

		int res = authInfoService.saveAuthInfo(authInfo);
		 return res > 0 ? "1" : "0";

	}
	/**
	 * @方法名: findUserInfoAuthInfo
	 * @方法说明: 用户分配权限
	 * @作者: ShiDaBao
	 * @邮箱：1471262032
	 * @日期: 2020年8月8日上午11:22:09
	 * @param roleInfo
	 * @param request
	 * @return
	 * @return: String
	 */
	@RequestMapping("/findUserInfoAuthInfo")
	public String findUserInfoAuthInfo(UserInfo userInfo,HttpServletRequest request) {
		JSONArray authjsonArray=authInfoService.findUserInfoAuthInfo(userInfo);
		request.setAttribute("authjsonArray", authjsonArray);
		return "user-auth";
	}
	
	
}
