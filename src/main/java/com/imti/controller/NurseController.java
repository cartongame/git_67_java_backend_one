package com.imti.controller;

import com.imti.bean.pojo.Nurse;
import com.imti.service.NurseService;
import com.imti.util.PageBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/nurse")
public class NurseController {

    @Resource
    private NurseService nurseService;

    @RequestMapping("/page")
    public String pageNurseInfo(Nurse nurse, PageBean page, HttpServletRequest request) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("nurse", nurse);
        map.put("page", page);
        PageBean p = nurseService.pageNurseInfo(map);
        request.setAttribute("page", p);
        return "/nurse-list";
    }

    @GetMapping("/addNurse")
    public String showAddNursePage(HttpServletRequest request) {

        return "/nurse-add";
    }

    @RequestMapping("/saveNurse")
    public String saveNurseInfo(Nurse nurse, HttpServletRequest request) {

        nurseService.saveNurse(nurse);
        return "redirect:/nurse/page";
    }
}
