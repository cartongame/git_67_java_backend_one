<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">

				<div class="layui-form-item">
					<label for="title" class="layui-form-label"> <span
						class="x-red">*</span>资讯标题
					</label>
					<div class="layui-input-inline">
						<input type="text" id="title" name="title" required=""
							maxlength="16" lay-verify="title" autocomplete="off"
							class="layui-input" value="${noticeInfo.title}" >
					</div>
				</div>

				<div class="layui-form-item">
					<label for="context" class="layui-form-label"> <span
						class="x-red">*</span>内容
					</label>
					<div class="layui-input-inline">
						<input type="text" id="context" name="context" required=""
							maxlength="25" lay-verify="context" autocomplete="off"
							class="layui-input" value="${noticeInfo.context}">
					</div>
				</div>

				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui
				.use(
						[ 'form', 'layer' ],
						function() {
							$ = layui.jquery;
							var form = layui.form, layer = layui.layer;

							//监听提交
							form
									.on(
											'submit(add)',
											function(data) {
												console.log(data);
												var infoId= $("#infoId")
												.val();
												var title = $("#title")
												.val();
												var context = $("#context")
												.val();
											var url = "";
											var infoId = "${noticeInfo.infoId}";
											var msg = "";
											if(infoId == null || infoId == ""){
												url = "${pageContext.request.contextPath}/notice/saveNoticeInfo";
												msg = "增加成功";
												
											} else {
												url = "${pageContext.request.contextPath}/notice/updateNoticeInfo";
												msg = "修改成功";
											}

												$.post(url,{
													infoId : infoId,
													title : title,
													context : context
																},
																function(res) {

																	layer
																			.alert(
																					msg,
																					{
																						icon : 6
																					},
																					function() {
																						// 获得frame索引
																						var index = parent.layer
																								.getFrameIndex(window.name);
																						window.location.href
																						//关闭当前frame
																						parent.layer
																								.close(index);
																						window.parent.location
																								.reload();
																});

																})

												return false;

											});

						});


	</script>

</body>

</html>