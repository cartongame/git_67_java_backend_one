<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>快过期药品页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">快到期药品</a>
			<a> <cite>快过期药品列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	
	<div class="layui-card-body ">

		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th>药品ID</th>
					<th>品牌</th>
					<th>药品名称</th>
					<th>药品库存</th>
					<th>生产日期</th>
					<th>到期日期</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="product" varStatus="p">
			<tr>
				<td align="center"><c:out value="${product.productId}"></c:out></td>
				<td align="center"><c:out value="${product.storeId}"></c:out></td>
				<td align="center"><c:out value="${product.productName}"></c:out></td>
				<td align="center"><c:out value="${product.productInvent}"></c:out></td>
				<td align="center"><c:out value="${product.productDate}"></c:out></td>
				<td align="center"><c:out value="${product.suppDate}"></c:out></td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>

			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});

		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});
</script>
</html>
</body>
</html>
