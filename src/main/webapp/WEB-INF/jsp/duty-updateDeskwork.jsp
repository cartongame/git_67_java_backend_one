<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>分配科室</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
        <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
     </head>
    <body> 
        <div class="layui-fluid">
            <div class="layui-row">
            
                <form class="layui-form" method="post" action="saveDeskWork">   
                    <input type="hidden" name="docId" id="docId" value="${duty.docId}">
                    <input type="hidden" name="dutyId" id="dutyId" value="${duty.dutyId}">
					 <div class="layui-form-item">
						<label for="deskId" class="layui-form-label"> 
							<span  class="x-red">*</span>科室
						</label>
						<div class="layui-input-inline">
	                            <select id="deskId" name="deskId">
							    	<option value="" >无科室</option>
									<c:forEach items="${groupInfo}" var="g" varStatus="s">
										<option value="${g.groupId}" ${g.se=='1'?"selected='selected'":""}>${g.groupName}</option>
									</c:forEach>
								</select>
						</div>
					</div>
                                    
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label"></label>
	                       <button class="layui-btn" 
							onclick="checkForm()">
							<i class="layui-icon">修改</i>
						</button>
                    </div>
                    
                </form>
            </div>
        </div>
        <script>
        layui.use(['form', 'layer','jquery'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

        });
            function checkForm(){
            	var deskId=$("#deskId").val();
            	var docId=$("#docId").val();
            	var dutyId=$("#dutyId").val();
            	if(deskId !=null && deskId!=''){
            		$.post( "${pageContext.request.contextPath}/duty/saveDeskWork",
            		{
            			deskId : deskId , docId : docId , dutyId : dutyId
            		},
            		function(res){
            			if(res == 1){
            				layer.msg('成功', {
         						icon : 1,
         						time : 2000
         					},
         					function() {
        						// 获得frame索引
        						var index = parent.layer.getFrameIndex(window.name);
        						//关闭当前frame
        						parent.layer.close(index);
        						window.parent.location.reload();
        					});
            			}else if(res==2){
            				layer.msg('网络异常', {
         						icon : 1,
         						time : 2000
         					});
            			}
            		});
            	}else{
            		 layer.msg('请选择科室', {
 						icon : 2,
 						time : 1000
 					});
            	}
           }
            
                
            
           
</script>
    </body>

</html>