<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>就诊卡页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
	</div>
	<div class="layui-card-body ">
			<ul>
				<li>卡号：
					<span>
						<c:out value="${card.cardNum}"></c:out>
					</span>
				</li>
				
				<li>二维码地址：
					<span>
						<c:out value="${card.qrCode}"></c:out>
					</span>
				</li>
				<li>身份证号：
					<span>
						<c:out value="${card.cardCode}"></c:out>
					</span>
				</li>
				<li>余额：
					<span>
						<c:out value="${card.money}"></c:out>
					</span>
				</li>
			</ul>
		</div>
		<div class="layui-card-body ">
		</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
</body>
</html>
