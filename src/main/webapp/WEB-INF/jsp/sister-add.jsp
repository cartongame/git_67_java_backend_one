<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增护士长</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">
				
				<div class="layui-form-item">
					<label for="nurseName" class="layui-form-label"> <span
						class="x-red">*</span>姓名
					</label>
					<div class="layui-input-inline">
						<input type="text" id="nurseName" 
							name="nurseName" required="" maxlength="20" lay-verify="nurseName"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="sex" class="layui-form-label">
					<span class="x-red">*</span>性别
					</label>
					<div style="width: 180px" class="layui-input-inline">
						<select name="sex" id="sex">
							<option value="1">男</option>
							<option value="0">女</option>
						</select>
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="phone" class="layui-form-label"> <span
						class="x-red">*</span>手机号
					</label>
					<div class="layui-input-inline">
						<input type="text" id="phone" onblur="validatePhone()"
							name="phone" required="" maxlength="11" lay-verify="phone"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="idCard" class="layui-form-label"> <span
						class="x-red">*</span>身份证号
					</label>
					<div class="layui-input-inline">
						<input type="text" id="idCard" onblur="validateCard()"
							name="idCard" required="" maxlength="18" lay-verify="idCard"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="pwd" class="layui-form-label"> <span
						class="x-red">*</span>密码
					</label>
					<div class="layui-input-inline">
						<input type="text" id="pwd"
							name="pwd" required="" maxlength="12" lay-verify="pwd"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="email" class="layui-form-label"> <span
						class="x-red">*</span>邮箱
					</label>
					<div class="layui-input-inline">
						<input type="text" id="email" onblur="validateEmail()"
							name="email" required="" maxlength="20" lay-verify="email"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="birthday" class="layui-form-label"> <span
						class="x-red">*</span>出生日期
					</label>
					<div class="layui-input-inline">
						<input type="date" id="birthday" name="birthday" required=""
							lay-verify="birthday" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="nurseConcat" class="layui-form-label"> <span
						class="x-red">*</span>紧急联系人
					</label>
					<div class="layui-input-inline">
						<input type="text" id="nurseConcat"
							name="nurseConcat" required="" maxlength="10" lay-verify="nurseConcat"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="tel" class="layui-form-label"> <span
						class="x-red">*</span>联系方式
					</label>
					<div class="layui-input-inline">
						<input type="text" id="tel" onblur="validateTel()"
							name="tel" required="" maxlength="11" lay-verify="tel"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="intro" class="layui-form-label"> <span
						class="x-red">*</span>个人介绍
					</label>
					<div class="layui-input-inline">
						<textarea rows="2" cols="30" id="intro"
							name="intro" required="" maxlength="100" lay-verify="intro"
							autocomplete="off">
						</textarea>
					</div>
				</div>

				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui.use([ 'form', 'layer' ],function() {
		$ = layui.jquery;
		var form = layui.form, layer = layui.layer;

		//自定义验证规则
 		form.verify({
 			
 			 nurseName : function(value){
 				if (value.length < 1) {
					return '姓名不能为空';
				}
 				if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
               	 return '不能有特殊字符';
               	 }
 			},
 			
 			phone :  [ /^1[3456789]\d{9}$/, '手机号不规范' ] ,
 			
 			pwd : [ /(.+){6,12}$/, '密码必须6到12位' ],
 			
 			idCard : [ /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/, '身份证号不规范' ] ,
 		
			email : [/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,'邮箱格式不正确'],
			 
			birthday : function(value){
				 if (value.length < 1) {
						return '生日不能为空';
					}
				},
			nurseConcat : function(value){
				 if (value.length < 1) {
						return '姓名不能为空';
					}
				 if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
	               	 return '不能有特殊字符';
	               	 }
				},
			tel : [ /^1[3456789]\d{9}$/, '手机号不规范' ] , 
			
			intro : function(value) {
			if (value.length < 5) {
				return '个人介绍最少5个汉字';
			}
			if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
              	 return '不能有特殊字符';
              	 }
             	  if(/(^\_)|(\__)|(\_+$)/.test(value)){
             	  	return '首尾不能出现下划线';
             	 	}
             	 		if(/^\d+\d+\d$/.test(value)){
             	 	return '不能全为数字';
            }
		}, 
		});
		//监听提交
		form.on('submit(add)',
		function(data) {
			console.log(data);
			var nurseName=$.trim($("#nurseName").val());
			var phone=$.trim($("#phone").val());
			var sex = $.trim($("#sex").val());
			var email = $.trim($("#email").val());
			var birthday = $.trim($("#birthday").val());
			var nurseConcat = $.trim($("#nurseConcat").val());
			var tel = $.trim($("#tel").val());
			var intro = $.trim($("#intro").val());
			var idCard=$.trim($("#idCard").val());
			var nickName= nurseName ;
			var userName= phone ;
			var userPwd=$.trim($("#pwd").val());
			
			 if(tel == phone){
				layer.msg('手机号重复', {
					icon : 5,
					time : 2000
				});
				$("#tel").val("");
				return;
			}
			if(tel == userPwd || userPwd == phone){
			layer.msg('密码不能和手机号相同', {
				icon : 5,
				time : 2000
			});
			$("#pwd").val("");
			return;
		}
		  $.post("${pageContext.request.contextPath}/user/saveSister",
			{
			 nurseName : nurseName,
			 userName : userName,
			 nickName : nickName,
			 userPwd : userPwd,
			 idCard : idCard,
			 phone : phone,
			 sex : sex,
			 email : email,
			 birthday : birthday,
			 nurseConcat : nurseConcat,
			 tel : tel,
			 intro : intro
			},
			function(res){
				layer.alert("新增成功",
				{
					icon : 6
				},
				function() {
					// 获得frame索引
					var index = parent.layer.getFrameIndex(window.name);
					//关闭当前frame
					parent.layer.close(index);
					window.parent.location.reload();
				});
			}) 
			return false;
	});
	})
	
		//验证身份证号是否存在
		function validateCard() {
			var idCard = $.trim($("#idCard").val());
			if (!!idCard) {
				$.post("${pageContext.request.contextPath}/nurse/getNurseCard?idCard="
					+ idCard, function(res) {
				if (res == "1") {
					flag = false;
					layer.msg('身份证号已存在!', {
						icon : 5,
						time : 1000
					});
					$("#idCard").val("")
					return;
				}
			});

			}
		}
	
		//验证手机号是否存在
		function validatePhone() {
			var phone = $.trim($("#phone").val());
			if (!!phone) {
				$.post("${pageContext.request.contextPath}/user/getUserName?userName="
					+ phone, function(res) {
				if (res == "1") {
					flag = false;
					layer.msg('手机号已存在!', {
						icon : 5,
						time : 1000
					});
					$("#phone").val("")
					return;
				}
			});

			}
		}
		
		//紧急联系人手机号唯一验证
		function validateTel() {
			var tel = $.trim($("#tel").val());
			if (!!tel) {
				$.post("${pageContext.request.contextPath}/nurse/getCountOne?tel="
					+ tel, function(res) {
				if (res == "1") {
					flag = false;
					layer.msg('手机号已存在!', {
						icon : 5,
						time : 1000
					});
					$("#tel").val("")
					return;
				}
			});

			}
		}
		
		//验证邮箱是否存在
		function validateEmail() {
			var email = $.trim($("#email").val());
			if (!!email) {
				$.post("${pageContext.request.contextPath}/nurse/getCountOne?email="
					+ email, function(res) {
				if (res == "1") {
					flag = false;
					layer.msg('邮箱已存在!', {
						icon : 5,
						time : 1000
					});
					$("#email").val("")
					return;
				}
			});

			}
		}
		
			
		</script>

</body>

</html>