<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>调班</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
        <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
     </head>
    <body> 
        <div class="layui-fluid">
            <div class="layui-row">
                    <input type="hidden" name="dutyId" id=dutyId value="${duty.dutyId}">
                    
                    <div class="layui-form-item">
						<label for="docName" class="layui-form-label"> <span
							class="x-red">*</span>姓名
						</label>
						<div class="layui-input-inline">
							<input type="text" readonly="readonly" required=""
								lay-verify="docName" autocomplete="off"
								class="layui-input" value="${duty.docName}">
						</div>
					</div>
					
                     <div class="layui-form-item">
						<label for="dutyDate" class="layui-form-label"> 
						<span class="x-red">*</span>日期
						</label>
						<div class="layui-input-inline">
							<input type="date" id="dutyDate" name="dutyDate" required=""
								maxlength="20" lay-verify="dutyDate" autocomplete="off"
								class="layui-input">
						</div>
					</div>
					
					 <div class="layui-form-item">
						<label for="ampm" class="layui-form-label"> 
						<span  class="x-red">*</span>时段
						</label>
						<div class="layui-input-inline">
								<select id="ampm" name="ampm" style="width: 20">
		                            <option value="1">上午</option>
		                            <option value="2">下午</option>
		                            <option value="3">全天</option>
	                            </select>
						</div>
					</div>
	                     <button class="layui-btn" onclick="saveDutys()">
							<i class="layui-icon"></i>修改
						 </button>
            </div>
        </div>
        <script>
        layui.use([ 'laydate', 'form' ], function() {
    		var laydate = layui.laydate;
    		layer = layui.layer;
    	});
        
        function saveDutys(){
        	var dutyId=$.trim($("#dutyId").val());
        	var ampm=$("#ampm").val();
        	var dutyDate=$.trim($("#dutyDate").val());
        	if(dutyDate == null || dutyDate==''){
        		layer.msg("日期不能为空",
					{
						icon : 2
					});
        	}else{
        	$.post("${pageContext.request.contextPath}/duty/saveDutys",
        			{
        				dutyId : dutyId, ampm : ampm, dutyDate : dutyDate
        			},
        			function(res){
        				if (res > 0) {
							layer.msg("调班成功",
								{
									icon : 1,
									time : 1000
								},
							function() {
							// 获得frame索引
							var index = parent.layer.getFrameIndex(window.name);
							//关闭当前frame
							parent.layer.close(index);
							window.parent.location.reload();
						});
        			}else{
        				layer.msg("网络异常",
								{
									icon : 2,
									time : 1000
								});
        		}
        	});
       }
      }
   	
 
        
        
                
            </script>
    </body>

</html>