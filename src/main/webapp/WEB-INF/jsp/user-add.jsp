<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">
				<div class="layui-form-item">
					<label for="userName" class="layui-form-label"> <span
						class="x-red">*</span>用户名
					</label>
					<div class="layui-input-inline">
						<input type="text" id="userName" onblur="validateName()"
							name="userName" required="" maxlength="30" lay-verify="userName"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="userPwd" class="layui-form-label"> <span
						class="x-red">*</span>密码
					</label>
					<div class="layui-input-inline">
						<input type="password" id="userPwd" name="userPwd" required=""
							maxlength="16" lay-verify="userPwd" autocomplete="off"
							class="layui-input">
					</div>
				</div>


				<div class="layui-form-item">
					<label for="groupId" class="layui-form-label"> <span
						class="x-red">*</span>用户组名称
					</label>
					<div class="layui-input-inline">


						<select id="groupId">
						    <option value="">无用户组</option>
							<c:forEach items="${userGroupList}" var="g" varStatus="s">
								<option value="${g.groupId}">${g.groupName}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui
				.use(
						[ 'form', 'layer' ],
						function() {
							$ = layui.jquery;
							var form = layui.form, layer = layui.layer;

							//自定义验证规则
							form.verify({
								userName : function(value) {

									if (value.length < 5) {
										return '用户名至少得5个字符啊';
									}
								},
								userPwd : [ /(.+){6,12}$/, '密码必须6到12位' ],
							/*  repass: function(value) {
							    if ($('#L_pass').val() != $('#L_repass').val()) {
							        return '两次密码不一致';
							    }
							}  */
							});

							//监听提交
							form
									.on(
											'submit(add)',
											function(data) {
												console.log(data);
												var userName = $.trim($(
														"#userName").val());

												var userPwd = $.trim($(
														"#userPwd").val());
												var groupId = $.trim($(
														"#groupId").val());
												if (userName == null
														|| userName == '') {
													layer.msg('用户名不能为空!', {
														icon : 2,
														time : 1000
													});
												} else if (userPwd == null
														|| userPwd == '') {
													layer.msg('密码不能为空!', {
														icon : 2,
														time : 1000
													});
												} else {
													if (userName == userPwd) {
														layer.msg("用户名密码不能相同",
																{
																	icon : 2,
																	time : 1000
																})
													} else {
														$
																.post(
																		"${pageContext.request.contextPath}/user/saveUserInfo",
																		{
																			userName : userName,
																			userPwd : userPwd,
																			groupId : groupId
																		},
																		function(
																				res) {
																			//if(res=="1"){
																			//console.log("新增成功");
																			layer
																					.alert(
																							"增加成功",
																							{
																								icon : 6
																							},
																							function() {
																								// 获得frame索引
																								var index = parent.layer
																										.getFrameIndex(window.name);
																								//关闭当前frame
																								parent.layer
																										.close(index);
																								window.parent.location
																										.reload();
																							});
																			// }
																		})
													}
												}
												return false;

											});

						});

		//验证用户名是否存在
		function validateName() {
			var userName = $.trim($("#userName").val());

			if (!!userName) {
				$.post(
						"${pageContext.request.contextPath}/user/getUserName?userName="
								+ userName, function(res) {

							if (res == "1") {
								flag = false;
								layer.msg('用户已存在!', {
									icon : 5,
									time : 1000
								});
								$("#userName").val("")
								return;
							}
						});

			}
		}
		</script>

</body>

</html>