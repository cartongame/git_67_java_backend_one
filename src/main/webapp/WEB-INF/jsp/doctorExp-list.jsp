<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>实习医生审核资料列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">实习医生管理</a>
			<a> <cite>实习医生列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th><input type="checkbox" name="" lay-skin="primary">
					</th>
					<th>ID</th>
					<th>姓名</th>
					<th>性别</th>
					<th>手机号</th>
					<th>状态</th>
					<th>职位</th>
					<th>紧急联系人</th>
					<th>联系人电话</th>
					<th>审核</th>
				</tr>
			</thead>
	<tbody >
			<c:forEach items="${page.resultList}" var="doctorExpVo" varStatus="de">
			<tr>
				<td style="width: 20px;"><input type="checkbox"
					name="docId" value="${doctorExpVo.docId}" lay-skin="primary"></td>
				<td><c:out value="${doctorExpVo.docId}"></c:out></td>
				<td><c:out value="${doctorExpVo.name}"></c:out></td>
				<td><c:out value="${doctorExpVo.sex=='1'?'男':'女'}"></c:out></td>
				<td><c:out value="${doctorExpVo.phone}"></c:out></td>
				<td>
					<span class="layui-btn layui-btn-normal layui-btn-mini">
						待审核
					</span></td>
				<td><span class="layui-btn layui-btn-normal layui-btn-mini">实习医生</span></td>
				<td><c:out value="${doctorExpVo.concat}"></c:out></td>
				<td><c:out value="${doctorExpVo.tel}"></c:out></td>
				<td class="td-manage">
					<a title="通过" onclick="save_DoctorExp(this,'${doctorExpVo.docId}')"
						href="javascript:;"> <i class="layui-icon">&#xe642;</i>
					</a>
					<a title="不通过" onclick="del_DoctorExp(this,'${doctorExpVo.docId}')"
						href="javascript:;"> <i class="layui-icon">&#xe640;</i>
					</a>
				</td>
			</tr>
			
		</c:forEach>
	</tbody>
		</table>
			</div>
					<div class="layui-card-body ">
						<div class="page">
							<div>
								<jsp:include page="standard.jsp"></jsp:include>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});
	
	//审核通过
	function save_DoctorExp(obj,docId){
		var expState="1";
		window.location.href = "${pageContext.request.contextPath}/doctor/updateDoctorExpVo?docId="+docId+"&expState="+expState;
	}
	
	//审核不通过
	function del_DoctorExp(obj,docId){
		var expState="2";
		window.location.href = "${pageContext.request.contextPath}/doctor/updateDoctorExpVo?docId="+docId+"&expState="+expState;
	}
	

</script>
</html>
</body>
</html>
