<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<HTML>
<HEAD>
<TITLE>用户权限列表</TITLE>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/zTree_v3/css/demo.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/zTree_v3/css/zTreeStyle/zTreeStyle.css"
	type="text/css">
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery.ztree.excheck.js"></script>
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<!--
	<script type="text/javascript" src="../../../js/jquery.ztree.exedit.js"></script>
	-->
<SCRIPT type="text/javascript">
<!--
	var setting = {
		check : {
			enable : true,
			chkStyle : "checkbox"
			
		},
		callback : {
			onCheck : zTreeOnCheck
		},
		data : {
			simpleData : {
				enable : true
			}
		}
	};

	var zNodes = ${authjsonArray};
	$(document).ready(function(){  
		   $.fn.zTree.init($("#treeDemo"), setting, zNodes);
		   zTreeOnCheck();//加载页面触发默认显示已选择的权限 
		  });
	function zTreeOnCheck(event, treeId, treeNode) {
		var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
		var nodes = treeObj.getCheckedNodes(true);
		var authId = [];
		$.each(nodes, function(index, row) {
			authId.push(row.id);
		});
		authIds = authId.join(",");

		$("#authIds").val(authIds);
	};
	
	$(document).ready(function() {
		$.fn.zTree.init($("#treeDemo"), setting, zNodes);

	});
//-->
</SCRIPT>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"></script>

<script type="text/javascript">
	//用户分配权限
	function setUserAuth() {
		layui.use([ 'form', 'layer' ], function() {

			layer = layui.layer;

		});

		userId = "${param.userId}"
			authIds=$("#authIds").val();
		

		$.ajax({
			url : "../user/setUserAuth",
			data : {
				authIds : authIds,
				userId : userId
			},
			type : "post",
			dataType : "json",
			success : function(result) {
				if (result > 0) {

					layer.alert("分配成功", {
						icon : 6
					}, function() {
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);

					});

				}

				else {

					layer.alert("分配失败", {
						icon : 6
					}, function() {
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);
					});

				}
			}
		});

	}
</script>


</HEAD>

<BODY>
	<center>
		<div class="content_wrap">
			<div class="zTreeDemoBackground left">
				<ul id="treeDemo" class="ztree"></ul>
				<input type="hidden" id="authIds"></input>
			</div>
			<div class="layui-form-item">

				<label for="L_repass" class="layui-form-label"></label>
				<button class="layui-btn" lay-filter="add" lay-submit=""
					onclick="setUserAuth()">提交</button>
			</div>
		</div>
	</center>
</BODY>
</HTML>