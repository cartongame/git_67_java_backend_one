<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>医生列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">医生管理</a>
			<a> <cite>医生列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
			<form class="layui-form layui-col-space5" method="post" action="${pageContext.request.contextPath}/doc/page">
				<div class="layui-input-inline layui-show-xs-block">
					<select name="deskId" id="deskId" >
						<option value="">选择科室</option>
						<c:forEach items="${deskList}" var="desk">
						   <option value="${desk.deskId}" ${param.deskId==desk.deskId?'selected="selected"':''}>${desk.name}</option>
						</c:forEach>
					</select>
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<select name="job" id="job" >
						<option value="" >选择岗位</option>
						<option value="1" ${param.job=='1'?'selected="selected"':''}>主任医师</option>
						<option value="2" ${param.job=='2'?'selected="selected"':''}>副主任医师</option>
						<option value="3" ${param.job=='3'?'selected="selected"':''}>主治医师</option>
						<option value="4" ${param.job=='4'?'selected="selected"':''}>实习医生</option>
					</select>
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<input type="text" name="phone" id="phone" value="${param.phone}" maxlength="11" placeholder="请输入电话" autocomplete="off" class="layui-input">
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<input type="text" name="name" id="name" value="${param.name}" maxlength="20" placeholder="请输入用户名" autocomplete="off" class="layui-input">
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<button class="layui-btn" type="submit">
						<i class="layui-icon">&#xe615;</i>
					</button>
				</div>
		</form>
	</div>
	<div class="layui-card-header">
	   <%-- <c:if test="${fn:indexOf(authCodes, 'user-addUser')!=-1}"> --%>
			<button class="layui-btn"
				onclick="xadmin.open('添加用户','${pageContext.request.contextPath}/user/addUserInfo',400,400)">
				<i class="layui-icon"></i>添加
			</button>
		<%-- </c:if> --%>

	</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th><input type="checkbox" name="" lay-skin="primary"></th>
					<th>ID</th>
					<th>医生名称</th>
					<th>医生职位</th>
					<th>所属科室</th>
					<th>联系电话</th>
					<th>在职状态</th>
					<th>操作</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="doc" varStatus="s">
			<tr>
				<td style="width: 20px;"><input type="checkbox"
					name="docId" value="${doc.docId}" lay-skin="primary"></td>
				<td>${doc.docId}</td>
				<td>${doc.name}</td>
				<td><c:if test="${doc.job=='1'}">主任医师</c:if>
				<c:if test="${doc.job=='2'}">副主任医师</c:if>
				<c:if test="${doc.job=='3'}">主治医师</c:if>
				<c:if test="${doc.job=='4'}">实习医生</c:if></td>
				<td>${doc.deskName}</td>
				<td>${doc.phone}</td>
				<td class="td-status"><span class="layui-btn layui-btn-normal layui-btn-mini">${(doc.state=='1')?'在职':(doc.state=='0'?'退休':'离职')}</span></td>
				<td class="td-manage">
				   <a title="修改"
							onclick="xadmin.open('修改','${pageContext.request.contextPath}/user/getUserGroup?userId=${user.userId}',300,300)"
							href="javascript:;"> <i class="layui-icon">&#xe642;</i></a>
							
					<c:if test="${fn:indexOf(authCodes, 'reset_user_pwd')!=-1}">
						<a title="密码重置"
							onclick="userpsw_reset(this,'${user.userId}')"
							href="javascript:;"> <i class="layui-icon">&#xe669;</i></a>
					</c:if> 
					<a onclick="member_stop(this,'${user.userId}','${user.userState}')" title="${user.userState=='1'?'禁用':'启用'}"
							href="javascript:;"> <i class="layui-icon">${user.userState=='1'?'&#xe601;':'&#xe62f;'}</i>
					</a>
					</td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
			<c:if test="${not empty page.resultList}">
				<jsp:include page="standard.jsp"></jsp:include>
			</c:if>
			<c:if test="${empty page.resultList}">暂无数据</c:if>
			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});

		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});

	/*用户-启用停用*/
	function member_stop(obj, userId,userState) {
		
		if ($(obj).parents("tr").find(".td-status").find('span').html() == '已启用') {
			
	layer.confirm('确认要禁用吗？',
		function(index) {
			$.post("${pageContext.request.contextPath}/user/updateUserState",
					{
						userId : userId,userState:userState
					},
					function(res) {
						if (res > 0) {
							layer.alert("禁用成功",
								{
									icon : 1
								},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title','启用')
							
							$(obj).find('i').html('&#xe62f;');
							
							$(obj).parents("tr").find(".td-status").find('span').html('已禁用');

						} else {
							layer.msg('禁用失败!', {
								icon : 2,
								time : 1000
							});
						}
					});

				});
			} else {
				layer.confirm('确认要启用吗？',
					function(index) {
						$.post("${pageContext.request.contextPath}/user/updateUserState",
							{
								userId : userId,userState:userState
							},
						function(res) {
							if (res > 0) {
								layer.alert("启用成功",
									{
										icon : 1
									},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title','禁用')
							
							$(obj).find('i').html('&#xe601;');

							$(obj).parents("tr").find(".td-status").find('span').html('已启用');
						
						} else {
							layer.msg('启用失败!', {
								icon : 2,
								time : 1000
							});
						}
					});
			});
		}

	}

	/*用户-删除*/
	function member_del(obj, userId) {
		layer.confirm('确认要删除吗？', function(index) {
			//发异步删除数据
			$.post("${pageContext.request.contextPath}/user/deleteUser", {
				userId : userId
			}, function(res) {
				if (res > 0) {
					layer.msg('删除成功!', {
						icon : 1,
						time : 1000
					});
					$(obj).parents("tr").remove();
				} else {
					layer.msg('删除失败!', {
						icon : 1,
						time : 1000
					});
				}
			});

		});
	}
	//批量删除
	function delAll() {
		var user_id = [];
		$("input[name='userId']:checked").each(function() {
			user_id.push($(this).val());
		});
		if (user_id.length != 0) {

			userIds = user_id.join(",");
			// alert(userIds)
			/*  var data = tableCheck.getData();
			 alert(data) */
			layer.confirm('确认要删除吗？', function(index) {
				$.post("${pageContext.request.contextPath}/user/deleteUsers", {
					userIds : userIds
				}, function(res) {
					if (res > 0) {
						layer.msg('删除成功', {
							icon : 1
						});
						$(".layui-form-checked").not('.header').parents('tr')
								.remove();
					} else {
						layer.msg('删除失败', {
							icon : 1
						});
					}
				});

			});
		} else {
			layer.msg('请选择要删除的数据', {
				icon : 2
			});
		}
	}
</script>
</html>
</body>
</html>
