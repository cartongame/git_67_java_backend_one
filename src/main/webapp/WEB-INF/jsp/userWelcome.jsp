<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
<head>
<meta charset="UTF-8">
<title></title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>
<body>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
						<blockquote class="layui-elem-quote">
							欢迎登录： <span class="x-red"><c:out value="${currentUserInfo.nickName}"></c:out></span>！当前时间:<%=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date())%>
						</blockquote>
					</div>
				</div>
			</div>
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-header">我的系统</div>
					<div class="layui-card-body ">
						<ul
							class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
							
							<li class="layui-col-md2 layui-col-xs6">
								<a href="${pageContext.request.contextPath}/doctor/getDoctorDatum/page" class="x-admin-backlog-body">
										<h3>个人资料</h3>
										<p>
											<cite>1</cite>
										</p>
								</a>
							</li>
							<li class="layui-col-md2 layui-col-xs6">
								<a href="${pageContext.request.contextPath}/DayDetail/page" class="x-admin-backlog-body">
										<h3>我的贡献</h3>
										<p>
											<cite>2</cite>
										</p>
								</a>
							</li>
							<li class="layui-col-md2 layui-col-xs6">
							<a href="${pageContext.request.contextPath}/doctor/appraise/page" class="x-admin-backlog-body">
									<h3>我的评价</h3>
									<p>
										<cite>3</cite>
									</p>
							</a></li>
							<li class="layui-col-md2 layui-col-xs6 ">
								<a href="${pageContext.request.contextPath}/question/page" class="x-admin-backlog-body">
										<h3>问诊记录</h3>
										<p>
											<cite>4</cite>
										</p>
								</a>
							</li>
							
							<li class="layui-col-md2 layui-col-xs6">
								<a href="${pageContext.request.contextPath}/duty/page" class="x-admin-backlog-body">
										<h3>医生值班表</h3>
										<p>
											<cite>5</cite>
										</p>
								</a>
							</li>
							<li class="layui-col-md2 layui-col-xs6">
								<a href="${pageContext.request.contextPath}/nduty/page" class="x-admin-backlog-body">
										<h3>护士值班表</h3>
										<p>
											<cite>6</cite>
										</p>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<c:if test="${fn:indexOf(roles, 'director')!=-1}">
			<div class="layui-col-sm6 layui-col-md3">
				<div class="layui-card">
					<div class="layui-card-header">
						 <span class="layui-badge layui-bg-cyan layuiadmin-badge">员工管理</span>
					</div>
					<div class="layui-card-body  ">
						<p class="layuiadmin-big-font"></p>
						<button class="layui-btn" onclick="user()"	>
							<i class="layui-icon"></i>员工列表
						</button>
					</div>
				</div>
			</div>
			</c:if>
			<c:if test="${fn:indexOf(roles, 'boss')!=-1}">
			<div class="layui-col-sm6 layui-col-md3">
				<div class="layui-card">
					<div class="layui-card-header">
						 <span class="layui-badge layui-bg-cyan layuiadmin-badge">科室管理</span>
					</div>
					<div class="layui-card-body  ">
						<p class="layuiadmin-big-font"></p>
						<button class="layui-btn" onclick="group()"	>
							<i class="layui-icon"></i>科室列表
						</button>
					</div>
				</div>
			</div>
			<div class="layui-col-sm6 layui-col-md3">
				<div class="layui-card">
					<div class="layui-card-header">
						 <span class="layui-badge layui-bg-cyan layuiadmin-badge">角色管理</span>
					</div>
					<div class="layui-card-body  ">
						<p class="layuiadmin-big-font"></p>
						<button class="layui-btn" onclick="role()"	>
							<i class="layui-icon"></i>角色列表
						</button>
					</div>
				</div>
			</div>
			<div class="layui-col-sm6 layui-col-md3">
				<div class="layui-card">
					<div class="layui-card-header">
						 <span class="layui-badge layui-bg-cyan layuiadmin-badge">权限管理</span>
					</div>
					<div class="layui-card-body  ">
						<p class="layuiadmin-big-font"></p>
						<button class="layui-btn" onclick="auth()"	>
							<i class="layui-icon"></i>权限树
						</button>
					</div>
				</div>
			</div>
			<div class="layui-col-sm6 layui-col-md3">
				<div class="layui-card">
					<div class="layui-card-header">
						 <span class="layui-badge layui-bg-cyan layuiadmin-badge">医院管理</span>
					</div>
					<div class="layui-card-body  ">
						<p class="layuiadmin-big-font"></p>
						<button class="layui-btn" onclick="notice()"	>
							<i class="layui-icon"></i>资讯
						</button>
						<button class="layui-btn" onclick="hosp()"	>
							<i class="layui-icon"></i>资料
						</button>
					</div>
				</div>
			</div>
			</c:if>
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-header">系统信息</div>
					<div class="layui-card-body ">
						<table class="layui-table">
							<tbody>
								<tr>
									<th>xxx版本</th>
									<td>1.0.180420</td>
								</tr>
								<tr>
									<th>服务器地址</th>
									<td>x.xuebingsi.com</td>
								</tr>
								<tr>
									<th>操作系统</th>
									<td>WINNT</td>
								</tr>
								<tr>
									<th>运行环境</th>
									<td>Apache/2.4.23 (Win32) OpenSSL/1.0.2j mod_fcgid/2.3.9</td>
								</tr>
								<tr>
									<th>PHP版本</th>
									<td>5.6.27</td>
								</tr>
								<tr>
									<th>PHP运行方式</th>
									<td>cgi-fcgi</td>
								</tr>
								<tr>
									<th>MYSQL版本</th>
									<td>5.5.53</td>
								</tr>
								<tr>
									<th>ThinkPHP</th>
									<td>5.0.18</td>
								</tr>
								<tr>
									<th>上传附件限制</th>
									<td>2M</td>
								</tr>
								<tr>
									<th>执行时间限制</th>
									<td>30s</td>
								</tr>
								<tr>
									<th>剩余空间</th>
									<td>86015.2M</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-header">开发团队</div>
					<div class="layui-card-body ">
						<table class="layui-table">
							<tbody>
								<tr>
									<th>版权所有</th>
									<td>imti <a href="http://www.imti.com/" target="_blank">访问官网</a></td>
								</tr>
								<tr>
									<th>开发者</th>
									<td>imti</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<style id="welcome_style"></style>
			<div class="layui-col-md12"></div>
		</div>
	</div>
	</div>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});
	//用户
	function user(){
		window.location.href = "${pageContext.request.contextPath}/user/page";
	}
	//科室
	function group(){
		window.location.href = "${pageContext.request.contextPath}/group/page";
	}
	//角色
	function role(){
		window.location.href = "${pageContext.request.contextPath}/role/page";
	}
	
	//权限
	function auth(){
		window.location.href = "${pageContext.request.contextPath}/auth/page";
	}
	//资讯
	function notice(){
		window.location.href = "${pageContext.request.contextPath}/notice/page";
	}
	//资料
	function hosp(){
		window.location.href = "${pageContext.request.contextPath}/hosp/page";
	}
</script>
</body>
</html>