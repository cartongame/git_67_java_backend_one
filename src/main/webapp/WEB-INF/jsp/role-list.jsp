<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>角色列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">角色管理</a>
			<a> <cite>角色列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
						<form class="layui-form layui-col-space5">


							<div class="layui-input-inline layui-show-xs-block">
								<select name="roleState" id="roleState">
									<option value="">状态</option>
									<option value="1" ${param.roleState=='1'?'selected="selected"':''}>启用</option>
									<option value="0" ${param.roleState=='0'?'selected="selected"':''}>禁用</option>
								</select>
							</div>
							<div class="layui-input-inline layui-show-xs-block">
								<input type="text" name="roleName" id="roleName"
									value="${param.roleName}" placeholder="请输入角色名"
									autocomplete="off" class="layui-input">
							</div>
							<div class="layui-input-inline layui-show-xs-block">
								<button class="layui-btn" lay-submit="" lay-filter="sreach"
									onclick="sreachRole()">
									<i class="layui-icon">&#xe615;</i>
								</button>
							</div>
						</form>
					</div>
					<div class="layui-card-header">
						<c:if test="${fn:indexOf(authCodes, 'role-addRole')!=-1}">
							<button class="layui-btn"
								onclick="xadmin.open('添加角色','${pageContext.request.contextPath}/role/addRoleInfo',400,400)">
								<i class="layui-icon"></i>添加
							</button>
						</c:if>
					</div>
					<div class="layui-card-body ">
						<table class="layui-table layui-form">
							<thead>
								<tr>

									<th>角色ID</th>
									<th>角色名称</th>
									<th>用户code</th>
									<th>用户状态</th>
									<th>创建时间</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${page.resultList}" var="role" varStatus="s">
									<tr>

										<td style="width: 20px;">${role.roleId}</td>
										<td><c:out value="${role.roleName}"></c:out></td>
										<td><c:out value="${role.roleCode}"></c:out></td>
										<td class="td-status"><span
											class="layui-btn layui-btn-normal layui-btn-mini">${role.roleState=='1'?'已启用':'已禁用'}</span></td>

										<td>${fn:substring(role.createTime,0,19)}</td>
										<td class="td-manage">
												<a title="分配权限"
													onclick="xadmin.open('分配权限','${pageContext.request.contextPath}/auth/findRoleInfoAuthInfo?roleId=${role.roleId}',300,300)"
													href="javascript:;"> <i class="layui-icon">&#xe642;</i></a>
											
												<a onclick="member_stop(this,'${role.roleId}','${role.roleState}')" title="${role.roleState=='1'?'禁用':'启用'}"
													href="javascript:;"> <i class="layui-icon">${role.roleState=='1'?'&#xe601;':'&#xe62f;'}</i>

												</a>
											</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="layui-card-body ">
						<div class="page">
							<div>
								<jsp:include page="standard.jsp"></jsp:include>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;

		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});

		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});
     
	/*角色-启用停用*/
	function member_stop(obj, roleId,roleState) {
		
		if ($(obj).parents("tr").find(".td-status").find('span').html() == '已启用') {
			layer
					.confirm(
							'确认要禁用吗？',
							function(index) {
								$
										.post(
												"${pageContext.request.contextPath}/role/updateRoleState",
												{
													roleId : roleId,roleState:roleState
												},
												function(res) {
													if (res > 0) {
														
														layer
														.alert(
																"禁用成功",
																{
																	icon : 1
																},
														function() {
															// 获得frame索引
															var index = parent.layer
																	.getFrameIndex(window.name);
															//关闭当前frame
															parent.layer
																	.close(index);
															window.parent.location
																	.reload();
														});
														$(obj).attr('title',
																'启用')
														$(obj).find('i').html(
																'&#xe62f;');
														$(obj)
																.parents("tr")
																.find(
																		".td-status")
																.find('span')
																.html('已禁用');
														

													} else {
														layer.msg('禁用失败!', {
															icon : 2,
															time : 1000
														});
													}
												});

							});
		} else {
			layer
					.confirm(
							'确认要启用吗？',
							function(index) {
								$
										.post(
												"${pageContext.request.contextPath}/role/updateRoleState",
												{
													roleId : roleId,roleState:roleState
												},
												function(res) {
													if (res > 0) {
														layer
														.alert(
																"启用成功",
																{
																	icon : 1
																},
														function() {
															// 获得frame索引
															var index = parent.layer
																	.getFrameIndex(window.name);
															//关闭当前frame
															parent.layer
																	.close(index);
															window.parent.location
																	.reload();
														});
														$(obj).attr('title',
																'禁用')
														$(obj).find('i').html(
																'&#xe601;');

														$(obj)
																.parents("tr")
																.find(
																		".td-status")
																.find('span')
																.html('已启用');
													} else {
														layer.msg('启用失败!', {
															icon : 2,
															time : 1000
														});
													}
												});

							});
		}

	}
	
	//角色查询
	function sreachRole() {
		var roleName = $("#roleName").val();
		var roleState = $("#roleState").val();

		window.location.href = "${pageContext.request.contextPath}/role/page?roleName="
				+ roleName + "&roleState=" + roleState;
	}
</script>
</html>
</body>
</html>
