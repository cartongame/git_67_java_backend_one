<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>今日挂号患者页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">今日患者管理</a>
			<a> <cite>今日患者列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
		
		<div class="layui-card-header">
			<c:if test="${pr.state !='2' && pr.state !='3' && pr.state !='0'}">
				<button class="layui-btn" onclick="open_medicine()">
					<i class="layui-icon"></i>开处方
				</button>
			</c:if>
			<c:if test="${pr.state !='2' && pr.state !='3' && pr.state !='0'}">
				<button class="layui-btn" onclick="open_receipts()">
					<i class="layui-icon"></i>开单据
				</button>
			</c:if>
			<c:if test="${pr.state !='4' && pr.state !='2' && pr.state !='3'}">
				<button class="layui-btn" onclick="await()" >
					<i class="layui-icon"></i>等待
				</button>
			</c:if>
			
				<button class="layui-btn" onclick="next_bit()" >
					<i class="layui-icon"></i>下一位
				</button>
				<button class="layui-btn" onclick="cry_mark()" >
					<i class="layui-icon"></i>叫号
				</button>
				
			<button class="layui-btn" onclick="help()" 
				style="line-height: 1.6em; margin-top: 3px; float: right">
				<i class="layui-icon"></i>帮忙挂号
			</button>
		</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th>患者号</th>
					<th>挂号单</th>
					<th>姓名</th>
					<th>性别</th>
					<th>出生日期</th>
					<th>患者关系</th>
					<th>预约状态</th>
					<th>初复诊状态</th>
					<th>就诊卡</th>
				</tr>
			</thead>
	<tbody >
			<c:forEach items="${page.resultList}" var="pr" varStatus="r">
			<tr>
				<c:if test="${pr.state=='1'}">
					<input type="hidden" name="registId" value="${pr.registId}" >
				</c:if>
				<c:if test="${pr.state=='1'}">
					<input type="hidden" name="patientId" value="${pr.patientId}" >
				</c:if>
				<td><c:out value="${pr.patientId}"></c:out></td>
				<td><c:out value="${pr.registId}"></c:out></td>
				<td><c:out value="${pr.name}"></c:out></td>
				<td><c:out value="${pr.sex=='1'?'男':'女'}"></c:out></td>
				<td><c:out value="${fn:substring(pr.birthday,0,9)}"></c:out></td>
				<td class="td-status">
				<span class="layui-btn layui-btn-normal layui-btn-mini">
					<c:out value="${pr.relation=='1'?'本人':(pr.relation=='2'?'配偶':(pr.relation=='3'?'子女':(pr.relation=='4'?'父母':'其他'))) }"></c:out>
				</span>
				</td>
				<td>
					<c:out value="${pr.state=='0'?'已退号':(pr.state=='1'?'预约':(pr.state=='2'?'已就诊':(pr.state=='3'?'爽约':'等待'))) }"></c:out>
					<c:if test="${pr.state=='4'}">
						<button class="layui-btn" onclick="go_on(this,'${pr.registId}')" >
							<i class="layui-icon"></i>续诊
						</button>
						<button class="layui-btn" onclick="open_goOn(this,'${pr.patientId}','${pr.registId}')" >
							<i class="layui-icon"></i>开处方
						</button>
					</c:if>
				</td>
				<td><c:out value="${pr.isPast=='1'?'初诊':'复诊'}"></c:out>
					<c:if test="${pr.isPast=='2' && pr.state!='0'}">
						<button class="layui-btn" onclick="find_case(this,'${pr.patientId}')" >
							<i class="layui-icon"></i>查看病例
						</button>
					</c:if>
				</td>
				<td>
				<c:if test="${pr.state !='2' && pr.state !='3' && pr.state !='0'}">
					<button class="layui-btn" 
						onclick="xadmin.open('就诊卡','${pageContext.request.contextPath}/registration/selectCard?patientId=${pr.patientId}',400,400)">
						<i class="layui-icon"></i>查看
					</button>
				</c:if>
				</td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
			</div>
					<div class="layui-card-body ">
						<div class="page">
							<div>
								<jsp:include page="standard.jsp"></jsp:include>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});
		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});
	
	//点击下一位
	function next_bit(){
		var registId=$("input[name='registId']").val();
		var patientId=$("input[name='patientId']").val();
		if(registId == null || registId=='' || patientId == null || patientId=='' ){
			layer.msg('无患者!', {
				icon : 2,
				time : 2000
			}); 
		}else{
			window.location.href = "${pageContext.request.contextPath}/registration/updatePatientState?registId="+registId+"&state="+"2"+"&patientId="+patientId;
		}
	}
	//点击叫号
	function cry_mark(){
		var patientId=$("input[name='patientId']").val();
		if(patientId == null || patientId==''){
			layer.msg('无患者!', {
				icon : 2,
				time : 2000
			});
		}else{
		$.post("${pageContext.request.contextPath}/registration/cryMark",
			{
			patientId : patientId,
			},
			function(res){
				if(res == 4){
					layer.msg('叫号成功!', {
						icon : 1,
						time : 2000
					});
				}else if(res==0){
					layer.msg('没有就诊卡!', {
						icon : 2,
						time : 2000
					});
				}else if(res==2){
					layer.msg('余额不足!', {
						icon : 2,
						time : 2000
					});
				}else{
					layer.msg('网络异常!', {
						icon : 2,
						time : 2000
					});
				}
			});
	}
	}
	
	/* 1 已预约 2 已就诊  0 已退号 3 爽约  4 等待*/
	//点击等待
	function await(){
		var registId=$("input[name='registId']").val();
		if(registId == null || registId==''){
			layer.msg('无患者!', {
				icon : 2,
				time : 2000
			}); 
		}else{
		  window.location.href = "${pageContext.request.contextPath}/registration/updatePatientState?registId="+registId+"&state="+"4";
	 }
	}
	//点击续诊
	function go_on(obj,registId){
		window.location.href = "${pageContext.request.contextPath}/registration/updatePatientState?registId="+registId+"&state="+"2";
	}
	
	//点击续诊开处方
	function open_goOn(obj,patientId,registId){
		window.location.href = "${pageContext.request.contextPath}/registration/openMedicine?patientId="+patientId+"&registId="+registId;
	}
	
	//点击开处方
	function open_medicine(){
		var patientId=$("input[name='patientId']").val();
		var registId=$("input[name='registId']").val();
		if(patientId ==null || patientId=='' || registId==null || registId==''){
			layer.msg('无患者!', {
				icon : 2,
				time : 2000
			}); 
		}else{
		window.location.href = "${pageContext.request.contextPath}/registration/openMedicine?patientId="+patientId+"&registId="+registId;
	}
}
	//点击查看病例
	function find_case(obj,patientId){
		window.location.href = "${pageContext.request.contextPath}/registration/recordsPage?patientId="+patientId;
	}
	
	//点击开单据
	function open_receipts(){
		var patientId=$("input[name='patientId']").val();
		if(patientId ==null || patientId==''){
			layer.msg('无患者!', {
				icon : 2,
				time : 2000
			}); 
		}else{
			window.location.href = "${pageContext.request.contextPath}/registration/openReceipts?patientId="+patientId;
		}
	}
	//点击帮忙挂号
	function help(){
		$.post("${pageContext.request.contextPath}/registration/getDateNum",{},
			function(res){
				if(res <= 0){
					layer.msg('号已挂完!', {
						icon : 2,
						time : 2000
					});
				}else{
					window.location.href="${pageContext.request.contextPath}/registration/findAllDoctorPatient";
				}
		})
	}
	
	
	
	
</script>
</html>
