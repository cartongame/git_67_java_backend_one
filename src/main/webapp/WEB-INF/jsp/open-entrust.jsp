<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>开具医嘱</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
		<input type="hidden" name="patientId" id="patientId" value="${registration.patientId}" >
		<input type="hidden" name="registId" id="registId" value="${registration.registId}" >
				<div class="layui-form-item">
					<label for="context" class="layui-form-label"> <span
						class="x-red">*</span>医嘱
					</label>
					<div class="layui-input-inline">
						<textarea rows="2" cols="30" id="context"
							name="context" required="" maxlength="100" lay-verify="context"
							autocomplete="off">
						</textarea>
					</div>
				</div>
				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" onclick="open_entrust()">
						<i class="layui-icon"></i>确定
					</button>
				</div>
		</div>
	</div>
	<script>
		var flag = true;
		layui.use([ 'form', 'layer' ],function() {
		$ = layui.jquery;
		var form = layui.form, layer = layui.layer;

		//自定义验证规则
 		form.verify({
 			
			intro : function(value) {
				if (value.length < 5) {
					return '医嘱最少5个汉字';
				} 
				if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
               	 return '不能有特殊字符';
               	 }
              	  if(/(^\_)|(\__)|(\_+$)/.test(value)){
              	  	return '首尾不能出现下划线';
              	 	}
              	 		if(/^\d+\d+\d$/.test(value)){
              	 	return '不能全为数字';
              	 }
			}, 
		});
	});
		function open_entrust(){
			var context=$.trim($("#context").val()); //医嘱
			var patientId=$.trim($("#patientId").val()); //患者id
			var registId=$.trim($("#registId").val()); //挂号单id
			$.post("${pageContext.request.contextPath}/registration/updateContext",
					{
						patientId : patientId, advice : context , registId : registId
					},
					function(res) {
						if (res > 0) {
							layer.alert("已开具医嘱",
								{
									icon : 1
								},
								function() {
									// 获得frame索引
									var index = parent.layer.getFrameIndex(window.name);
									//关闭当前frame
									parent.layer.close(index);
									window.parent.location.reload();
								});
							}else{
							layer.alert("请先开处方",
								{
									icon : 2
								});
							}
				});
		}
		
		
		
		</script>

</body>

</html>