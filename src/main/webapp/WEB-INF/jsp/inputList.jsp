<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form method="post" action="${pageContext.request.contextPath}/input/inputs">
				<table border="1" cellspacing="0" width="400px" height="600px" align="center">
		
					<tr>
						<td>
							药品名称:
						</td>
						<td>
							<input type="text" id="productName" name="productName" >
						</td>
					</tr>
					<tr>
						<td>所属药房:</td>
						<td>
							<select id="storeName" name="storeName">
								<option value="西药房">西药房</option>
								<option value="中药房">中药房</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							药品类型:
						</td>
						<td>
							<select id="typeName" name="typeName">
								<option value="消炎类">消炎类</option>
								<option value="止疼类">止疼类</option>
								<option value="感冒类">感冒类</option>
								<option value="儿科用药类">儿科用药类</option>
								<option value="消化科用药类">消化科用药类</option>
								<option value="抗生素">抗生素</option>
								<option value="保健类">保健类</option>
								<option value="消化科用药">消化科用药</option>
								<option value="中药类">中药类</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>条形码/编号:</td>
						<td>
							<input type="text" id="productNum" name="productNum" >
						</td>
					</tr>
					<tr>
						<td>药品库存:</td>
						<td>
							<input type="text" id="productInvent" name="productInvent" >
						</td>
					</tr>
					<tr>
						<td>药品介绍:</td>
						<td>
							<input type="text" id="introduce" name="introduce" >
						</td>
					</tr>
					<tr>
						<td>药品进价:</td>
						<td>
							<input type="text" id="inPrice" name="inPrice">
						</td>
					</tr>
					<tr>
						<td>药品售价:</td>
						<td>
							<input type="text" id="salePrice" name="salePrice">
						</td>
					</tr>
					<tr>
						<td>生产日期:</td>
						<td>
							<input type="date" id="productDate" name="productDate" >
						</td>
					</tr>
					<tr>
						<td>到期日期:</td>
						<td>
							<input type="date" id="suppDate" name="suppDate">
						</td>
					</tr>
					<tr>
						<td colspan="2"  align="center">
							<input type="submit" value="提交"/>
						</td>		
					</tr>
				</table>
			</form>
		</div>
	</div>
	
</body>

</html>