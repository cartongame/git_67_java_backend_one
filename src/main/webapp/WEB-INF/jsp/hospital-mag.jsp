<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>科室列表页面</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
        <script src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
    </head>
    
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="#">首页</a>
                <a href="#">医院</a>
                <a>
                    <cite>医院信息</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            
                        </div>
                        <div class="layui-card-header">
                       <c:if test="${fn:indexOf(authCodes, 'group-addGroup')!=-1}">
                            <button class="layui-btn" onclick="xadmin.open('添加医院信息','${pageContext.request.contextPath}/hosp/addHospital',400,400)">
                                <i class="layui-icon"></i>添加</button>
                                </c:if>
                                </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                        
                                        <th>医院ID</th>
                                        <th>医院名称</th>
                                        <th>医院介绍</th>
                                       <th>成立日期</th>
                                       <th>地址</th>
                                       <th>等级</th>
                                         <th>网址</th>
                                       <th>咨询电话</th>
                                        <th>操作</th></tr>
                                </thead>
                               <tbody>
  							<c:forEach items="${page.resultList}" var="hospital" varStatus="s">
                                    <tr>
                                        
                                        <td style="width:20px;">${hospital.hospId}</td>
                                        
                                       <td><c:out value="${hospital.hospName}"></c:out> </td>
                                        <td><c:out value="${hospital.intro}"></c:out></td>
                                          <td><c:out value="${hospital.foundTime}"></c:out></td>
                                          <td><c:out value="${hospital.address}"></c:out></td>
                                          <td><c:out value="${hospital.level}"></c:out></td>
                                          <td><c:out value="${hospital.netWork}"></c:out></td>
                                          <td><c:out value="${hospital.phone}"></c:out></td>
                                     <td class="td-manage">
                                             
											
											
											 <%-- <c:if test="${fn:indexOf(authCodes, 'update_desk')!=-1}"> --%>
                                           <a title="修改医院信息" onclick="xadmin.open('修改医院信息','${pageContext.request.contextPath}/hosp/findHosp?hospId=${hospital.hospId}',400,400)" href="javascript:;">
                                                <i class="layui-icon">&#xe642;</i></a> 
                                           
                                                
                                        </td>
                                    </tr>
                                   </c:forEach> 
                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    <jsp:include page="standard.jsp"></jsp:include>
                                    
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });
        });
   
	
		//查询
		function sreachDesk(){
			var hospId=$("#hospId").val();
			var hospName=$("#hospName").val();
			var intro=$("#intro").val();
			var foundTime=$("#foundTime").val();
			var address=$("#address").val();
			var level=$("#level").val();
			var netWork=$("#netWork").val();
			var phone=$("#phone").val();
			
			window.location.href="${pageContext.request.contextPath}/desk/page?hospId="+hospId+"&hospName="+hospName+"&intro="+intro+"&foundTime="+foundTime+"&address="+address+"&level="+level+"&netWork="+netWork+"&phone="+phone;
		
		}
		

        
        
        </script>
</html>
</body>
</html>