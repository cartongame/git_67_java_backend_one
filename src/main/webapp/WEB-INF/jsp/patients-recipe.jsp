<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>患者处方页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
	</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th>医生姓名</th>
					<th>药品名称</th>
					<th>数量</th>
					<th>时间</th>
					<th>内容</th>
					<th>操作</th>
				</tr>
			</thead>
	<tbody >
			<c:forEach items="${recipe }" var="recipe" varStatus="r">
			<tr>
				<input type="hidden" id="patientId" name="patientId" value="${recipe.patientId}"/>
				<td><c:out value="${recipe.docName}"></c:out></td>
				<td><c:out value="${recipe.productName}"></c:out></td>
				<td><c:out value="${recipe.pNum}"></c:out></td>
				<td><c:out value="${recipe.createTime}"></c:out></td>
				<td><c:out value="${recipe.context}"></c:out></td>
				<td class="td-manage">
					<button class="layui-btn" 
						onclick="xadmin.open('费用清单','${pageContext.request.contextPath}/patient/getPatientMoney?patientId=${recipe.patientId}',500,400)">
						<i class="layui-icon"></i>费用清单
					</button>
			</td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
		</div>
		<div class="layui-card-body ">
		</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});
	

</script>
</html>
</body>
</html>
