<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>药房列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form" method="post" action="updateBuyList">
					<input type="hidden" id="productId" name="productId" value="${product.productId}">
					<input type="hidden" id="buyId" name="buyId" value="${buyList.buyId }">
				<div class="layui-form-item">
					<label for="productName" class="layui-form-label"> <span
						class="x-red">*</span>药品名称
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productName" name="productName" required=""
							maxlength="16" lay-verify="productName" autocomplete="off"
							class="layui-input" value="${product.productName}" disabled="disabled">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="factBuyNum" class="layui-form-label"> <span
						class="x-red">*</span>实际采购数量
					</label>
					<div class="layui-input-inline">
						<input type="text" id="factBuyNum" name="factBuyNum" required=""
							maxlength="16" lay-verify="factBuyNum" autocomplete="off"
							class="layui-input" value="${buyList.factBuyNum}" disabled="disabled">
					</div>
				</div>


				
				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
</body>
</html>

