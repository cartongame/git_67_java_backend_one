<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>调班</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
        <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
     </head>
    <body> 
        <div class="layui-fluid">
            <div class="layui-row">
            
                    <input type="hidden" name="ndutyId" id="ndutyId" value="${nurseDuty.ndutyId}">
                    <div class="layui-form-item">
						<label for="nurseName" class="layui-form-label"> <span
							class="x-red">*</span>姓名
						</label>
						<div class="layui-input-inline">
							<input type="text" id="nurseName" name="nurseName" required=""
								maxlength="20" lay-verify="nurseName" autocomplete="off"
								class="layui-input" value="${nurseDuty.nurseName}">
								
						</div>
					</div>
                    
                    
                     <div class="layui-form-item">
						<label for="ndutyDate" class="layui-form-label"> 
						<span class="x-red">*</span>日期
						</label>
						<div class="layui-input-inline">
							<input type="date" id="ndutyDate" name="ndutyDate" required=""
								maxlength="20" lay-verify="ndutyDate" autocomplete="off"
								class="layui-input" value="${param.ndutyDate}">
						</div>
					</div>
					
					 <div class="layui-form-item">
						<label for="nampm" class="layui-form-label"> 
						<span  class="x-red">*</span>时段
						</label>
						<div class="layui-input-inline">
								<select id="nampm" name="nampm">
		                            <option value="1">上午</option>
		                            <option value="2">下午</option>
		                            <option value="3">全天</option>
	                            </select>
						</div>
					</div>
                 
                    
                   <button class="layui-btn" onclick="saveNDutys()">
							<i class="layui-icon"></i>修改
						 </button>
                    
            </div>
        </div>
        <script>
        layui.use([ 'laydate', 'form' ], function() {
    		var laydate = layui.laydate;
    		layer = layui.layer;
    	});
        
        function saveNDutys(){
        	var ndutyId=$.trim($("#ndutyId").val());
        	var ampm=$("#ampm").val();
        	var ndutyDate=$.trim($("#ndutyDate").val());
        	if(ndutyDate == null || ndutyDate==''){
        		layer.msg("日期不能为空",
					{
						icon : 2,
						time : 1000
					});
        	}else{
        	$.post("${pageContext.request.contextPath}/nduty/updateNDutys",
        			{
        				ndutyId : ndutyId,ampm:ampm,ndutyDate : ndutyDate
        			},
        			function(res){
        				if (res > 0) {
							layer.msg("调班成功",
								{
									icon : 1,
									time : 1000
								},
							function() {
							// 获得frame索引
							var index = parent.layer.getFrameIndex(window.name);
							//关闭当前frame
							parent.layer.close(index);
							window.parent.location.reload();
						});
        			}else{
        				layer.msg("网络异常",
								{
									icon : 2,
									time : 1000
								});
        		}
        	});
       }
      }
   	
            </script>
    </body>

</html>