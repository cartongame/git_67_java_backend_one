<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>检验单据页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
	</div>
	<div class="layui-card-body ">
		<c:forEach items="${checkoutList }" var="checkoutList" varStatus="c">
			<ul>
				<li>附件：
		    		<img src="${checkoutList.files}"/>
				</li>
				<li>检验时间：
					<span>
						<c:out value="${checkoutList.createTime}"></c:out>
					</span>
				</li>
				<li>审核人：
					<span>
						<c:out value="${checkoutList.person}"></c:out>
					</span>
				</li>
				<li>备注：
					<span>
						<c:out value="${checkoutList.remark}"></c:out>
					</span>
				</li>
			</ul>
			<ul></ul>
		</c:forEach>
		</div>
		<div class="layui-card-body ">
		</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});

</script>
</html>
</body>
</html>
