<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>添加护士值班信息</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
        <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
     </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                    
                    <div class="layui-form-item">
                        <label for="nurseName" class="layui-form-label">
                            <span class="x-red">*</span>值班护士</label>
	                        <div class="layui-input-inline">
	                           <select id="nurseId" name="nurseId">
		                            <option value="">=请选择=</option>
		                            <c:forEach items="${nurseList}" var="n">
		                                <option value="${n.nurseId}">${n.nurseName}</option>
		                            </c:forEach>
	                            </select>
	                        </div>
                    </div>
                    
                    <div class="layui-form-item">
                        <label for="ndutyDate" class="layui-form-label">
                            <span class="x-red">*</span>值班日期</label>
                        <div class="layui-input-inline">
                            <input type="date" id="ndutyDate" name="ndutyDate"  class="layui-input"></div>
                    </div>
                    
                    <input type="hidden" id="dateFormat" name="dateFormat" value="<%=new SimpleDateFormat("yyyy-MM-dd").format(new Date())%>" />
                    
                    <div class="layui-form-item">
                        <label for="nampm" class="layui-form-label">
                            <span class="x-red">*</span>值班时间</label>
                        <div class="layui-input-inline">
                            <select id="nampm" name="nampm">
	                            <option value="1">上午</option>
	                            <option value="2">下午</option>
	                            <option value="3">全天</option>
                            </select>
                         </div>
                    </div>
                    
                   <button class="layui-btn" onclick="saveNDuty()">
					<i class="layui-icon"></i>完成
					</button>
                    
            </div>
	        </div>
	<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});
	 
	
	function saveNDuty(){
		var nurseId=$.trim($("#nurseId").val());
		var ndutyDate=$.trim($("#ndutyDate").val());
		var ampm=$.trim($("#ampm").val());
		var dateFormat=$.trim($("#dateFormat").val());
		if(nurseId == null || nurseId==''){
			layer.msg('请选择护士', {
				icon : 2,
				time : 1000
			});
			return;
		}
		if(ndutyDate < dateFormat){
      		layer.msg('不能选择过去的时间', {
					icon : 2,
					time : 1000
				});
      		}else{
				$.post( "${pageContext.request.contextPath}/nduty/saveNDuty",
	        		{
						nurseId : nurseId, ndutyDate : ndutyDate,ampm : ampm
	        		},
	        		function(res){
	        			if(res > 0 ){
	        				layer.msg('添加成功', {
	     						icon : 1,
	     						time : 1000
	     					},
	     					function() {
	    						// 获得frame索引
	    						var index = parent.layer.getFrameIndex(window.name);
	    						//关闭当前frame
	    						parent.layer.close(index);
	    						window.parent.location.reload();
	    					});
	        			}else{
	        				layer.msg('网络异常', {
	     						icon : 2,
	     						time : 1000
	     					});
	        			}
	       });
      	}
	}
	        
	
	
	</script>
</body>

</html>