<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>药品列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">药品管理</a>
			<a> <cite>药品盘点列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>

		
     <form class="layui-form" method="post" action="${pageContext.request.contextPath}/storeCheck/saveCheck">
     
     					<div class="layui-form-item">
	                            <c:forEach items="${storeCheckList}" var="sc">
	                               <%-- <td><c:out value="${store.storeId}"></c:out></td> --%>
	                               <input type="hidden" id="storeId" name="storeId" value="${store.storeId}"/>
          		         		 </c:forEach>
          		         	
                        </div>
                        <div class="layui-form-item">
	                            <c:forEach items="${storeCheckList}" var="sc">
	                               <%-- <td><c:out value="${store.storeId}"></c:out></td> --%>
	                               <input type="hidden" id="productId" name="productId" value="${product.productId}"/>
          		         		 </c:forEach>
          		         	
                        </div>
                        
                        <%-- <div class="layui-form-item">
	                            <c:forEach items="${storeCheckList}" var="sc">
	                               <td><c:out value="${store.storeId}"></c:out></td>
	                               <input type="text" id=batchId name="batchId" value="${checkBatch.batchId}"/>
          		         		 </c:forEach>
          		         	
                        </div>
                        
                        <div class="layui-form-item">
	                            <c:forEach items="${storeCheckList}" var="sc">
	                               <td><c:out value="${store.storeId}"></c:out></td>
	                               <input type="text" id=recordId name="recordId" value="${checkRecord.recordId}"/>
          		         		 </c:forEach>
          		         	
                        </div> --%>
   
                   		 <div class="layui-form-item">
                            <span class="x-red">*</span>药品名称: 
	                            <c:forEach items="${storeCheckList}" var="sc">
	                               <td><c:out value="${sc.productName}"/></td>
          		          </c:forEach>
                        </div>
               
                         <div class="layui-form-item">
                            <span class="x-red">*</span>库存:
	                            <c:forEach items="${storeCheckList}" var="sc">
	                               <td><c:out value="${sc.productInvent}"/></td>
	                            </c:forEach>
                        </div>
                        
                        <div class="layui-form-item">
                            <span class="x-red">*</span>盘点数量:
	                            <input type="text" id="checkNum" name="checkNum">
                        </div>
                        
                         <div class="layui-form-item">
                            <span class="x-red">*</span>批次号:
	                            <input type="daty" id="batchNum" name="batchNum">
                        </div>
                        
                         <div class="layui-form-item">
                           <span class="x-red">*</span>是否损耗:
								
								<input onClick="return diva()"  type="button" value="是"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input onClick="return divb()"  type="button" value="否"/>
								<div id="div1" style="display: none">
								<span class="x-red">*</span>损耗原因
								<input type="text" style="width:200px;height:40px;border:1px solid #000"  class="layui-form-item" name="reson" value=""><br>
								</div>
								
									<script type="text/javascript">
								function diva(){
									document.getElementById('div1').style.display="";
								};
								function divb(){
									document.getElementById('div1').style.display="none";
								};
								</script>
　　						 </div>
					
					 <div class="layui-form-item">
                            <span class="x-red">*</span>损耗数量:
	                            <input type="text" id="lossNum" name="lossNum">
                        </div>
	                     
	                   <button id="saveCheckBatch"  class="layui-btn" type="submit">
								<i class="layui-icon"></i>提交
							</button> 
	                        <%-- <button id="saveCheckBatch"  class="layui-btn" 
								onclick="xadmin.open('添加信息','${pageContext.request.contextPath}/storeCheck/saveCheck',400,400)">
								<i class="layui-icon"></i>提交
							</button> --%>
                </form>
	
		<div class="layui-card-body ">
	
	</div>
			
</body>

</html>

