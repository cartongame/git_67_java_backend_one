<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">
				<div class="layui-form-item">
					<label for="groupName" class="layui-form-label"> <span
						class="x-red">*</span>用户组名
					</label>
					<div class="layui-input-inline">
						<input type="text" id="groupName" name="groupName" required=""
							maxlength="25" lay-verify="groupName" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="groupCode" class="layui-form-label"> <span
						class="x-red">*</span>用户组code
					</label>
					<div class="layui-input-inline">
						<input type="text" id="groupCode" name="groupCode" required=""
							maxlength="16" lay-verify="groupCode" autocomplete="off"
							class="layui-input">
					</div>
				</div>







				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui
				.use(
						[ 'form', 'layer' ],
						function() {
							$ = layui.jquery;
							var form = layui.form, layer = layui.layer;

							//自定义验证规则
							form.verify({
								groupName : function(value) {
									if (value.length < 1) {
										return '用户组名至少得2个字符啊';
									}
								},
								groupCode : function(value) {
									if (value.length < 2) {
										return '用户组code至少3个字符啊';
									}
								}
							});

							//监听提交
							form
									.on(
											'submit(add)',
											function(data) {
												console.log(data);
												var groupName = $("#groupName")
														.val();
												
												var groupCode = $("#groupCode")
														.val();

												$
														.post(
																"${pageContext.request.contextPath}/group/saveGroupInfo",
																{
																	groupName : groupName,
																	
																	groupCode : groupCode
																},
																function(res) {

																	layer
																			.alert(
																					"增加成功",
																					{
																						icon : 6
																					},
																					function() {
																						// 获得frame索引
																						var index = parent.layer
																								.getFrameIndex(window.name);
																						//关闭当前frame
																						parent.layer
																								.close(index);
																						window.parent.location
																								.reload();
																					});

																})

												return false;

											});

						});

		//验证用户组名是否存在
		$("#groupName").blur(
				function() {
					var groupName = $.trim($("#groupName").val());

					if (!!groupName) {
						$.post(
								"${pageContext.request.contextPath}/group/getGroup?groupName="
										+ groupName, function(res) {

									if (res == "1") {
										flag = false;
										layer.msg('用户组已存在!', {
											icon : 5,
											time : 1000
										});
										$("#groupName").val("")
										return;
									}
								});

					}
				})
		//验证groupCode是否存在
		$("#groupCode").blur(
				function() {

					var groupCode = $.trim($("#groupCode").val());

					if (!!groupCode) {
						$.post(
								"${pageContext.request.contextPath}/group/getGroup?groupCode="
										+ groupCode, function(res) {

									if (res == "1") {
										flag = false;
										layer.msg('用户组code已存在!', {
											icon : 5,
											time : 1000
										});
										$("#groupCode").val("")
										return;
									}
								});

					}

				})
	</script>

</body>

</html>