<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>用户列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">用户管理</a>
			<a> <cite>用户列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
			<form class="layui-form layui-col-space5">
				<div class="layui-input-inline layui-show-xs-block">
					<input class="layui-input" placeholder="开始日" name="start" value="${param.start}"
						id="start">
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<input class="layui-input" placeholder="截止日" name="end" value="${param.end}"
						id="end">
				</div>

				<div class="layui-input-inline layui-show-xs-block">
					<select name="userState" id="userState" >
						<option value=""  >状态</option>
						<option value="1" ${param.userState=='0'?'selected="selected"':''}>离职</option>
						<option value="0" ${param.userState=='1'?'selected="selected"':''}>在职</option>
					</select>
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<input type="text" name="userName" id="userName"
						value="<c:out value="${param.userName}"></c:out>" placeholder="请输入用户名"
						autocomplete="off" class="layui-input">
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<button class="layui-btn" 
						onclick="sreachUser()">
						<i class="layui-icon">&#xe615;</i>
					</button>
				</div>
		</form>
	</div>
	<div class="layui-card-header">
	
		<c:if test="${fn:indexOf(authCodes, 'user-addUser')!=-1}">
			<button class="layui-btn"
				onclick="xadmin.open('添加医生账号','${pageContext.request.contextPath}/user/addUserInfo',400,400)">
				<i class="layui-icon"></i>添加医生账号
			</button>
		</c:if>
		<c:if test="${fn:indexOf(authCodes, 'user-addUser')!=-1}">
			<button class="layui-btn"
				onclick="xadmin.open('添加护士长账号','${pageContext.request.contextPath}/user/addSister',500,500)">
				<i class="layui-icon"></i>添加护士长账号
			</button>
		</c:if>

	</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<!-- <th><input type="checkbox" name="" lay-skin="primary">
					</th> -->
					<th>用户ID</th>
					<th>用户名称</th>
					<th>用户账号</th>
					<th>用户状态</th>
					<th>创建时间</th>
					<th>操作</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="user" varStatus="s">
			<tr>
				<%-- <td style="width: 20px;"><input type="checkbox"
					name="userId" value="${user.userId}" lay-skin="primary"></td> --%>
				<td><c:out value="${user.userId}"></c:out></td>
				<td><c:out value="${user.nickName}"></c:out></td>
				<td><c:out value="${user.userName}"></c:out></td>
				<td class="td-status"><span
					class="layui-btn layui-btn-normal layui-btn-mini">${user.userState=='1'?'在职':'离职'}</span>
				</td>
				<td>${fn:substring(user.createTime,0,19)}</td>
				<td class="td-manage">
				
					<c:if test="${fn:indexOf(authCodes, 'add_user_group')!=-1}">
						<a title="分配科室"
							onclick="xadmin.open('分配科室','${pageContext.request.contextPath}/user/getUserGroup?userId=${user.userId}&phone=${user.userName }',300,300)"
							href="javascript:;"> <i class="layui-icon">&#xe642;</i></a>
					</c:if>
					
					<c:if test="${fn:indexOf(authCodes, 'add_user_role')!=-1}">
						<a title="分配角色"
							onclick="xadmin.open('分配角色','${pageContext.request.contextPath}/role/getUserRole?userId=${user.userId}&phone=${user.userName }',300,300)"
							href="javascript:;"> <i class="layui-icon">&#xe642;</i></a>
					</c:if>
					<c:if test="${fn:indexOf(authCodes, 'doctor_addMessage')!=-1}">
					<a title="完善资料"
							onclick="xadmin.open('完善资料','${pageContext.request.contextPath}/doctor/addDoctorMessage?phone=${user.userName}',400,400)"
							href="javascript:;"> <i class="layui-icon">&#xe642;</i></a>
					</c:if>
					<c:if test="${fn:indexOf(authCodes, 'reset_user_pwd')!=-1}">
						<a title="密码重置"
							onclick="userpsw_reset(this,'${user.userId}')"
							href="javascript:;"> 
							<i class="layui-icon">&#xe669;</i>
						</a>
					</c:if> 
					
					</td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>

			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});

		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});

	/*用户-启用停用*/
	function member_stop(obj, userId,userState) {
		if ($(obj).parents("tr").find(".td-status").find('span').html() == '在职') {
		layer.confirm('确认要离职吗？',
			function(index) {
				$.post("${pageContext.request.contextPath}/user/updateUserState",
					{
						userId : userId,userState:userState
					},
					function(res) {
						if (res > 0) {
							layer.alert("离职成功",
								{
									icon : 1
								},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title','在职')
							
							$(obj).find('i').html('&#xe62f;');
							
					$(obj).parents("tr").find(".td-status").find('span').html('在职');

						} else {
							layer.msg('离职失败!', {
								icon : 2,
								time : 1000
							});
						}
					});

				});
			} else {
				layer.confirm('确认要在职吗？',
					function(index) {
						$.post("${pageContext.request.contextPath}/user/updateUserState",
							{
								userId : userId,userState:userState
							},
						function(res) {
							if (res > 0) {
								layer.alert("在职成功",
									{
										icon : 1
									},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title','离职')
							
							$(obj).find('i').html('&#xe601;');

							$(obj).parents("tr").find(".td-status").find('span').html('离职');
						
						} else {
							layer.msg('在职失败!', {
								icon : 2,
								time : 1000
							});
						}
					});
			});
		}

	}
	//用户查询
	function sreachUser() {
		var userName = $("#userName").val();
		var userState = $("#userState").val();
		var start = $("#start").val();
		var end = $("#end").val();
		
		window.location.href = "${pageContext.request.contextPath}/user/page?userName="
				+ userName
				+ "&userState="
				+ userState
				+ "&start="
				+ start
				+ "&end=" + end;

	}

	//密码重置
	function userpsw_reset(obj, userId) {
		layer.confirm('你确认要重置吗？', function(index) {
			//ajax
			$.post("${pageContext.request.contextPath}/user/resetPwd", {
				userId : userId
			}, function(res) {
				if (res > 0) {
					layer.msg('密码重置成功，重置为123456!', {
						icon : 1,
						time : 1000
					});
				} else {
					layer.msg('密码重置失败!', {
						icon : 1,
						time : 1000
					});
				}
			});
		});
	}
	/*用户-开除*/
	function member_del(obj, userId) {
		layer.confirm('确认要开除吗？', function(index) {
			//发异步开除数据
			$.post("${pageContext.request.contextPath}/user/deleteUser", {
				userId : userId
			}, function(res) {
				if (res > 0) {
					layer.msg('开除成功!', {
						icon : 1,
						time : 1000
					});
					$(obj).parents("tr").remove();
				} else {
					layer.msg('开除失败!', {
						icon : 1,
						time : 1000
					});
				}
			});

		});
	}
	//批量开除
	/* function delAll() {
		var user_id = [];
		$("input[name='userId']:checked").each(function() {
			user_id.push($(this).val());
		});
		if (user_id.length != 0) {

			userIds = user_id.join(",");
			layer.confirm('确认要开除吗？', function(index) {
				$.post("${pageContext.request.contextPath}/user/deleteUsers", {
					userIds : userIds
				}, function(res) {
					if (res > 0) {
						layer.msg('开除成功', {
							icon : 1
						});
						$(".layui-form-checked").not('.header').parents('tr')
								.remove();
					} else {
						layer.msg('开除失败', {
							icon : 1
						});
					}
				});

			});
		} else {
			layer.msg('请选择要开除的数据', {
				icon : 2
			});
		}
	} */
</script>
</html>
</body>
</html>
