<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>药房列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
		<div class="x-nav">
			<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">出库清单</a>
			</span> <a class="layui-btn layui-btn-small"
				style="line-height: 1.6em; margin-top: 3px; float: right"
				onclick="location.reload()" title="刷新"> <i
				class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
			</a>
		</div>
		<div class="layui-fluid">
			<div class="layui-row layui-col-space15">
				<div class="layui-col-md12">
					<div class="layui-card">
						<div class="layui-card-body ">
		</div>
			
		<div class="layui-card-body ">
			<table class="layui-table layui-form">
				<thead>
					<tr> 
						<th>药品名称</th> 
						<th>所属药房</th>
						<th>预计采购数量</th>
						<th>采购人</th>
						<th>采购电话</th>
						<th>操作</th>
					</tr>
				</thead>
		<tbody>
			<c:forEach items="${page.resultList}" var="buyListVo" varStatus="p">
				<tr>
					<td><c:out value="${buyListVo.productName}"></c:out></td>
					<td><c:out value="${buyListVo.storeName}"></c:out></td>
					<td><c:out value="${buyListVo.buyNum}"></c:out></td>
					<td><c:out value="${buyListVo.buyUser}"></c:out></td>
					<td><c:out value="${buyListVo.phone}"></c:out></td>
					<td>
						<button class="layui-btn" 
							onclick="xadmin.open('审核单','${pageContext.request.contextPath}/product/buyProducts?buyId=${buyListVo.buyId }&storeId=${buyListVo.storeId}&productId=${buyListVo.productId}&factBuyNum=${buyListVo.factBuyNum }&productName=${buyListVo.productName}',400,400)">
							<i class="layui-icon">	</i>审核
						</button>
					</td>
				</tr>
			</c:forEach> 
		</tbody>
			</table>
		</div>
		<div class="layui-card-body ">
			<div class="page">
				<jsp:include page="standard.jsp"></jsp:include>
			</div>
		</div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>

