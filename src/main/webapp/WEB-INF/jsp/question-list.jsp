<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>咨询问题列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">咨询问题管理</a>
			<a> <cite>咨询问题列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th>患者姓名</th>
					<th>问题</th>
					<th>回答</th>
				</tr>
			</thead>
	<tbody >
			<c:forEach items="${questions}" var="question" varStatus="q">
			<tr>
				<td><c:out value="${question.pname}"></c:out></td>
				<td><c:out value="${question.question}"></c:out></td>
				<td><c:out value="${question.answer}"></c:out></td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
			</div>
					<div class="layui-card-body ">
						<div class="page">
							<div>
								<jsp:include page="standard.jsp"></jsp:include>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});
	
	//模糊查询
	function sreachDoctor() {
		var doctorName = $.trim($("#doctorName").val());
		var state = $.trim($("#state").val());
		window.location.href = "${pageContext.request.contextPath}/doctor/page?doctorName="
				+ doctorName
				+ "&state=" + state;
		}
	
	
	//医生的在职离职
	function doctor_stop(obj, doctorPhone,state) {
		if ($(obj).parents("tr").find(".td-status").find('span').html() == '在职') {
		layer.confirm('确认要离职吗？',
			function(index) {
				$.post("${pageContext.request.contextPath}/doctor/updateDoctorState",
					{
					phone : doctorPhone , state:state
					},
					function(res) {
						if (res > 0) {
							layer.alert("离职成功",
								{
									icon : 1
								},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title','在职')
							$(obj).find('i').html('&#xe62f;');
							$(obj).parents("tr").find(".td-status").find('span').html('离职');
						} else {
							layer.msg('离职失败!', {
								icon : 2,
								time : 1000
							});
						}
					});
				});
			} else {
				layer.confirm('确认要在职吗？',
					function(index) {
						$.post("${pageContext.request.contextPath}/doctor/updateDoctorState",
							{
							phone : doctorPhone,state:state
							},
						function(res) {
							if (res > 0) {
								layer.alert("在职成功",
									{
										icon : 1
									},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title',' 离职')
							$(obj).find('i').html('&#xe601;');
							$(obj).parents("tr").find(".td-status").find('span').html('在职');
						
						} else {
							layer.msg('在职失败!', {
								icon : 2,
								time : 1000
							});
						}
					});
			});
		}

	}
	/*医生-开除*/
	function doctor_del(obj, doctorPhone) {
		layer.confirm('确认要开除吗？', function(index) {
			$.post("${pageContext.request.contextPath}/doctor/updateDoctorState", {
				phone : doctorPhone,state : "2"
			}, function(res) {
				if (res > 0) {
					layer.msg('开除成功!', {
						icon : 1,
						time : 1000
					},
					function() {
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);
						window.parent.location.reload();
					});
				} else {
					layer.msg('开除失败!', {
						icon : 1,
						time : 1000
					});
				}
			});

		});
	}
	/*医生-退休*/
	function doctor_update(obj, doctorPhone) {
		layer.confirm('确认要退休吗？', function(index) {
			$.post("${pageContext.request.contextPath}/doctor/updateDoctorState", {
				phone : doctorPhone,state : "3"
			}, function(res) {
				if (res > 0) {
					layer.msg('已退休!', {
						icon : 1,
						time : 1000
					},
					function() {
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);
						window.parent.location.reload();
					});
				} else {
					layer.msg('退休失败!', {
						icon : 2,
						time : 1000
					});
				}
			});

		});
	}
	
	
	
	
	

</script>
</html>
</body>
</html>
