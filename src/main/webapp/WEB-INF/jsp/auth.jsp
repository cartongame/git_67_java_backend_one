<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<HTML>
<HEAD>
<TITLE>ZTREE DEMO - radio</TITLE>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/zTree_v3/css/demo.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/zTree_v3/css/zTreeStyle/zTreeStyle.css"
	type="text/css">
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery.ztree.excheck.js"></script>
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>

<!--
	<script type="text/javascript" src="../../../js/jquery.ztree.exedit.js"></script>
	-->
<SCRIPT type="text/javascript">
<!--
	var setting = {
		check : {
			enable : true,
			chkStyle : "radio",
			radioType : "all"
		},
		callback : {
			onCheck : zTreeOnCheck
		},

		data : {
			simpleData : {
				enable : true
			}
		}
	};

	var zNodes = ${authjsonArray};
	$(document).ready(function(){  
		   $.fn.zTree.init($("#treeDemo"), setting, zNodes);
		   zTreeOnCheck();//加载页面触发默认显示已选择的权限 
		  });
	function zTreeOnCheck(event, treeId, treeNode) {
		var treeObj = $.fn.zTree.getZTreeObj("treeDemo"), nodes = treeObj
				.getCheckedNodes(true), v = "";

		//获取选中的复选框的值
		for (var i = 0; i < nodes.length; i++) {
			v = nodes[i].id + "_" + nodes[i].authType;
			
			$("#authIdInput").val(v);
			
		}

	};

	$(document).ready(function() {
		$.fn.zTree.init($("#treeDemo"), setting, zNodes);
	});
	//-->
	layui.use([ 'form', 'layer' ], function() {

		layer = layui.layer;

	});
	//添加权限
	function addAuth() {
		var authIds = $("#authIdInput").val();//3_3
		var arr = authIds.split("_");
		if (arr.length > 1 && arr[1] == "3") {//按钮
			layer.msg('对不起，按钮权限下无法添加子权限!', {
				icon : 2,
				time : 1000
			});

			return;
		} else {
			window.location.href = "${pageContext.request.contextPath}/auth/addAuthInfo?params="
					+ authIds;
		}
	}
	
</SCRIPT>


</HEAD>

<BODY>

	<center>
		<div class="content_wrap">
			<div class="zTreeDemoBackground left">
				<ul id="treeDemo" class="ztree"></ul>
				<input type="hidden" id="authIdInput" /> 
					
			</div>

		</div>
		<div class="layui-card-header">
			<c:if test="${fn:indexOf(authCodes, 'auth-addAuth')!=-1}">
				<button class="layui-btn" onclick="addAuth()">
					<i class="layui-icon"></i>添加
				</button>
				</c:if>
		</div>
		

	</center>
</BODY>
</HTML>

