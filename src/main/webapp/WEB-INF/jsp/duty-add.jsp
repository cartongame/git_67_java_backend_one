<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
  <head>
      <meta charset="UTF-8">
      <title>添加医生值班信息</title>
      <meta name="renderer" content="webkit">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
      <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
      <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
   </head>
  <body>
      <div class="layui-fluid">
          <div class="layui-row">
                  <div class="layui-form-item">
                      <label for="docName" class="layui-form-label">
                          <span class="x-red">*</span>值班医生</label>
                       <div class="layui-input-inline">
                          <select id="docId" name="docId">
                            <option value="">=请选择=</option>
                            <c:forEach items="${docList}" var="docList" varStatus="d">
                                <option value="${docList.docId}">${docList.name}</option>
                            </c:forEach>
                           </select>
                       </div>
                  </div>
                  
                  <div class="layui-form-item">
                      <label for="dutyDate" class="layui-form-label">
                          <span class="x-red">*</span>值班日期</label>
                      <div class="layui-input-inline">
                          <input type="date" id="dutyDate" name="dutyDate"  class="layui-input"></div>
                  </div>
                  
              	<input type="hidden" id="dateFormat" name="dateFormat" value="<%=new SimpleDateFormat("yyyy-MM-dd").format(new Date())%>" />
              		
                  <div class="layui-form-item">
                      <label for="ampm" class="layui-form-label">
                          <span class="x-red">*</span>值班时间</label>
                      <div class="layui-input-inline">
                          <select id="ampm" name="ampm">
                           <option value="1">上午</option>
                           <option value="2">下午</option>
                           <option value="3">全天</option>
                          </select>
                       </div>
                  </div>
                  
                 <div class="layui-form-item">
				<label for="regNum" class="layui-form-label"> <span
					class="x-red">*</span>挂号人数
				</label>
				<div class="layui-input-inline">
					<input type="text" required="" id="regNum" name="regNum"
						lay-verify="regNum" autocomplete="off" maxlength="2"
						class="layui-input" placeholder="默认50人">
				</div>
			</div>
                  
             <button class="layui-btn" onclick="saveDuty()">
					<i class="layui-icon"></i>完成
			 </button>
                  
          </div>
      </div>
      <script>
      layui.use([ 'laydate', 'form' ], function() {
  		var laydate = layui.laydate;
  		layer = layui.layer;
  	});
       
      
      
      function saveDuty(){
      	var docId=$.trim($("#docId").val());
      	var dutyDate=$.trim($("#dutyDate").val());
      	var ampm=$.trim($("#ampm").val());
      	var regNum=$.trim($("#regNum").val());
      	var dateFormat=$.trim($("#dateFormat").val());
      	if(docId == null || docId==''){
      		layer.msg('请选择医生', {
				icon : 2,
				time : 1000
			});
      		return;
      	}
      	if(dutyDate < dateFormat){
      		layer.msg('不能选择过去的时间', {
					icon : 2,
					time : 1000
				});
      	}else{
      	var num=50;
      	if(regNum == null || regNum==''){
      		$.post( "${pageContext.request.contextPath}/duty/saveDuty",
              		{
      				docId : docId , dutyDate : dutyDate , ampm : ampm, regNum : num
              		},
              		function(res){
              			if(res > 0 ){
              				layer.msg('添加成功', {
           						icon : 1,
           						time : 1000
           					},
           					function() {
          						// 获得frame索引
          						var index = parent.layer.getFrameIndex(window.name);
          						//关闭当前frame
          						parent.layer.close(index);
          						window.parent.location.reload();
          					});
              			}
              		});
      		}else{
      		$.post( "${pageContext.request.contextPath}/duty/saveDuty",
              		{
      				docId : docId , dutyDate : dutyDate , ampm : ampm, regNum : regNum
              		},
              		function(res){
              			if(res > 0){
              				layer.msg('添加成功', {
           						icon : 1,
           						time : 1000
           					},
           					function() {
          						// 获得frame索引
          						var index = parent.layer.getFrameIndex(window.name);
          						//关闭当前frame
          						parent.layer.close(index);
          						window.parent.location.reload();
          					});
              			}
              		});
      		
      		
      	}
      }
      	
      }
                
                
                
</script>
</body>

</html>