<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
				<table border="1" cellspacing="0" width="400px" height="100px" align="center">
					<form  method="post" action="addProduct">
						<input type="hidden" id="insId" name="insId" value="${inStore.insId }">
						<input type="hidden" id="productId" name="productId" value="${product.productId}"/>
		
					<tr>
						<td>药品名称</td>
						<td>
							<input type="text" id="productName" name="productName" value="${product.productName}" disabled="disabled"/>
						</td>
					</tr>
					<tr>
						<td>药品入库数量</td>
						<td>
							<input type="text" id="inNum" name="inNum" value="${inStore.inNum}" disabled="disabled"/>
						</td>
					</tr>
					
					<tr>
						<td colspan="2"  align="center">
							<input type="submit" value="提交入库"/>
						</td>
					</tr>
				
				</table>

				
			</form>

	</div>
</div>
</body>

</html>