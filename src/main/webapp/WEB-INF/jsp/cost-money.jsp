<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>费用清单页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
	</div>
	<div class="layui-card-body ">
		<c:forEach items="${costList }" var="costList" varStatus="c">
			<ul>
				<li>医生姓名：
					<span>
						<c:out value="${costList.docName}"></c:out>
					</span>
				</li>
				
				<li>项目名称：
					<span>
						<c:out value="${costList.itemName}"></c:out>
					</span>
				</li>
				<li>项目规格：
					<span>
						<c:out value="${costList.itemRule}"></c:out>
					</span>
				</li>
				<li>单位：
					<span>
						<c:out value="${costList.unit}"></c:out>
					</span>
				</li>
				<li>药品名：
					<span>
						<c:out value="${costList.productName}"></c:out>
					</span>
				</li>
				<li>数量：
					<span>
						<c:out value="${costList.num}"></c:out>
					</span>
				</li>
				<li>单价：
					<span>
						<c:out value="${costList.price}"></c:out>
					</span>
				</li>
				<li>小计 ：
					<span>
						<c:out value="${costList.total}"></c:out>
					</span>
				</li>
				<li>时间：
					<span>
						<c:out value="${costList.createTime}"></c:out>
					</span>
				</li>
			</ul>
		</c:forEach>
		</div>
		<div class="layui-card-body ">
		</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
</body>
</html>
