<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>护士详细资料页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
	</div>
	<div class="layui-card-body ">
			<ul>
				<li>姓名：
					<span>
						<c:out value="${nurses.nurseName}"></c:out>
					</span>
				</li>
				<li>性别：
					<span>
						<c:out value="${nurses.sex=='1'?'男':'女'}"></c:out>
					</span>
				</li>
				<li>科室：
					<span>
						<c:out value="${nurses.deskName}"></c:out>
					</span>
				</li>
				<li>手机号：
					<span>
						<c:out value="${nurses.phone}"></c:out>
					</span>
				</li>
				<li>邮箱：
					<span>
						<c:out value="${nurses.email}"></c:out>
					</span>
				</li>
				<li>出生日期：
					<span>
						<c:out value="${fn:substring(nurses.birthday,0,9)}"></c:out>
					</span>
				</li>
				<li>紧急联系人：
					<span>
						<c:out value="${nurses.nurseConcat}"></c:out>
					</span>
				</li>
				<li>联系人电话：
					<span>
						<c:out value="${nurses.tel}"></c:out>
					</span>
				</li>
				<li>介绍：
					<span>
						<c:out value="${nurses.intro}"></c:out>
					</span>
				</li>
			</ul>
		</div>
		<div class="layui-card-body ">
		</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});
		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});
	

</script>
</html>
</body>
</html>
