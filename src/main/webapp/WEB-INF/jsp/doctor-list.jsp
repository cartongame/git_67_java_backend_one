<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>医生列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">医生管理</a>
			<a> <cite>医生列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
			<form class="layui-form layui-col-space5">
				<div class="layui-input-inline layui-show-xs-block">
					<select name="state" id="state" >
						<option value=""  >状态</option><!-- 1在职 0离职  2开除 3退休 -->
						<option value="2" ${param.state=='2'?'selected="selected"':''}>开除</option>
						<option value="0"${param.state=='0'?'selected="selected"':''}>离职</option>
						<option value="1" ${param.state=='1'?'selected="selected"':''}>在职</option>
						<option value="3" ${param.state=='3'?'selected="selected"':''}>退休</option>
					</select>
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<input type="text" name="doctorName" id="doctorName"
						value="<c:out value="${param.doctorName}"></c:out>" placeholder="请输入医生名"
						autocomplete="off" class="layui-input">
				</div>
				
				<div class="layui-input-inline layui-show-xs-block">
					<button class="layui-btn" 
						onclick="sreachDoctor()">
						<i class="layui-icon">&#xe615;</i>
					</button>
				</div>
				<c:if test="${fn:indexOf(roleCode, 'director')!=-1}">
					<a class="layui-btn" href="${pageContext.request.contextPath}/doctor/findAllDoctorExp">
					<i class="layui-icon"></i>实习医生审核
				</c:if>
			</a>
		</form>
	</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th>ID</th>
					<th>姓名</th>
					<th>性别</th>
					<th>状态</th>
					<th>所在科室</th>
					<th>职位</th>
					<th>操作</th>
				</tr>
			</thead>
	<tbody >
			<c:forEach items="${page.resultList}" var="doctor" varStatus="v">
			<tr>
				<input type="hidden" name="doctorPhone" id="doctorPhone" 
					value="${doctor.doctorPhone}">
				<td><c:out value="${doctor.docId}"></c:out></td>
				<td><c:out value="${doctor.doctorName}"></c:out></td>
				<td><c:out value="${doctor.sex=='1'?'男':(doctor.sex=='0'?'女':'待补充')}"></c:out></td>
				<td class="td-status">
				<span class="layui-btn layui-btn-normal layui-btn-mini">
					<c:out value="${doctor.state=='0'?'离职':(doctor.state=='1'?'在职':(doctor.state=='2'?'开除':'退休')) }"></c:out>
				</span>
				<!-- 1在职 0离职  2开除 3退休 -->
				</td>
				<td><c:out value="${doctor.deskName}"></c:out></td>
				<!-- 职位  1 主任医师  2 副主任医师  3 主治医生 4 实习医生 -->
				<td>
					<span class="layui-btn layui-btn-normal layui-btn-mini">
					<c:out value="${doctor.job=='1'?'主任医师':(doctor.job=='2'?'副主任医师':(doctor.job=='3'?'主治医生':'实习医生')) }"></c:out>
				</span>
				</td>
				<td class="td-manage">
				<c:if test="${fn:indexOf(roleCode, 'director')!=-1}">
					<c:if test="${doctor.state=='1' || doctor.state=='0'}">
						<a onclick="doctor_stop(this,'${doctor.doctorPhone}','${doctor.state}')" 
							title="${doctor.state=='1'?'离职':'在职'}"
							href="javascript:;">
							<i class="layui-icon">${doctor.state=='1'?'&#xe601;':'&#xe62f;'}</i>
						</a>
						<a title="开除" onclick="doctor_del(this,'${doctor.doctorPhone}')"
							href="javascript:;"> <i class="layui-icon">&#xe640;</i>
						</a>
						<a title="退休" onclick="doctor_update(this,'${doctor.doctorPhone}')"
							href="javascript:;"> <i class="layui-icon">&#xe641;</i>
						</a>
					</c:if>
				 </c:if> 
					<a title="修改资料"
					onclick="xadmin.open('修改资料','${pageContext.request.contextPath}/doctor/message?phone=${doctor.doctorPhone}&docId=${doctor.docId}',700,500)">
					<i class="layui-icon">&#xe642;</i>
					</a>
					<a title="评价"
						onclick="xadmin.open('评价','${pageContext.request.contextPath}/doctor/appraise?docId=${doctor.docId}',700,500)">
						<i class="layui-icon">&#xe642;</i>
					</a>
			</td>
			</tr>
			
		</c:forEach>
	</tbody>
		</table>
			</div>
					<div class="layui-card-body ">
						<div class="page">
							<div>
								<jsp:include page="standard.jsp"></jsp:include>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});
	
	//模糊查询
	function sreachDoctor() {
		var doctorName = $.trim($("#doctorName").val());
		var state = $.trim($("#state").val());
		window.location.href = "${pageContext.request.contextPath}/doctor/page?doctorName="
				+ doctorName
				+ "&state=" + state + "&deskId=" + deskId + "&phone=";
		}
	
	
	//医生的在职离职
	function doctor_stop(obj, doctorPhone,state) {
		if ($(obj).parents("tr").find(".td-status").find('span').html() == '在职') {
		layer.confirm('确认要离职吗？',
			function(index) {
				$.post("${pageContext.request.contextPath}/doctor/updateDoctorState",
					{
					phone : doctorPhone , state:state
					},
					function(res) {
						if (res > 0) {
							layer.alert("离职成功",
								{
									icon : 1
								},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title','在职')
							$(obj).find('i').html('&#xe62f;');
							$(obj).parents("tr").find(".td-status").find('span').html('离职');
						} else {
							layer.msg('离职失败!', {
								icon : 2,
								time : 1000
							});
						}
					});
				});
			} else {
				layer.confirm('确认要在职吗？',
					function(index) {
						$.post("${pageContext.request.contextPath}/doctor/updateDoctorState",
							{
							phone : doctorPhone,state:state
							},
						function(res) {
							if (res > 0) {
								layer.alert("在职成功",
									{
										icon : 1
									},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title',' 离职')
							$(obj).find('i').html('&#xe601;');
							$(obj).parents("tr").find(".td-status").find('span').html('在职');
						
						} else {
							layer.msg('在职失败!', {
								icon : 2,
								time : 1000
							});
						}
					});
			});
		}

	}
	/*医生-开除*/
	function doctor_del(obj, doctorPhone) {
		layer.confirm('确认要开除吗？', function(index) {
			$.post("${pageContext.request.contextPath}/doctor/updateDoctorState", {
				phone : doctorPhone,state : "2"
			}, function(res) {
				if (res > 0) {
					layer.msg('开除成功!', {
						icon : 1,
						time : 1000
					},
					function() {
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);
						window.parent.location.reload();
					});
				} else {
					layer.msg('开除失败!', {
						icon : 1,
						time : 1000
					});
				}
			});

		});
	}
	/*医生-退休*/
	function doctor_update(obj, doctorPhone) {
		layer.confirm('确认要退休吗？', function(index) {
			$.post("${pageContext.request.contextPath}/doctor/updateDoctorState", {
				phone : doctorPhone,state : "3"
			}, function(res) {
				if (res > 0) {
					layer.msg('已退休!', {
						icon : 1,
						time : 1000
					},
					function() {
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);
						window.parent.location.reload();
					});
				} else {
					layer.msg('退休失败!', {
						icon : 2,
						time : 1000
					});
				}
			});

		});
	}
	
	
	
	
	

</script>
</html>
</body>
</html>
