<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>欢迎页面-权限管理系统</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<c:forEach items="${roleInfoList}" var="r" varStatus="s">
				<div class="layui-input-inline">
					<input type="checkbox" ${r.ch=='1'?"checked='checked'":""}
						name="roleId" value="${r.roleId}" />${r.roleName}&nbsp;&nbsp;&nbsp;</div>
			</c:forEach>
			</br> </br> </br> </br> </br> </br> </br>
			<div class="layui-form-item">

				<label for="L_repass" class="layui-form-label"></label>
				<button class="layui-btn" lay-filter="add" lay-submit=""
					onclick="setRole()">提交</button>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		layui.use([ 'form', 'layer' ], function() {

			layer = layui.layer;

		});
		//分配角色
		function setRole() {
			var role_id = [];
			$("input[name='roleId']:checked").each(function() {
				role_id.push($(this).val());
			});

			roleIds = role_id.join(",");
			userId = "${param.userId}";

			$.ajax({
				url : "../role/setUserInfoRoleInfo",
				data : {
					userId : userId,
					roleIds : roleIds
				},
				type : "post",
				dataType : "json",
				success : function(result) {
					if (result.msg) {

						layer.alert("分配成功", {
							icon : 6
						},
								function() {
									// 获得frame索引
									var index = parent.layer
											.getFrameIndex(window.name);
									//关闭当前frame
									parent.layer.close(index);
								});

					}

					else {

						layer.alert("分配失败", {
							icon : 6
						},
								function() {
									// 获得frame索引
									var index = parent.layer
											.getFrameIndex(window.name);
									//关闭当前frame
									parent.layer.close(index);
								});

					}
				},
				error : function() {
					layer.alert("网络繁忙，请稍后操作", {
						icon : 6
					}, function() {
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);
					});
				}
			});

		}
	</script>
	<!--         <script>
            layui.use(['form', 'layer'],
                    function() {
                        $ = layui.jquery;
                        var form = layui.form,
                        layer = layui.layer;

                //自定义验证规则
            /*     form.verify({
                    nikename: function(value) {
                        if (value.length < 5) {
                            return '昵称至少得5个字符啊';
                        }
                    },
                    pass: [/(.+){6,12}$/, '密码必须6到12位'],
                    repass: function(value) {
                        if ($('#L_pass').val() != $('#L_repass').val()) {
                            return '两次密码不一致';
                        }
                    }
                }); */

                //监听提交
                form.on('submit(add)',
                function(data) {
                    console.log(data);
                    //发异步，把数据提交给php
                    layer.alert("分配成功", {
                        icon: 6
                    },
                    function() {
                        // 获得frame索引
                        var index = parent.layer.getFrameIndex(window.name);
                        //关闭当前frame
                        parent.layer.close(index);
                    });
                    return false;
                });

            });</script>  -->
	<!-- <script>var _hmt = _hmt || []; (function() {
                var hm = document.createElement("script");
                hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(hm, s);
            })();</script>  -->
</body>

</html>