<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>医生资料页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
	</div>
	<div class="layui-card-body ">
	
			<ul>
				<li>姓名：
					<span>
						<c:out value="${doctorExpVo.name}"></c:out>
					</span>
				</li>
				
				<li>头像：
					<form id="uploadImg" method="post" action="/doctor/uploadImg" enctype="multipart/form-data">
	    				<img src="${doctorExpVo.docImg}" width="200" height="200"/>
	    				<input type="hidden" id="docId" name="docId" value="${docId }"/>
	    				<input onchange="sub()" type="file" id="file" name="file"></input>
	    			</form>
				</li>
				
				<li>性别：
					<span>
						<c:out value="${doctorExpVo.sex=='1'?'男':(doctorExpVo.sex=='0'?'女':'待补充')}"></c:out>
					</span>
				</li>
				
				<li>身份证号：
					<span>
						<c:out value="${doctorExpVo.idCard}"></c:out>
					</span>
				</li>
				<li>职位：
					<span>
						<c:out value="${doctorExpVo.job=='1'?'主任医师':(doctorExpVo.job=='2'?'副主任医师':(doctorExpVo.job=='3'?'主治医生':'实习医生')) }"></c:out>
					</span>
				</li>
				<li>手机号：
					<span>
						<c:out value="${doctorExpVo.phone}"></c:out>
					</span>
				</li>
				<li>邮箱：
					<span>
						<c:out value="${doctorExpVo.email}"></c:out>
					</span>
				</li>
				<li>证件：
		    			<form id="uploadUrls" method="post" action="/doctor/uploadUrls" enctype="multipart/form-data">
							<input id="file" type="file" name="file"/>
							<input id="file" type="file" name="file"/>
							<input id="file" type="file" name="file"/>
							<input type="hidden" id="docId" name="docId" value="${docId }"/>
							<input class="layui-btn" type="submit" value="上传"/>
						</form>
	    				<c:forEach items="${doctorExp }" var="doctorExp" varStatus="d">
	    					<c:out value="${doctorExp.urls}"></c:out>
	    				</c:forEach>
				</li>
				<input type="hidden" id="docId" name="docId" value="${docId }"/>
				<li>论文：
						<input type="text" id="nets" name="nets" width="20" height="20" placeholder="请输入论文网址"/>
						<button class="layui-btn" onclick="setNets()">
							<i class="layui-icon"></i>上传
						</button>
					 <c:forEach items="${doctorExp }" var="doctorExp" varStatus="d">
						<span>
							<c:out value="${doctorExp.nets}"></c:out>
						</span>
					</c:forEach>
				</li>
				<li>出生日期：
					<span>
						<c:if test="${doctorExpVo.birthday == null}">待补充</c:if>
						<c:if test="${doctorExpVo.birthday != null}">
						${fn:substring(doctorExpVo.birthday,0,9)}
						</c:if>
					</span>
				</li>
				<li>紧急联系人：
					<span>
						<c:out value="${doctorExpVo.concat}"></c:out>
					</span>
				</li>
				<li>联系人电话：
					<span>
						<c:out value="${doctorExpVo.tel}"></c:out>
					</span>
				</li>
				<li>介绍：
					<span>
						<c:out value="${doctorExpVo.intro}"></c:out>
					</span>
				</li>
			</ul>
		</div>
		<div class="layui-card-body ">
		</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});
	
	//上传图片 docId
	function sub(){
		$("#uploadImg").submit();
	}
	
	
	
	
	//上传论文地址
	function setNets(){
		var nets=$.trim($("#nets").val());
		var docId=$.trim($("#docId").val());
		if(nets ==null || nets==''){
			layer.msg('请输入网址!', {
				icon : 2,
				time : 1000
			});
		}else{
			$.post("${pageContext.request.contextPath}/doctor/updateNets",{
				nets : nets,
				docId : docId
				
			},function(res){
				if(res > 0){
				layer.msg('上传成功', {
					icon : 1,
					time : 1000
				},function() {
					// 获得frame索引
					var index = parent.layer.getFrameIndex(window.name);
					//关闭当前frame
					parent.layer.close(index);
					window.parent.location.reload();
				});
				}
			})
		}
	}
	
	/* var reg=/(^#)|(^http(s*):\/\/[^\s]+\.[^\s]+)/;
	if(!reg.test(data.field.link)){
	     layer.msg('外链格式错误，请输入以http://或https://开头的完整url！',{icon: 5});
	     return false;
	}*/
	

</script>
</html>
</body>
</html>
