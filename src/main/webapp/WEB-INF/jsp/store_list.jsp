<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>盘点列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>

			
			<div class="layui-input-inline layui-show-xs-block">
					<input type="date" placeholder="盘点时间" name="checkTime"
						id="checkTime">
				</div>
				
				
	<div class="layui-input-inline layui-show-xs-block">
				<button class="layui-btn" 
					onclick="serchStoreCheck()">
					<i class="layui-icon">&#xe615;</i>
				</button>
			</div>
		

           
           <div class="layui-card-body ">
           <table class="layui-table layui-form">
			<thead>
				<tr>
					<th>盘点ID</th>
					<th>药房名称</th>
					<th>药品名称</th>
					<th>批次号</th>
					<th>库存</th>
					<th>盘点数量</th>
					<th>盘点时间</th>
					<th>损耗数量</th>
					<th>损耗原因</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="sc" varStatus="p">
					<tr>
						<td><c:out value="${sc.checkId}"></c:out></td>
						<td><c:out value="${sc.storeName}"></c:out></td>
						<td><c:out value="${sc.productName}"></c:out></td>
						<td><c:out value="${sc.batchNum}"></c:out></td>
						<td><c:out value="${sc.productNum}"></c:out></td>
						<td><c:out value="${sc.checkNum}"></c:out></td>
						<td><c:out value="${sc.checkTime}"></c:out></td>
						<td><c:out value="${sc.lossNum}"></c:out></td>
						<td><c:out value="${sc.reson}"></c:out></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
          
	</div>
	
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>

			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});

		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});

	
	//根据时间查询盘点信息
	 function serchStoreCheck() {
		
		var checkTime = $("#checkTime").val();
		
		
		window.location.href = "${pageContext.request.contextPath}/storeCheck/page?checkTime="
				+ checkTime
				+ "&storeId="
				+ storeId
				+ "&recordId="
				+ recordId
				+ "&productId="
				+ productId
				+ "&batchId="
				+ batchId
				+ "&checkNum="
				+ checkNum
				+ "&checkTime="
				+ checkTime
				+ "&checkUser="
				+ checkUser
				+ "&lossNum="
				+ lossNum
				+ "&batchNum="
				+ batchNum
				+ "&reson=" + reson;

	} 
</script>
</html>
