<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>开单据</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
		<input type="hidden" name="patientId" id="patientId" value="${checkoutList.patientId}"/>
		
			<form id="uploadfile" method="post" action="/registration/uploadFile" enctype="multipart/form-data">
   				<img src="${checkoutList.files}" width="100" height="100"/>
   				<input type="hidden" name="patientId" id="patientId" value="${checkoutList.patientId}"/>
   				<input onchange="sub()" type="file" id="files" name="files"></input>
   			</form>
   			
			<form class="layui-form">		
				<div class="layui-form-item">
					<label for="itemName" class="layui-form-label"> <span
						class="x-red">*</span>项目名称
					</label>
					<div class="layui-input-inline">
						<input type="text" id="itemName"
							name="itemName" required="" maxlength="10" lay-verify="itemName"
							autocomplete="off" class="layui-input">
					</div>
				</div>
			
				<div class="layui-form-item">
					<label for="price" class="layui-form-label"> <span
						class="x-red">*</span>价格
					</label>
					<div class="layui-input-inline">
						<input type="text" id="price"
							name="price" required="" maxlength="5" lay-verify="price"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="person" class="layui-form-label"> <span
						class="x-red">*</span>审核人
					</label>
					<div class="layui-input-inline">
						<input type="text" id="person"
							name="person" required="" maxlength="5" lay-verify="person"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="remark" class="layui-form-label"> <span
						class="x-red">*</span>备注
					</label>
					<div class="layui-input-inline">
						<textarea rows="2" cols="30" id="remark"
							name="remark" required="" maxlength="100" lay-verify="remark"
							autocomplete="off">
						</textarea>
					</div>
				</div>

				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">完成</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui.use([ 'form', 'layer' ],function() {
		$ = layui.jquery;
		var form = layui.form, layer = layui.layer;

		
		form.verify({
			itemName : function(value) {
				if (value.length < 1) {
					return '不能为空';
				}
				if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                	 return '不能有特殊字符';
                	 }
               	  if(/(^\_)|(\__)|(\_+$)/.test(value)){
               	  	return '首尾不能出现下划线';
               	 	}
               	 		if(/^\d+\d+\d$/.test(value)){
               	 	return '不能全为数字';
               	 }
			},
			price : function(value) {
				if (value.length < 1) {
					return '不能为空';
				}
			},
			
			person : function(value) {
				if (value.length < 1) {
					return '不能为空';
				}
				if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                	 return '不能有特殊字符';
                	 }
               	  if(/(^\_)|(\__)|(\_+$)/.test(value)){
               	  	return '首尾不能出现下划线';
               	 	}
               	 		if(/^\d+\d+\d$/.test(value)){
               	 	return '不能全为数字';
               	 }
			},
			
			remark : function(value){
				if(value.length<1){
					return '不能为空';
				}
				if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                	 return '不能有特殊字符';
                	 }
               	  if(/(^\_)|(\__)|(\_+$)/.test(value)){
               	  	return '首尾不能出现下划线';
               	 	}
               	 if(/^\d+\d+\d$/.test(value)){
               	 	return '不能全为数字';
               	 }
			}
		});
		
		
		//监听提交
		form.on('submit(add)',
		function(data) {
			console.log(data);
			var price=$.trim($("#price").val());
			var person = $.trim($("#person").val());
			var remark = $.trim($("#remark").val());
			var patientId = $.trim($("#patientId").val());
			var itemName = $.trim($("#itemName").val());
		$.post("${pageContext.request.contextPath}/registration/saveCheckoutList",
			{	
			 price : price,
			 person : person,
			 itemName : itemName,
			 patientId : patientId,
			 remark : remark
			},
			function(res){
				if(res=="04"){
					layer.alert("请先上传化验单",
					{
						icon : 2
					});
					return;
				}else if(res == "0"){
					layer.alert("没有就诊卡",
					{
						icon : 2
					});
					return;
				} else if(res =="1"){
					layer.alert("余额不足",
					{
						icon : 2
					});
					return;
				}else if(res =="12"){
					layer.alert("网络异常",
					{
						icon : 2
					});
					return;
				}else if(res =="13"){
					layer.alert("已开单据",
					{
						icon : 1
					},
                  function() {
						// 获得frame索引
						var index = parent.layer.getFrameIndex(window.name);
						//关闭当前frame
						parent.layer.close(index);
						window.parent.location.reload();
					}) 
				}
			})
		return false;
	});
	})
	
	
	
	//上传附件
	function sub(){
		$("#uploadfile").submit();
	}
	
		
		</script>

</body>

</html>