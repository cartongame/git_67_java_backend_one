<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>修改密码</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">
				
				<div class="layui-form-item">
					<label for="userPwd" class="layui-form-label">
					<span class="x-red">*</span>请输入新密码
					</label>
					<div class="layui-input-inline">
						<input type="password" id="userPwd" name="userPwd" required=""
							lay-verify="userPwd" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="userPwd" class="layui-form-label">
					<span class="x-red">*</span>请再次输入新密码
					</label>
					<div class="layui-input-inline">
						<input type="password" id="userPwd" name="userPwd" required=""
							lay-verify="userPwd" autocomplete="off"
							class="layui-input">
					</div>
				</div>
			
				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui.use([ 'form', 'layer' ],function() {
		$ = layui.jquery;
		var form = layui.form, layer = layui.layer;

		//自定义验证规则
 		form.verify({
 			userPwd : function(value){
 				if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
               	 return '不能有特殊字符';
               	 }
              	  if(/(^\_)|(\__)|(\_+$)/.test(value)){
              	  	return '首尾不能出现下划线';
              	 	}
              	 /*  if(/^\d+\d+\d$/.test(value)){
              	 	return '不能全为数字';
              	 } */
 			}
		});
	//监听提交
	form.on('submit(add)',
	function(data) {
		console.log(data);
		var userPwd = $.trim($("#userPwd").val());
		var pswd=userPwd;
		$.post("${pageContext.request.contextPath}/user/updateDoctorAccPwd", {
			userPwd : userPwd,
			pswd : pswd
			}, function(res) {
				alert(res)
				 if (res == "0") {
					alert("修改失败")
				}else if(res == "1"){
					alert("修改成功!请重新登录")
					//window.location.href = "${pageContext.request.contextPath}/user/logout";
				}else{
					layer.msg('网络异常', {
						icon : 2,
						time : 3000
					});
				}
		})
	})
		
		
		
		
		
		
	})
	
		</script>

</body>

</html>