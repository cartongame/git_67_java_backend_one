<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>医生资料页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
	</div>
	<div class="layui-card-body ">
			<ul>
				<li>姓名：
					<span>
						<c:out value="${doctorDatum.name}"></c:out>
					</span>
				</li>
				<li>性别：
					<span>
						<c:out value="${doctorDatum.sex=='1'?'男':(doctorDatum.sex=='0'?'女':'待补充')}"></c:out>
					</span>
				</li>
				<li>职位：
					<span>
						<c:out value="${doctorDatum.job=='1'?'主任医师':(doctorDatum.job=='2'?'副主任医师':(doctorDatum.job=='3'?'主治医生':'实习医生')) }"></c:out>
					</span>
				</li>
				<li>手机号：
					<span>
						<c:out value="${doctorDatum.phone}"></c:out>
					</span>
				</li>
				<li>邮箱：
					<span>
						<c:out value="${doctorDatum.email}"></c:out>
					</span>
				</li>
				<li>证件：
						<c:forEach items="${doctorExpInfo }" var="doctorExpInfo" varStatus="d">
	    					<c:out value="${doctorExpInfo.urls}"></c:out>
	    				</c:forEach>
				</li>
				<li>论文：
					<c:forEach items="${doctorExpInfo }" var="doctor" varStatus="d">
	    				<c:out value="${doctor.nets}"></c:out>
	    			</c:forEach>
				</li>
				<li>出生日期：
				<span>
					<c:if test="${doctorDatum.birthday == null}">待补充</c:if>
					<c:if test="${doctorDatum.birthday != null}">
					${fn:substring(doctorDatum.birthday,0,9)}
					</c:if>
				</span>
				</li>
				<li>紧急联系人：
					<span>
						<c:out value="${doctorDatum.concat}"></c:out>
					</span>
				</li>
				<li>联系人电话：
					<span>
						<c:out value="${doctorDatum.tel}"></c:out>
					</span>
				</li>
				<li>介绍：
					<span>
						<c:out value="${doctorDatum.intro}"></c:out>
					</span>
				</li>
			</ul>
		</div>
		<div class="layui-card-body ">
		</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
</body>
</html>
