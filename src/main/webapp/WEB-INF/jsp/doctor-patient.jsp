<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>复诊患者列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">复诊患者管理</a>
			<a> <cite>复诊患者列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
			<form class="layui-form layui-col-space5">
				<div class="layui-input-inline layui-show-xs-block">
					<input type="text" name="name" id="name"
						value="<c:out value="${param.name}"></c:out>" placeholder="请输入患者名"
						autocomplete="off" class="layui-input">
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<button class="layui-btn" 
						onclick="sreachName()">
						<i class="layui-icon">&#xe615;</i>
					</button>
				</div>
		</form>
	</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th>患者ID</th>
					<th>姓名</th>
					<th>性别</th>
					<th>电话</th>
					<th>身份证号</th>
					<th>出生日期</th>
					<th>紧急联系人</th>
					<th>联系电话</th>
					<th>患者关系</th>
					<th>操作</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="p" varStatus="s">
			<tr>
				<td><c:out value="${p.patientId}"></c:out></td>
				<td><c:out value="${p.name}"></c:out></td>
				<td><c:out value="${p.sex=='1'?'男':'女'}"></c:out></td>
				<td><c:out value="${p.phone}"></c:out></td>
				<td><c:out value="${p.cardCode}"></c:out></td>
				<td><c:out value="${fn:substring(p.birthday,0,10)}"></c:out></td>
				<td><c:out value="${p.concat}"></c:out></td>
				<td><c:out value="${p.tel}"></c:out></td>
				<!-- 患者关系	1 本人 2 配偶 3 子女 4 父母 5 其他 -->
				<td><c:out value="${p.relation=='1'?'本人':(p.relation=='2'?'配偶':(p.relation=='3'?'子女':(p.relation=='4'?'父母':'其他'))) }"></c:out>
				</td>
				<td>
					<button class="layui-btn" 
						onclick="help(this,'${p.patientId}')">
						<i class="layui-icon"></i>挂号
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>

			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});

	//用户查询
	function sreachName() {
		var name = $("#name").val();
		location.href = "/patient/findAllDoctorPatient?name="+name;
	}
	//挂号
	function help(obj,patientId){
		$.post("${pageContext.request.contextPath}/registration/setRegistration",
			{
			patientId : patientId
			},
			function(res){
				if(res == 1){
					layer.msg('已挂号', {
						icon : 1,
						time : 1000
					});
				}else if(res == 2){
					layer.msg('该患者已挂过号或为黑名单', {
						icon : 2,
						time : 1000
					});
				}else{
					layer.msg('网络异常', {
						icon : 2,
						time : 1000
					});
				}
		})
	}
	
	
</script>
</body>
</html>
