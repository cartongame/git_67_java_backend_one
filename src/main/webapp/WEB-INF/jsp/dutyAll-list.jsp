<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>医生值班列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">医生值班管理</a>
			<a> <cite>医生值班列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th>值班ID</th>
					<th>科室名称</th>
					<th>医生名称</th>
					<th>值班时间</th>
					<th>AMPM</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="p" varStatus="s">
			<tr>
				<td><c:out value="${p.dutyId}"></c:out></td>
				<td><c:out value="${p.deskName}"></c:out></td>
				<td><c:out value="${p.docName}"></c:out></td>
				<td><c:out value="${fn:substring(p.dutyDate,0,10)}"></c:out></td>
				<td><c:out value="${p.ampm=='1'?'上午':(p.ampm=='2'?'下午':'全天')}"></c:out></td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>

			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
</body>
</html>
