<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>历史收入明细列表</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">我的贡献</a>
			<a> <cite>历史收入明细</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
					
			<form class="layui-form layui-col-space5">
				<div class="layui-input-inline layui-show-xs-block">
					<input class="layui-input" placeholder="开始日" name="start" value="${param.start}"
						id="start">
				</div>
				<div class="layui-input-inline layui-show-xs-block">
					<input class="layui-input" placeholder="截止日" name="end" value="${param.end}"
						id="end">
				</div>

				<div class="layui-input-inline layui-show-xs-block">
					<input type="text" name="patName" id="patName"
						value="<c:out value="${param.patName}"></c:out>" placeholder="请输入用户名"
						autocomplete="off" class="layui-input">
				</div>
				
				<div class="layui-input-inline layui-show-xs-block">
					<button class="layui-btn" onclick="sreachUser()">
						<i class="layui-icon">&#xe615;</i>
					</button>
				</div>	
		</form>
		
		<button class="layui-btn"
				onclick="xadmin.open('历史收入统计','${pageContext.request.contextPath}/DayDetail/findHisCost',500,500)">
				<i class="layui-icon"></i>历史收入统计
		</button>
		
		<button class="layui-btn"
				onclick="xadmin.open('上周收入统计','${pageContext.request.contextPath}/DayDetail/findHisCostWeek',500,500)">
				<i class="layui-icon"></i>上周收入统计
		</button>
		
		<button class="layui-btn"
				onclick="xadmin.open('上一月收入统计','${pageContext.request.contextPath}/DayDetail/findHisCostMath',500,500)">
				<i class="layui-icon"></i>上一月收入统计
		</button>
		
	</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th><input type="checkbox" name="" lay-skin="primary"></th>
					<th>患者ID</th>
					<th>患者名称</th>
					<th>项目名称</th>
					<th>项目规格</th>
					<th>单位</th>
					<th>数量</th>
					<th>单价</th>
					<th>小计</th>
					<th>收费时间</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="d" varStatus="s">
			<tr>
				<td style="width: 20px;">
					<input type="checkbox" name="patientId" value="${d.patientId}" lay-skin="primary">
				</td>
				<td style="width: 20px;"><c:out value="${d.patientId}"></c:out></td>
				<td><c:out value="${d.patName}"></c:out></td>
				<td><c:out value="${d.itemName}"></c:out></td>				
				<td><c:out value="${d.itemRule}"></c:out></td>
				<td><c:out value="${d.unit}"></c:out></td>
				<td><c:out value="${d.num}"></c:out></td>
				<td><c:out value="${d.price}"></c:out></td>
				<td><c:out value="${d.total}"></c:out></td>
				<td><c:out value="${d.createTime}"></c:out></td>
									
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>

			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});

		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});

	/*用户-启用停用*/
	function member_stop(obj, userId,userState) {
		
		if ($(obj).parents("tr").find(".td-status").find('span').html() == '已启用') {
			
	layer.confirm('确认要禁用吗？',
		function(index) {
			$.post("${pageContext.request.contextPath}/user/updateUserState",
					{
						userId : userId,userState:userState
					},
					function(res) {
						if (res > 0) {
							layer.alert("禁用成功",
								{
									icon : 1
								},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title','启用')
							
							$(obj).find('i').html('&#xe62f;');
							
							$(obj).parents("tr").find(".td-status").find('span').html('已禁用');

						} else {
							layer.msg('禁用失败!', {
								icon : 2,
								time : 1000
							});
						}
					});

				});
			} else {
				layer.confirm('确认要启用吗？',
					function(index) {
						$.post("${pageContext.request.contextPath}/user/updateUserState",
							{
								userId : userId,userState:userState
							},
						function(res) {
							if (res > 0) {
								layer.alert("启用成功",
									{
										icon : 1
									},
							function() {
								// 获得frame索引
								var index = parent.layer.getFrameIndex(window.name);
								//关闭当前frame
								parent.layer.close(index);
								window.parent.location.reload();
							});
							$(obj).attr('title','禁用')
							
							$(obj).find('i').html('&#xe601;');

							$(obj).parents("tr").find(".td-status").find('span').html('已启用');
						
						} else {
							layer.msg('启用失败!', {
								icon : 2,
								time : 1000
							});
						}
					});
			});
		}

	}
	//用户查询
	function sreachUser() {
		var patName = $("#patName").val();
		var start = $("#start").val();
		var end = $("#end").val();
		
		window.location.href = "${pageContext.request.contextPath}/DayDetail/page?patName="
				+ patName
				+ "&start="
				+ start
				+ "&end="
				+ end;

	}

	//密码重置
	function userpsw_reset(obj, userId) {
		layer.confirm('你确认要重置吗？', function(index) {
			//ajax
			$.post("${pageContext.request.contextPath}/user/resetPwd", {
				userId : userId
			}, function(res) {
				if (res > 0) {
					layer.msg('密码重置成功，重置为123456!', {
						icon : 1,
						time : 1000
					});
				} else {
					layer.msg('密码重置失败!', {
						icon : 1,
						time : 1000
					});
				}
			});
		});
	}
	/*用户-删除*/
	function member_del(obj, userId) {
		layer.confirm('确认要删除吗？', function(index) {
			//发异步删除数据
			$.post("${pageContext.request.contextPath}/user/deleteUser", {
				userId : userId
			}, function(res) {
				if (res > 0) {
					layer.msg('删除成功!', {
						icon : 1,
						time : 1000
					});
					$(obj).parents("tr").remove();
				} else {
					layer.msg('删除失败!', {
						icon : 1,
						time : 1000
					});
				}
			});

		});
	}
	//批量删除
	function delAll() {
		var user_id = [];
		$("input[name='userId']:checked").each(function() {
			user_id.push($(this).val());
		});
		if (user_id.length != 0) {

			userIds = user_id.join(",");
			// alert(userIds)
			/*  var data = tableCheck.getData();
			 alert(data) */
			layer.confirm('确认要删除吗？', function(index) {
				$.post("${pageContext.request.contextPath}/user/deleteUsers", {
					userIds : userIds
				}, function(res) {
					if (res > 0) {
						layer.msg('删除成功', {
							icon : 1
						});
						$(".layui-form-checked").not('.header').parents('tr')
								.remove();
					} else {
						layer.msg('删除失败', {
							icon : 1
						});
					}
				});

			});
		} else {
			layer.msg('请选择要删除的数据', {
				icon : 2
			});
		}
	}
</script>
</html>
</body>
</html>
