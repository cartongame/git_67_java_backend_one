<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">
					<input type="hidden" id="productId" name="productId" value="${product.productId}">
					<input type="hidden" id="storeId" name="storeId" value="${product.storeId}">
				<div class="layui-form-item">
					<label for="productName" class="layui-form-label"> <span
						class="x-red">*</span>药品名称
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productName" name="productName" required=""
							maxlength="16" lay-verify="productName" autocomplete="off"
							class="layui-input" value="${product.productName}" disabled="disabled">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="productInvent" class="layui-form-label"> <span
						class="x-red">*</span>药品库存
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productInvent" name="productInvent" required=""
							maxlength="16" lay-verify="productInvent" autocomplete="off"
							class="layui-input" value="${product.productInvent}" disabled="disabled">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="salePrice" class="layui-form-label"> <span
						class="x-red">*</span>药品售价
					</label>
					<div class="layui-input-inline">
						<input type="text" id="salePrice" name="salePrice" required=""
							maxlength="16" lay-verify="salePrice" autocomplete="off"
							class="layui-input" value="${product.salePrice}" disabled="disabled">
					</div>
				</div>
				
				<div class="layui-form-item">
					<label for="outNum" class="layui-form-label"> <span
						class="x-red">*</span>出库数量
					</label>
					<div class="layui-input-inline">
						<input type="text" id="outNum" name="outNum" required=""
							maxlength="16" lay-verify="outNum" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="createBy" class="layui-form-label"> <span
						class="x-red">*</span>出库人
					</label>
					<div class="layui-input-inline">
						<input type="text" id="createBy" name="createBy" required=""
							maxlength="16" lay-verify="createBy" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交审核</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui
				.use(
						[ 'form', 'layer' ],
						function() {
							$ = layui.jquery;
							var form = layui.form, layer = layui.layer;

							//自定义验证规则
							form.verify({
								userName : function(value) {

									if (value.length < 5) {
										return '用户名至少得5个字符啊';
									}
								},
								userPwd : [ /(.+){6,12}$/, '密码必须6到12位' ],
							/*  repass: function(value) {
							    if ($('#L_pass').val() != $('#L_repass').val()) {
							        return '两次密码不一致';
							    }
							}  */
							});

							//监听提交
							form
									.on(
											'submit(add)',
											function(data) {
												console.log(data);
												var productId = $.trim($(
														"#productId").val());

												var storeId = $.trim($(
														"#storeId").val());
												var productInvent = $.trim($(
														"#productInvent").val());
												var outNum = $.trim($(
												"#outNum").val());
												var createBy = $.trim($(
														"#createBy").val());
												if (createBy == null
														|| createBy == '') {
													layer.msg('出库人不能为空!', {
														icon : 2,
														time : 1000
													});
												} else {
													if (productInvent < outNum) {
														layer.msg("出库数量不能大于库存",
																{
																	icon : 2,
																	time : 1000
																})
													} else {
														$
																.post(
																		"${pageContext.request.contextPath}/product/addOutNum",
																		{
																			productId : productId,
																			storeId : storeId,
																		
																			outNum : outNum,
																			createBy : createBy
																		},
																		function(
																				res) {
																			//if(res=="1"){
																			//console.log("新增成功");
																			layer
																					.alert(
																							"出库成功",
																							{
																								icon : 6
																							},
																							function() {
																								// 获得frame索引
																								var index = parent.layer
																										.getFrameIndex(window.name);
																								//关闭当前frame
																								parent.layer
																										.close(index);
																								window.parent.location
																										.reload();
																							});
																			// }
																		})
													}
												}
												return false;

											});

						});

	
		</script>
</body>

</html>