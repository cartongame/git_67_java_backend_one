<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">
				<div class="layui-form-item">
					<label for="roleName" class="layui-form-label"> <span
						class="x-red">*</span>角色名
					</label>
					<div class="layui-input-inline">
						<input type="text" id="roleName" name="roleName" required=""
							maxlength="25" lay-verify="roleName" autocomplete="off"
							class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="roleCode" class="layui-form-label"> <span
						class="x-red">*</span>角色code
					</label>
					<div class="layui-input-inline">
						<input type="text" id="roleCode" name="roleCode" required=""
							maxlength="16" lay-verify="roleCode" autocomplete="off"
							class="layui-input">
					</div>
				</div>







				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui
				.use(
						[ 'form', 'layer' ],
						function() {
							$ = layui.jquery;
							var form = layui.form, layer = layui.layer;

							//自定义验证规则
							form.verify({
								roleName : function(value) {
									if (value.length < 1) {
										return '角色名至少得2个字符啊';
									}
								},
								
								roleCode : function(value) {
									if (value.length < 2) {
										return '角色code至少3个字符啊';
									}
								}
							});

							//监听提交
							form
									.on(
											'submit(add)',
											function(data) {
												console.log(data);
												var roleName = $.trim($(
														"#roleName").val());
												
												var roleCode = $.trim($(
														"#roleCode").val());
												if (roleName == null
														|| roleName == '') {
													layer.msg('角色名不能为空!', {
														icon : 2,
														time : 1000
													});
												} else if (roleCode == null
														|| roleCode == '') {
													layer.msg('roleCode不能为空!',
															{
																icon : 2,
																time : 1000
															});
												} else {
													$
															.post(
																	"${pageContext.request.contextPath}/role/saveRoleInfo",
																	{
																		roleName : roleName,
																		
																		roleCode : roleCode
																	},
																	function(
																			res) {
																		//if(res=="1"){
																		//console.log("新增成功");
																		layer
																				.alert(
																						"增加成功",
																						{
																							icon : 6
																						},
																						function() {
																							// 获得frame索引
																							var index = parent.layer
																									.getFrameIndex(window.name);
																							//关闭当前frame
																							parent.layer
																									.close(index);
																							window.parent.location
																									.reload();
																						});
																		// }
																	})
												}
												return false;

											});

						});

		//验证角色名是否存在
		$("#roleName").blur(
				function() {
					var roleName = $.trim($("#roleName").val());

					if (!!roleName) {
						$.post(
								"${pageContext.request.contextPath}/role/getRoleName?roleName="
										+ roleName, function(res) {

									if (res == "1") {
										flag = false;
										layer.msg('角色已存在!', {
											icon : 5,
											time : 1000
										});
										$("#roleName").val("")
										return;
									}
								});

					}
				})
		//验证角色Code是否存在
		$("#roleCode").blur(
				function() {

					var roleCode = $.trim($("#roleCode").val());

					if (!!roleCode) {
						$.post(
								"${pageContext.request.contextPath}/role/getRoleName?roleCode="
										+ roleCode, function(res) {

									if (res == "1") {
										flag = false;
										layer.msg('角色code已存在!', {
											icon : 5,
											time : 1000
										});
										$("#roleCode").val("")
										return;
									}
								});

					}

				})
	</script>

</body>

</html>