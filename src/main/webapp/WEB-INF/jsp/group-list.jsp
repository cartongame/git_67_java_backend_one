<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>用户列表页面</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
        <script src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
    </head>
    
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="#">首页</a>
                <a href="#">用户组管理</a>
                <a>
                    <cite>用户组列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                
                                
                                 <div class="layui-input-inline layui-show-xs-block">
                                    <select name="groupState" id="groupState">
                                        <option value="">状态</option>
                                        <option value="1" ${param.groupState=='1'?'selected="selected"':''}>启用</option>
                                        <option value="0" ${param.groupState=='0'?'selected="selected"':''}>禁用</option>
                                    </select>
                                </div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input type="text" name="groupName" id="groupName" value="${param.groupName}" placeholder="请输入用户组名" autocomplete="off" class="layui-input"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="layui-btn" lay-submit="" lay-filter="sreach" onclick="sreachGroup()">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-header">
                       <c:if test="${fn:indexOf(authCodes, 'group-addGroup')!=-1}">
                            <button class="layui-btn" onclick="xadmin.open('添加用户组','${pageContext.request.contextPath}/group/addGroupInfo',400,400)">
                                <i class="layui-icon"></i>添加</button>
                                </c:if>
                                </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                        
                                        <th>用户组ID</th>
                                        <th>用户组名称</th>
                                        <th>用户code</th>
                                        
                                        <th>用户状态</th>
                                        <th>操作</th></tr>
                                </thead>
                                <tbody>
  							<c:forEach items="${page.resultList}" var="group" varStatus="s">
                                    <tr>
                                        
                                        <td style="width:20px;">${group.groupId}</td>
                                       <td><c:out value="${group.groupName}"></c:out> </td>
                                        <td><c:out value="${group.groupCode}"></c:out></td>
                                         
                                        
                                        <td class="td-status"><span
											class="layui-btn layui-btn-normal layui-btn-mini">${group.groupState=='1'?'已启用':'已禁用'}</span>
											
											
                            
                            
											</td>
                                        
                                       <td class="td-manage">
                                             
											
											
											 <c:if test="${fn:indexOf(authCodes, 'add_group_role')!=-1}">
                                            <a title="分配角色" onclick="xadmin.open('分配角色','${pageContext.request.contextPath}/group/getGroupRole?groupId=${group.groupId}',300,300)" href="javascript:;">
                                                <i class="layui-icon">&#xe642;</i></a></c:if>
                                                <c:if test="${fn:indexOf(authCodes, 'stop_group_start_group')!=-1}">
                                          
										<a onclick="member_stop(this,'${group.groupId}','${group.groupState}')" title="${group.groupState=='1'?'禁用':'启用'}"
													href="javascript:;"> <i class="layui-icon">${group.groupState=='1'?'&#xe601;':'&#xe62f;'}</i>

												</a> 
                                            </c:if>
                                        </td>
                                    </tr>
                                   </c:forEach> 
                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    <jsp:include page="standard.jsp"></jsp:include>
                                    
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });
        });
    /*用户组-启用停用*/
	function member_stop(obj, groupId,groupState) {
		
		if ($(obj).parents("tr").find(".td-status").find('span').html() == '已启用') {
			layer
					.confirm(
							'确认要禁用吗？',
							function(index) {
								$
										.post(
												"${pageContext.request.contextPath}/group/updateGroupState",
												{
													groupId : groupId,groupState:groupState
												},
												function(res) {
													if (res > 0) {
														
														layer
														.alert(
																"禁用成功",
																{
																	icon : 1
																},
														function() {
															// 获得frame索引
															var index = parent.layer
																	.getFrameIndex(window.name);
															//关闭当前frame
															parent.layer
																	.close(index);
															window.parent.location
																	.reload();
														});
														$(obj).attr('title',
																'启用')
														$(obj).find('i').html(
																'&#xe62f;');
														$(obj)
																.parents("tr")
																.find(
																		".td-status")
																.find('span')
																.html('已禁用');
														

													} else {
														layer.msg('禁用失败!', {
															icon : 2,
															time : 1000
														});
													}
												});

							});
		} else {
			layer
					.confirm(
							'确认要启用吗？',
							function(index) {
								$
										.post(
												"${pageContext.request.contextPath}/group/updateGroupState",
												{
													groupId : groupId,groupState:groupState
												},
												function(res) {
													if (res > 0) {
														layer
														.alert(
																"启用成功",
																{
																	icon : 1
																},
														function() {
															// 获得frame索引
															var index = parent.layer
																	.getFrameIndex(window.name);
															//关闭当前frame
															parent.layer
																	.close(index);
															window.parent.location
																	.reload();
														});
														$(obj).attr('title',
																'禁用')
														$(obj).find('i').html(
																'&#xe601;');

														$(obj)
																.parents("tr")
																.find(
																		".td-status")
																.find('span')
																.html('已启用');
													} else {
														layer.msg('启用失败!', {
															icon : 2,
															time : 1000
														});
													}
												});

							});
		}

	}
	
		//用户组查询
		function sreachGroup(){
			var groupName=$("#groupName").val();
			var groupState=$("#groupState").val();
			
			window.location.href="${pageContext.request.contextPath}/group/page?groupName="+groupName+"&groupState="+groupState;
		}
		
		

        
        
        </script>
</html>
</body>
</html>