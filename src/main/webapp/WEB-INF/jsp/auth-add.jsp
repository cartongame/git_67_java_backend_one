<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>

</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">
				<div class="layui-form-item">
					<label for="authName" class="layui-form-label"> <span
						class="x-red">*</span>菜单名
					</label>
					<div class="layui-input-inline">
						<input type="text" id="authName" onblur="validateName()"
							name="authName" required="" maxlength="25" lay-verify="authName"
							autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="authUrl" class="layui-form-label"> <span
						class="x-red">*</span>路径
					</label>
					<div class="layui-input-inline">
						<input type="text" id="authUrl" name="authUrl" required=""
							maxlength="16" lay-verify="authUrl" autocomplete="off"
							class="layui-input">
					</div>
					<span id="npc"></span>
				</div>
				<div class="layui-form-item">
					<label for="authCode" class="layui-form-label"> <span
						class="x-red">*</span>auth_code
					</label>
					<div class="layui-input-inline">
						<input type="text" id="authCode" name="authCode" required=""
							maxlength="16" lay-verify="authCode" autocomplete="off"
							class="layui-input">
					</div>
				</div>

				<div class="layui-form-item">
					<label for="authType" class="layui-form-label"> <span
						class="x-red">*</span>权限类型
					</label>
					<div class="layui-input-inline">
						<select id="authType">
							<option value="1">模块</option>
							<option value="2">列表</option>
							<option value="3">按钮</option>
						</select>

					</div>
				</div>
				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
	

	<script type="text/javascript">
		//页面一加载触发事件
		$(function() {
			var params = "${param.params}";
			$("#authType").show();
			if (!params) {//参数为空，表示一级
				//url code 隐藏
				$("#authUrl").hide();
				$("#authCode").hide();
				//类型只能选择模块
				$("#authType option:gt(0)").attr("disabled", "disabled");
			} else {
				var arr = params.split("_");
				if (arr.length > 1 && arr[1] == "1") {//模块
					$("#authType option:eq(2)").attr("disabled", "disabled");
					$("#authUrl").hide();
					$("#authCode").hide();
					$("#authType").change(function() {
						var val = $(this).val();
						if (val == "2") {//列表
							$("#authUrl").show();
						} else {
							$("#authUrl").hide();
						}
					});
				} else if (arr.length > 1 && arr[1] == "2") {//列表
					$("#authType option:lt(2)").attr("disabled", "disabled");
					$("#authType option:eq(2)").attr("selected", "selected");
				} 
			}
		});
		var flag = true;
		layui.use([ 'form', 'layer' ],
			function() {
				$ = layui.jquery;
				var form = layui.form, layer = layui.layer;
				//自定义验证规则
				form.verify({
					authName : function(value) {
						if (value.length < 3) {
							return '菜单名至少得3个字符啊';
						}
					}

				});

				//监听提交
				form.on('submit(add)',
				function(data) {
					console.log(data);
					var authName = $.trim($("#authName").val());
					var authType = $.trim($("#authType").val());
					var authUrl = $.trim($("#authUrl").val());
					var authCode = $.trim($("#authCode").val());
					var params = "${param.params}";
					var arr = params.split("_");
					var parentId = 0;
					if (arr.length > 1) {
						parentId = arr[0]
					}
					 if (authName == null || authName == '') {
						layer.msg('菜单不能为空!', {
							icon : 2,
							time : 1000
						});
						 return false;
					} 
					

					 if($("#authUrl").is(":visible")){
					       if(authUrl==null || authUrl==''){
					    	    layer.msg("路径不能为空", {
					               icon: 2,
					               time: 2000
					           }); 
					    	   return false;
					       }
					 	}
					 if($("#authCode").is(":visible")){
					       if(authCode==null || authCode==''){
					    	    layer.msg("CODE不能为空", {
					               icon: 2,
					               time: 2000
					           }); 
					    	   return false;
					        }
					     }
					 
				$.post(
					"${pageContext.request.contextPath}/auth/saveAuthInfo",
					{
						authName : authName,
						authType : authType,
						parentId : parentId,
						authUrl : authUrl,
						authCode : authCode
					},
					function(res) {
						layer.alert("增加成功",
							{
								icon : 1
							},
						function() {
							// 获得frame索引
							var index = parent.layer.getFrameIndex(window.name);
							//关闭当前frame
							parent.layer.close(index);
							
							window.parent.location.reload();
						});

			})

		});

	});

		//验证菜单名是否存在
		function validateName() {

			var authName = $.trim($("#authName").val());
			var params = "${param.params}";
			var arr = params.split("_");
			var parentId = 0;
			if (arr.length > 1) {

				parentId = arr[0]
			}

			if (!!authName) {
				$.post("${pageContext.request.contextPath}/auth/getAuthName", {
					authName : authName,
					parentId : parentId
				}, function(res) {

					if (res == "1") {
						flag = false;
						layer.msg('菜单已存在!', {
							icon : 5,
							time : 1000
						});
						$("#authName").val("");
						return;
					}
				});

			}

		}
		//验证路径是否存在
		$("#authUrl").blur(function() {
			var authUrl = $.trim($("#authUrl").val());

			if (!!authUrl) {

				$.post("${pageContext.request.contextPath}/auth/getAuthUrl", {
					authUrl : authUrl
				}, function(res) {

					if (res == "1") {
						flag = false;
						layer.msg('路径已存在!', {
							icon : 5,
							time : 1000
						});
						$("#authUrl").val("");
						return;
					}
				});

			}
		})
		//验证authCode是否存在
		$("#authCode").blur(function() {
			var authCode = $.trim($("#authCode").val());

			if (!!authCode) {

				$.post("${pageContext.request.contextPath}/auth/getAuthUrl", {
					authCode : authCode
				}, function(res) {

					if (res == "1") {
						flag = false;
						layer.msg('权限code已存在!', {
							icon : 5,
							time : 1000
						});
						$("#authCode").val("");
						return;
					}
				});

			}
		})
		
		
	</script>
</body>
</html>