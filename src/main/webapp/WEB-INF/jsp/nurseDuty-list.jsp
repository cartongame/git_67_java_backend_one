<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>护士列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">医生管理</a>
			<a> <cite>护士值班列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
			<form class="layui-form layui-col-space5" method="post" action="/nduty/page">
			
				<div class="layui-input-inline layui-show-xs-block">
					<input class="layui-input" placeholder="值班时间" name="ndutyDate" value="${param.ndutyDate}"
						id="dutyDate">
				</div>
				
				<div class="layui-input-inline layui-show-xs-block">
					<input type="text" name="nurseName" id="nurseName"
						value="<c:out value="${param.nurseName}"></c:out>" placeholder="请输入护士名称"
						autocomplete="off" class="layui-input">
				</div>
				
				<div class="layui-input-inline layui-show-xs-block">
					<button class="layui-btn" 
						onclick="sreachNurse()">
						<i class="layui-icon">&#xe615;</i>
					</button>
				</div>
		</form>
	</div>
	<div class="layui-card-header">
		<c:if test="${fn:indexOf(roleCode, 'sister')!=-1}">
		   <button class="layui-btn"
				onclick="xadmin.open('添加值班表','${pageContext.request.contextPath}/nduty/addNDuty?nurseId=${nurse.nurseId}',400,400)">
				<i class="layui-icon"></i>添加值班表
			</button>
		</c:if>

	</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr>
					<th>值班ID</th>
					<th>科室名称</th>
					<th>护士名称</th>
					<th>值班时间</th>
					<th>AMPM</th>
					<th>操作</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="p" varStatus="s">
			<tr>
				<td><c:out value="${p.ndutyId}"></c:out></td>
				<td><c:out value="${p.deskName}"></c:out></td>
				<td><c:out value="${p.nurseName}"></c:out></td>
				<td><c:out value="${fn:substring(p.ndutyDate,0,10)}"></c:out></td>
				<td><c:out value="${p.nampm=='1'?'上午':(p.nampm=='2'?'下午':'全天')}"></c:out></td>
				<td class="td-manage">
					<c:if test="${fn:indexOf(roleCode, 'sister')!=-1}">
						<a title="分配科室"
							onclick="xadmin.open('分配科室','${pageContext.request.contextPath}/nduty/updateNDeskWork?nurseId=${p.nurseId}&ndutyId=${p.ndutyId}',400,400)"
							href="javascript:;"> <i class="layui-icon">&#xe642;</i></a>
						<a title="调班"
							onclick="xadmin.open('调班','${pageContext.request.contextPath}/nduty/updateNDuty?ndutyId=${p.ndutyId}&nurseName=${p.nurseName}',400,400)"
							href="javascript:;">
							<i class="layui-icon">&#xe642;</i>
						</a>
					 </c:if>
				</td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>

			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
	});


	//用户查询
	function sreachNurse() {
		var nurseName = $("#nurseName").val();
		var dutyDate = $("#dutyDate").val();
		window.location.href = "${pageContext.request.contextPath}/nduty/page?nurseName="
				+ nurseName
				+ "&dutyDate="
				+ dutyDate;
	}

</script>
</html>
</body>
</html>
