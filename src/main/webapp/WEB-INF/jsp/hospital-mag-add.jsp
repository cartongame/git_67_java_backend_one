<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
			<form class="layui-form">



				<div class="layui-form-item">
					<label for="hospName" class="layui-form-label"> <span
						class="x-red">*</span>医院名
					</label>
					<div class="layui-input-inline">
						<input type="text" id="hospName" name="hospName" required=""
							maxlength="25" lay-verify="hospName" autocomplete="off"
							class="layui-input" value="${hospital.hospName}">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="intro" class="layui-form-label"> <span
						class="x-red">*</span>医院介绍
					</label>
					<div class="layui-input-inline">
						<input type="text" id="intro" name="intro" required=""
							maxlength="16" lay-verify="intro" autocomplete="off"
							class="layui-input" value="${hospital.intro}">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="foundTime" class="layui-form-label"> <span
						class="x-red">*</span>创建时间
					</label>
					<div class="layui-input-inline">
						<input type="text" id="foundTime" name="foundTime" required=""
							maxlength="16" lay-verify="foundTime" autocomplete="off"
							class="layui-input" value="${hospital.foundTime}">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="address" class="layui-form-label"> <span
						class="x-red">*</span>地址
					</label>
					<div class="layui-input-inline">
						<input type="text" id="address" name="address" required=""
							maxlength="16" lay-verify="address" autocomplete="off"
							class="layui-input" value="${hospital.address}">
					</div>
				</div><div class="layui-form-item">
					<label for="level" class="layui-form-label"> <span
						class="x-red">*</span>医院等级
					</label>
					<div class="layui-input-inline">
						<input type="text" id="level" name="level" required=""
							maxlength="16" lay-verify="level" autocomplete="off"
							class="layui-input" value="${hospital.level}">
					</div>
				</div><div class="layui-form-item">
					<label for="netWork" class="layui-form-label"> <span
						class="x-red">*</span>网址
					</label>
					<div class="layui-input-inline">
						<input type="text" id="netWork" name="netWork" required=""
							maxlength="16" lay-verify="netWork" autocomplete="off"
							class="layui-input" value="${hospital.netWork}">
					</div>
				</div><div class="layui-form-item">
					<label for="phone" class="layui-form-label"> <span
						class="x-red">*</span>电话
					</label>
					<div class="layui-input-inline">
						<input type="text" id="phone" name="phone" required=""
							maxlength="16" lay-verify="phone" autocomplete="off"
							class="layui-input" value="${hospital.phone}">
					</div>
				</div>








				<div class="layui-form-item">
					<label for="L_repass" class="layui-form-label"></label>
					<button class="layui-btn" lay-filter="add" lay-submit="">提交</button>
				</div>
			</form>
		</div>
	</div>
	<script>
		var flag = true;
		layui.use([ 'form', 'layer' ],function() {
			$ = layui.jquery;
			var form = layui.form, layer = layui.layer;

			//监听提交
			form.on('submit(add)',function(data) {
								console.log(data);
								var hospId = $("#hospId").val();
								var name = $("#hospName").val();
								var intro= $("#intro").val();
								var foundTime = $("#foundTime").val();
								var address = $("#address").val();
								var level = $("#level").val();
								var netWork = $("#netWork").val();
								var phone = $("#phone").val();
							var url = "";
							var infoId = "${hospital.hospId}";
							var msg = "";
							if(hospId == null || hospId == ""){
								url = "${pageContext.request.contextPath}/hosp/saveHospital";
								msg = "增加成功";
							} else {
								url = "${pageContext.request.contextPath}/hosp/updateHospital";
								msg = "修改成功";
							}
								$.post(url,{
									hospId : hospId,
									hospName : name,
									intro : intro,
									foundTime : foundTime,
									address : address,
									level : level,
									netWork : netWork,
									phone : phone
												},
												function(res) {layer.alert(msg,{icon : 6},function() {
													// 获得frame索引
													var index = parent.layer.getFrameIndex(window.name);
													window.location.href
													//关闭当前frame
													parent.layer.close(index);
													window.parent.location.reload();
												});

												})

								return false;

							});

		});


	</script>

</body>

</html>