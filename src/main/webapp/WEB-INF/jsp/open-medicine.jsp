<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>开处方页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">开处方</a>
			<a> <cite>开处方</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
						<div class="layui-input-inline layui-show-xs-block">
							<input type="text" name="productName" id="productName" placeholder="请输入药品名"
								autocomplete="off" class="layui-input">
						</div>
						<button onclick="sreachProduct()">
							<i class="layui-icon">&#xe615;</i>
						</button>
							<div class="layui-input-inline layui-show-xs-block">
							<input type="text" name="pNum" required="" maxlength="2"
									placeholder="请输入数量" lay-verify="pNum" autocomplete="off"
								autocomplete="off" class="layui-input">
							</div>
							<div class="layui-input-inline layui-show-xs-block">
								<button class="layui-btn" onclick="affirm()">
								<i class="layui-icon"></i>确定
								</button>
							</div>
							<div class="layui-input-inline layui-show-xs-block">
							<button class="layui-btn" style="line-height: 1.6em; margin-top: 3px; float: right"
								onclick="xadmin.open('医嘱','${pageContext.request.contextPath}/registration/openEntrust?patientId=${records.patientId}&registId=${records.registId}',400,400)">
							<i class="layui-icon"></i>开具医嘱
							</button>
							</div>
					</div>
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr><th></th>
					<th>id</th>
					<th>药品名</th>
					<th>库存</th>
					<input type="hidden" name="patientId" id="patientId" value="${records.patientId}" >
					<input type="hidden" name="registId" id="registId" value="${records.registId}" >
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="product" varStatus="s">
			<tr>
				<td style="width: 20px;">
				<input type="radio" name="productId" value="${product.productId}">
				</td>
				<td><c:out value="${product.productId}"></c:out></td>
				<td><c:out value="${product.productName}"></c:out></td>
				<td><c:out value="${product.productInvent}"></c:out></td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>
			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
	<script>
		layui.use([ 'form', 'layer' ],function() {
		$ = layui.jquery;
		var form = layui.form, layer = layui.layer;
		form.verify({
			pNum : [ /^[0-9]+(.[0-9]{2})?$/ ,'只能为数字,最多两位小数' ] ,
		})
	});
		
		
		
	//模糊查询药品
	function sreachProduct() {
		var productName = $.trim($("#productName").val());
		var patientId = $.trim($("#patientId").val());
		var registId = $.trim($("#registId").val());
		if(productName==null || productName==''){
			layer.msg('请输入需要查询的药品',{
				icon : 2
			});
		}else{
			location.href ="/registration/findAllProduct?productName="+productName+"&patientId="+patientId+"&registId="+registId;
		}
	}
	
	//选中药品开处方
	function affirm() {
		var patientId=$("input[name='patientId']").val(); //患者id
		var registId = $.trim($("#registId").val()); //挂号单id
		var pNum=$("input[name='pNum']").val(); //数量
		var productId=$("input[name='productId']:checked").val();//药品id
		if(productId == null || productId==''){
			layer.alert("请选择药品",
					{
						icon : 2
					});
		}else if(pNum == null || pNum==''){
			layer.alert("请输入数量",
					{
						icon : 2
					});
		}else{
			layer.confirm('请确认药品数量！',
				function(index) {
				$.post("${pageContext.request.contextPath}/registration/saveRecords",
					{
					productId : productId , patientId : patientId , pNum : pNum , registId : registId
					},
					function(res) {
						if (res == "1") {
							layer.alert("已开处方",
							{
								icon : 1
							});
							}else{
							layer.alert("网络异常",
									{
										icon : 1
									});
						}
				});
			});
	}
	} 
	
	
	
	
	
	
		
</script>
</html>