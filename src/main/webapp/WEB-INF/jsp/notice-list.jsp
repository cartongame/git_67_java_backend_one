<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>资讯列表页面</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
        <script src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
    </head>
    
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="#">首页</a>
                <a href="#">资讯管理</a>
                <a>
                    <cite>资讯列表</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                
                                
                         
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input type="text" name="title" id="title" value="${param.title}" placeholder="请输入标题名" autocomplete="off" class="layui-input"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="layui-btn" lay-submit="" lay-filter="sreach" onclick="sreachNotice()">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-header">
                      
                            <button class="layui-btn" onclick="xadmin.open('添加资讯','${pageContext.request.contextPath}/notice/addNotice',400,400)">
                                <i class="layui-icon"></i>添加</button>
                             
                                </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                         <th>资讯ID</th>
                                        <th>资讯标题</th>
                                        <th>资讯内容</th>
                                        <th>创建人</th>
                                       <th>创建时间</th>
                                        <th>操作</th></tr>
                                </thead>
                               <tbody>
  							<c:forEach items="${page.resultList}" var="notice" varStatus="s">
                                    <tr>
                                        
                                        <td style="width:20px;">${notice.infoId}</td>
                                        <td><c:out value="${notice.title}"></c:out> </td>
                                       <td><c:out value="${notice.context}"></c:out> </td>
                                        <td><c:out value="${notice.createBy}"></c:out></td>
                                          <td><c:out value="${notice.createTime}"></c:out></td>
                                     <td class="td-manage">
                                           
                                           <a title="修改资讯信息" onclick="xadmin.open('修改资讯信息','${pageContext.request.contextPath}/notice/findNotice?infoId=${notice.infoId}',400,400)" href="javascript:;">
                                                <i class="layui-icon">&#xe642;</i></a> 
                                         <a title="删除" onclick="member_del(this,'${notice.infoId}')"
							href="javascript:;"> <i class="layui-icon">&#xe640;</i></a>
                                           
                                        </td>
                                    </tr>
                                   </c:forEach> 
                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    <jsp:include page="standard.jsp"></jsp:include>
                                    
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });
        });
   
		//列表查询
		function sreachNotice(){
			var info_id=$("#infoId").val();
			var title=$("#title").val();
			var context=$("#context").val();
			var createBy=$("#createBy").val();
			var createTime=$("#createTime").val();
			
			window.location.href="${pageContext.request.contextPath}/notice/page?infoId="+infoId+"&title="+title+"&context="+context+"&createBy="+createBy+"&createTime="+createTime;
		}
		
		/*用户-删除*/
		function member_del(obj, infoId) {
			layer.confirm('确认要删除吗？', function(index) {
				//发异步删除数据
				$.post("/notice/deleteNotice", {
					infoId : infoId
				}, function(res) {
					if (res > 0) {
						layer.msg('删除成功!', {
							icon : 1,
							time : 1000
						});
						$(obj).parents("tr").remove();
					} else {
						layer.msg('删除失败!', {
							icon : 1,
							time : 1000
						});
					}
				});

			});
		}

        
        
        </script>
</html>
</body>
</html>