<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>药房列表页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/zTree_v3/js/jquery-1.4.4.min.js"></script>
</head>

<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="#">首页</a> <a href="#">药房管理</a>
			<a> <cite>药房列表</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
		<form class="layui-form layui-col-space5">
			<div class="layui-input-inline layui-show-xs-block">
				<select name="storeName" id="storeName">
					<option value="西药房">西药房</option>
					<option value="中药房">中药房</option>
				</select>
			</div>
			<div class="layui-input-inline layui-show-xs-block">
				<button class="layui-btn" 
					onclick="sreachProduct()">
					<i class="layui-icon">&#xe615;</i>
				</button>
			</div>
		</form>
	</div>
		
	<div class="layui-card-body ">
		<table class="layui-table layui-form">
			<thead>
				<tr> 
					<th>药品名称</th>
					<th>条形码/编号</th>
					<th>药品库存</th>
					<th>药品分类</th>
					<th>药品进价</th>
					<th>药品售价</th>
					<th>保质期</th>
					<th>操作</th>
				</tr>
			</thead>
	<tbody>
		<c:forEach items="${page.resultList}" var="productVo" varStatus="p">
			<tr>
				<td><c:out value="${productVo.productName}"></c:out></td> 
				<td><c:out value="${productVo.productNum}"></c:out></td>
				<td><c:out value="${productVo.productInvent}"></c:out></td>
				<td><c:out value="${productVo.typeName}"></c:out></td>
				<td><c:out value="${productVo.inPrice}"></c:out></td>
				<td><c:out value="${productVo.salePrice}"></c:out></td>
				<td><c:out value="${productVo.suppDate}"></c:out></td>
				<td>
					<button class="layui-btn" 
						onclick="xadmin.open('采购单','${pageContext.request.contextPath}/product/editProduct?productId=${productVo.productId}&productName=${productVo.productName}&storeId=${productVo.storeId}&productInvent=${productVo.productInvent}',400,400)">
							<i class="layui-icon"></i>采购
					</button>
					<button class="layui-btn"
						onclick="xadmin.open('入库单','${pageContext.request.contextPath}/product/exitProduct?productId=${productVo.productId}&productName=${productVo.productName}&storeId=${productVo.storeId}',400,400)">
							<i class="layui-icon"></i>入库
					</button>
					<button class="layui-btn"
						onclick="xadmin.open('出库单','${pageContext.request.contextPath}/product/productRetrieval?productId=${productVo.productId}&storeId=${productVo.storeId}&productName=${productVo.productName}&productInvent=${productVo.productInvent}&salePrice=${productVo.salePrice}',400,400)">
							<i class="layui-icon"></i>出库
					</button>
					<button class="layui-btn"
						onclick="xadmin.open('药品详情','${pageContext.request.contextPath}/product/productDetails?productName=${productVo.productName}&productNum=${productVo.productNum}&productInvent=${productVo.productInvent}&introduce=${productVo.introduce}&inPrice=${productVo.inPrice}&salePrice=${productVo.salePrice}&imgs=${productVo.imgs}&productDate=${productVo.productDate}&suppDate=${productVo.suppDate}',400,400)">
							<i class="layui-icon"></i>详情
					</button>
					<button class="layui-btn"
						onclick="xadmin.open('药品盘点','${pageContext.request.contextPath}/storeCheck/store?productId=${productVo.productId}&productName=${productVo.productName}&storeName=${productVo.storeName }&storeId=${productVo.storeId}&batchId=${productVo.batchId}&recordId=${productVo.recordId}',400,400)">
							<i class="layui-icon"></i>盘点
					</button>
				</td>
			</tr>
		</c:forEach>
	</tbody>
		</table>
	</div>
	<div class="layui-card-body ">
		<div class="page">
			<div>
				<jsp:include page="standard.jsp"></jsp:include>

			</div>
		</div>
	</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui.use([ 'laydate', 'form' ], function() {
		var laydate = layui.laydate;
		layer = layui.layer;
		//执行一个laydate实例
		laydate.render({
			elem : '#start' //指定元素
		});

		//执行一个laydate实例
		laydate.render({
			elem : '#end' //指定元素
		});
	});

	
	//药品查询
	function sreachProduct() {
		var productName = $("#productName").val();
		var storeName = $("#storeName").val();
		
		window.location.href = "${pageContext.request.contextPath}/product/page?productName="
				+ productName;
				+ "&storeName="
				+ storeName;

	}
	//上传图片
	function sub(){
	  $("#uploadImg").submit();
	}
	
</script>
</html>
