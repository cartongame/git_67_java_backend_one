<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
<meta charset="UTF-8">
<title>新增页面</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/xadmin.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</head>

<body>
	<div class="layui-fluid">
		<div class="layui-row">
				<div class="layui-form-item">
					<label for="productName" class="layui-form-label"> <span
						class="x-red"></span>药品名称
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productName" name="productName" required=""
							maxlength="16" lay-verify="productName" autocomplete="off"
							class="layui-input" value="${product.productName}" disabled="disabled">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="productNum" class="layui-form-label"> <span
						class="x-red"></span>条形码/编号
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productNum" name="productNum" required=""
							maxlength="16" lay-verify="productNum" autocomplete="off"
							class="layui-input" value="${product.productNum}" disabled="disabled">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="productInvent" class="layui-form-label"> <span
						class="x-red"></span>药品库存
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productInvent" name="productInvent" required=""
							maxlength="16" lay-verify="productInvent" autocomplete="off"
							class="layui-input" value="${product.productInvent}" disabled="disabled">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="introduce" class="layui-form-label"> <span
						class="x-red"></span>药品介绍
					</label>
					<div class="layui-input-inline">
						<input type="text" id="introduce" name="introduce" required=""
							maxlength="16" lay-verify="introduce" autocomplete="off"
							class="layui-input" value="${product.introduce}" disabled="disabled"> 
					</div>
				</div>
				<div class="layui-form-item">
					<label for="inPrice" class="layui-form-label"> <span
						class="x-red"></span>药品进价
					</label>
					<div class="layui-input-inline">
						<input type="text" id="inPrice" name="inPrice" required=""
							maxlength="16" lay-verify="inPrice" autocomplete="off"
							class="layui-input" value="${product.inPrice}" disabled="disabled">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="salePrice" class="layui-form-label"> <span
						class="x-red"></span>药品售价
					</label>
					<div class="layui-input-inline">
						<input type="text" id="salePrice" name="salePrice" required=""
							maxlength="16" lay-verify="salePrice" autocomplete="off"
							class="layui-input" value="${product.salePrice}" disabled="disabled">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="productDate" class="layui-form-label"> <span
						class="x-red"></span>生产日期
					</label>
					<div class="layui-input-inline">
						<input type="text" id="productDate" name="productDate" required=""
							maxlength="16" lay-verify="productDate" autocomplete="off"
							class="layui-input" value="${product.productDate}" disabled="disabled">
					</div>
				</div>
				<div class="layui-form-item">
					<label for="suppDate" class="layui-form-label"> <span
						class="x-red"></span>保质期
					</label>
					<div class="layui-input-inline">
						<input type="text" id="suppDate" name="suppDate" required=""
							maxlength="16" lay-verify="suppDate" autocomplete="off"
							class="layui-input" value="${product.suppDate}" disabled="disabled">
					</div>
				</div>
		</div>
	</div>
</body>

</html>