/*导出申请表*/
function downloadApple(){
	var membId=$(".membId").val();
	var pageContext=$("#pageContext").val();
	window.location.href=""+pageContext+"/contract/export?membId="+membId;
}

/*导出客户列表*/
function exportMemb(){
	var page=$("#page").val();
	var currNo=$("#currNo").val();
	var membName=$("#membName").val();
	var phone=$("#phone").val();
	var start=$("#start").val();
	var end=$("#end").val();
	var pageContext=$("#pageContext").val();
	window.location.href=""+pageContext+"/contract/exportMemb?page="+page
														+"&currNo="+currNo
														+"&membName="+membName
														+"&phone="+phone
														+"&start="+start
														+"&end="+end;
}

/*导入文件*/
function upload(){
	var flag=true;
	var fileValue=$("#upfile").val();
	var index=fileValue.lastIndexOf('.');
	var Extension=fileValue.substring(index);
	if(null==fileValue || ''==fileValue){
		flag=false;
		alert("请选择一个需要上传的excel文件");
	}else if(Extension!='.xls' && Extension!='.xlsx'){
		flag=false;
		alert("上传的文件不是excel文件")
	}
	if(flag=true){
		$("#upload").submit();
	}
}

/*密码重置*/
function updateMember(obj, membId) {
	var pageContext=$("#pageContext").val();
	layer.confirm('你确认要重置吗？', function(index) {
		$.post(""+pageContext+"/contract/updateMember?membId="+membId, {
			membId : membId
		}, function(res) {
			if (res) {
				layer.msg('密码重置成功，重置为666666!', {
					icon : 1,
					time : 1000
				});
			} else {
				layer.msg('密码重置失败!', {
					icon : 1,
					time : 1000
				});
			}
		});
	});
}

/*审核*/
function audit(obj,applyId,membId,membName,applyMoney,loanYear,loanWay,loanRate,repaymentWay,cardName,cardNum,payDate,loanUse,auditState){
	alert(auditState);
	var pageContext=$("#pageContext").val();
	layer.confirm('是否通过此申请？', {
		  btn: ['通过', '不通过'] ,
		  //审核不通过
		  btn2: function(index, layero){
			  var auditFlag=-1;
			  layer.prompt({
				  formType: 2,
				  value: '',
				  title: '请说明原因',
				  area: ['300px', '100px'] ,//自定义文本域宽高
				}, function(value, index, elem){
					$.post(""+pageContext+"/contract/updateAuditState",{auditState:auditState,auditFlag:auditFlag,applyId:applyId,membId:membId,reson:value},function(res){
					  if(res=="0"){
						  console.log("审核不通过操作成功");
					  }else{
						  console.log("审核不通过操作失败");
					  }
					 // window.location.reload();
				  });
				  layer.close(index);
				});
			  
		  }
		//审核通过
		}, function(index, layero){
			  var auditFlag=1;
			  var adviceContext;
			  //当行长审核通过
			  if(auditState>=2){
				  layer.prompt({
					  formType: 2,
					  value: '',
					  title: '审核通过,请填写放款通知单内容',
					  area: ['300px', '100px'], //自定义文本域宽高
					}, function(value, index, elem){
						adviceContext=value; //得到value
						console.log("3");
					  layer.close(index);
					  window.location.reload();
					});
				//生成合同
				  $.post(""+pageContext+"/contract/saveContract",{applyId:applyId,membId:membId,repaymentWay:repaymentWay,cardName:cardName,cardNum:cardNum,payDate:payDate,applyMoney:applyMoney,loanYear:loanYear,loanWay:loanWay,loanRate:loanRate,loanUse:loanUse},function(res){
					  if(res=="0"){
						  layer.msg('已生成合同', {
								icon : 2,
								time : 1000
							});
						  console.log("2");
					  }else{
						  console.log("生成合同失败");
					  }
				  });
			  }
			  //修改申请表和审核表的状态
			  $.post(""+pageContext+"/contract/updateAuditState",{membId:membId,auditState:auditState,auditFlag:auditFlag,applyId:applyId,adviceContext:adviceContext},function(res){
				  if(res=="0"){
					  console.log("申请表状态修改成功");
					  console.log("1");
					 // window.location.reload();
				  }else{
					  console.log("申请表状态修改失败");
				  }
			  });
			  layer.close(index);  
		});
}

function getMembDetail(obj,membId){
	var pageContext=$("#pageContext").val();
	window.location.href=""+pageContext+"/contract/getMembDetail?membId="+membId;
}
//客户查询

function toUpdateMemb(){
	var pageContext=$("#pageContext").val();
	$.post(""+pageContext+"/contract/toUpdateMemb",{
		membName:membName,		
		phone:phone,		
		age:age,		
		sex:sex,		
		nation:nation,		
		isMarriy:isMarriy,		
		profession:profession,		
		salary:salary,		
		loanUse:loanUse,		
		firmAddress:firmAddress,		
		firmTel:firmTel,		
		contact:contact,		
		concatPhone:concatPhone,		
		oldAddress:oldAddress,		
		newAddress:newAddress
	},function(res){
		if(res=="1"){
			console.log("1");
		}
	});
}


//修改客户信息
function updateMemb(){
	var membId= $("#membId").val();
	var pageContext=$("#pageContext").val();
	$.post(""+pageContext+"/contract/updateMemb",{
											membId: $("#membId").val(),
											membName:$("#membName").val(),
											age: $("#age").val(),
											phone:$("#phone").val(),
											sex: $("#sex").val(),
											nation: $("#nation").val(),
											cardNum: $("#cardNum").val(),
											isMarriy:$("#isMarriy").val(),
											profession: $("#profession").val(),
											salary:$("#salary").val(),
											loanUse:$("#loanUse").val(),
											firmName: $("#firmName").val(),
											firmAddress: $("#firmAddress").val(),
											firmTel:$("#firmTel").val(),
											contact:$("#contact").val(),
											concatPhone: $("#concatPhone").val(),
											oldAddress: $("#oldAddress").val(),
											newAddress: $("#newAddress").val(),
											},function(res){
												if(res=="0"){
													layer.msg('修改成功', {
														icon : 2,
														time : 1000
													});
													window.location.href=""+pageContext+"/contract/getMembDetail?membId="+membId;
												}else{
													layer.msg('修改失败', {
														icon : 2,
														time : 1000
													});
													window.location.href=""+pageContext+"/contract/getMembDetail?membId="+membId;
												}
	});
}

//跳至审核页面
function findApply(obj,membId){
	var pageContext=$("#pageContext").val();
	window.location.href=""+pageContext+"/contract/findApply?membId="+membId;
}

//打开合同
function toContract(obj,applyId,membId,loanWay){
	var pageContext=$("#pageContext").val();
	window.location.href=""+pageContext+"/contract/toContract?applyId="+applyId+"&membId="+membId+"&loanWay="+loanWay;
	}

//去资料页面
function toAudit(){
	var pageContext=$("#pageContext").val();
	window.location.href=""+pageContext+"/contract/findFiles";
}

//打印合同printContract
function printContract(){
	window.print();
}

//下载合同
function downContract(){
	var pageContext=$("#pageContext").val();
	window.location.href="/Credit/contract/downContract";
}

//提前还款审核
function auditRepayApp(obj,repayAppId){
	var pageContext=$("#pageContext").val();
	var auditFlag;
	var auditState;
	layer.confirm('审核是否通过？', {
		  btn: ['通过', '不通过'] //可以无限个按钮
		  ,btn2: function(index, layero){
		    //不通过时的回调
			  auditFlag="-1";
			  auditState="-1";
			//审核提交
				$.post(""+pageContext+"/contract/auditRepayApp",{auditFlag:auditFlag,auditState:auditState,repayAppId:repayAppId},function(res){
					if(res=="1"){
						console.log("提前还款审核成功");
					}else if(res=="-1"){
						console.log("提前还款审核失败");
					}
					
				});
				window.location.reload();
		  }
		}, function(index, layero){
		  //通过时的回调
			auditFlag="1";
			auditState="1";
			//审核提交
			$.post(""+pageContext+"/contract/auditRepayApp",{auditFlag:auditFlag,auditState:auditState,repayAppId:repayAppId},function(res){
				if(res=="1"){
					console.log("提前还款审核成功");
				}else if(res=="-1"){
					console.log("提前还款审核失败");
				}
			});
			layer.close(index);
			window.location.reload();
		});
}

//提前还款申请搜索
function sreachRepayApp(obj,createTime,auditState,membName){
	var pageContext=$("#pageContext").val();
	var start=$("#start");
	var end=("#end");
	$.post(""+pageContext+"/contract/findeRepayPage",{start:start,end:end,auditState:auditState,membName:membName},function(res){
		if(res=="1"){
			console.log("查询成功");
		}else{
			console.log("查询失败");
		}
	});
}

//查看放款通知单
function notice(){
	var pageContext=$("#pageContext").val();
	window.location.href=""+pageContext+"/contract/notice";
}

//打印放款通知单
function printNotice(){
	window.print();
}

//下载放款通知单
function downNotice(){
	window.location.href="/Credit/contract/downNotice";
}




