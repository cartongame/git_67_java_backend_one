<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!doctype html>
<html  class="x-admin-sm">
<head>
	<meta charset="UTF-8">
	<title>权限管理系统</title>
	 
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath }/css/login.css">
	  <link rel="stylesheet" href="${pageContext.request.contextPath }/css/xadmin.css">
      <script type="text/javascript" src="js/jquery.min.js"></script> 
    
</head>
<body class="login-bg">
    
    <div class="login layui-anim layui-anim-up">
        <div class="message">权限管理系统</div>
        <div id="darkbannerwrap"></div>
        
        <form action="${pageContext.request.contextPath }/user/login" method="post" id="userInfoLogin">
        <h1 id="npc" style="font-size: 16px;color:red; bold;">请输入用户信息</h1>
        <hr class="hr15">
            <input type="text" name="userName" id="userName" value="" placeholder="请输入用户名"  class="layui-input" >
            <hr class="hr15">
            <input type="password" name="userPwd" id="userPwd" value="" placeholder="请输入密码" class="layui-input">
            <hr class="hr15">
            <input type="text" onkeydown="if(event.keyCode==13) login()" oninput="value=value.replace(/[^\d]/g,'')" maxlength="4" name="code" id="code" value="" placeholder="请输入验证码" class="layui-input">
            <hr class="hr15">
            <img id="vdimgck" src="${pageContext.request.contextPath}/images/images.jsp?d="+new Date()+"" alt="看不清？点击更换" align="absmiddle" width="100" height="40" style="cursor:pointer" onclick="this.src=this.src+'?'" />
            <hr class="hr15">
            
            <input value="登录" onclick="login()" style="width:100%;" type="button">
            <hr class="hr20" >
        </form>
    </div>
 
    
    <script type="text/javascript" src="js/login.js"></script> 
</body>
</html>